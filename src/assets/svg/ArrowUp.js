import * as React from 'react';

function ArrowUp(props) {
	return (
		<svg
			width={16}
			height={10}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					d='M11.01 5.531l-4.25 4.25a.747.747 0 01-1.06 0l-.706-.706a.747.747 0 010-1.06l3.012-3.012-3.012-3.012a.747.747 0 010-1.06L5.7.225a.747.747 0 011.06 0l4.25 4.25a.743.743 0 010 1.056z'
					fill={props.color}
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path
						fill='#fff'
						transform='rotate(-90 5 5)'
						d='M0 0h10v16H0z'
					/>
				</clipPath>
			</defs>
		</svg>
	);
}

export default ArrowUp;
