import * as React from 'react';

function FacebookFooter(props) {
	return (
		<svg
			width={11}
			height={22}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M10.293 7.476H7.128V5.4c0-.78.517-.961.88-.961h2.234V1.012L7.166 1C3.752 1 2.975 3.556 2.975 5.192v2.284H1v3.532h1.975V21h4.153v-9.992h2.803l.362-3.532z'
				stroke='#fff'
			/>
		</svg>
	);
}

export default FacebookFooter;
