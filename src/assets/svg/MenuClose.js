import * as React from 'react';

function MenuClose(props) {
	return (
		<svg
			width={16}
			height={16}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M9.306 7.956l6.48-6.48A.889.889 0 0014.533.223l-6.48 6.48-6.48-6.49A.889.889 0 00.32 1.468L6.8 7.956l-6.49 6.48a.888.888 0 101.254 1.253l6.489-6.48 6.48 6.48a.89.89 0 001.253-1.253l-6.48-6.48z'
				fill='#fff'
			/>
		</svg>
	);
}

export default MenuClose;
