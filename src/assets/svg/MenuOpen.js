import * as React from 'react';

function MenuOpen(props) {
	return (
		<svg
			width={20}
			height={16}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M1 14.5h18M1 1h18H1zm0 6.75h18H1z'
				stroke={props.color}
				strokeWidth={2}
				strokeLinecap='round'
				strokeLinejoin='round'
			/>
		</svg>
	);
}

export default MenuOpen;
