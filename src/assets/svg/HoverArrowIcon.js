import * as React from 'react';

function HoverArrowIcon(props) {
	return (
		<svg
			width={128}
			height={13}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					d='M0 12h59.673l5.024-10 5.265 10H128'
					stroke='#fff'
					strokeWidth={2}
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path fill='#fff' d='M0 0h128v13H0z' />
				</clipPath>
			</defs>
		</svg>
	);  
}

export default HoverArrowIcon;
