import * as React from 'react';

function ContactFooter(props) {
	return (
		<svg
			width={24}
			height={18}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M3 3l6.844 6.873c1.357 1.503 2.808 1.503 4.352 0C15.74 8.371 18.008 6.08 21 3M9 9l-6 6M15 9l6 6'
				stroke='#fff'
			/>
			<path
				clipRule='evenodd'
				d='M3 .5A2.5 2.5 0 00.5 3v12A2.5 2.5 0 003 17.5h18a2.5 2.5 0 002.5-2.5V3A2.5 2.5 0 0021 .5H3z'
				stroke='#fff'
			/>
		</svg>
	);
}

export default ContactFooter;
