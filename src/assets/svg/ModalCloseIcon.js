import * as React from 'react';

function ModalCloseIcon(props) {
	return (
		<svg
			width={20}
			height={19}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					fillRule='evenodd'
					clipRule='evenodd'
					d='M9.787 8.324L17.841.27a.61.61 0 01.863.862l-8.055 8.055 8.329 8.328a.61.61 0 11-.863.862l-8.328-8.328-8.328 8.328a.61.61 0 01-.862-.862l8.328-8.328L.871 1.132A.61.61 0 011.733.27l8.054 8.054z'
					fill='#454545'
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path fill='#fff' d='M0 0h20v19H0z' />
				</clipPath>
			</defs>
		</svg>
	);
}

export default ModalCloseIcon;
