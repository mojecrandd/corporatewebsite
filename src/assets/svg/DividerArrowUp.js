import * as React from 'react';

function DividerArrowUp(props) {
	return (
		<svg
			width={30}
			height={15}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					d='M.5 14L14.535 1 29.5 14'
					stroke='#c2c1c1'
					strokeLinecap='square'
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path fill='#fff' d='M0 0h30v15H0z' />
				</clipPath>
			</defs>
		</svg>
	);
}

export default DividerArrowUp;
