import * as React from 'react';

function RefreshIcon(props) {
	return (
		<svg
			width={25}
			height={25}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					d='M24.316 11.019a.931.931 0 10-1.836.31c.948 5.582-2.809 10.877-8.391 11.825-5.582.948-10.877-2.808-11.825-8.39a10.253 10.253 0 0117.67-8.64l-4.125 1.373a.931.931 0 00.588 1.768l5.587-1.863a.931.931 0 00.637-.884V.931a.931.931 0 10-1.862 0v3.402C15.963-.228 8.378-.038 3.817 4.757-.745 9.553-.555 17.138 4.24 21.7c4.796 4.56 12.381 4.37 16.942-.425a11.984 11.984 0 003.133-10.256z'
					fill='#fff'
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path fill='#fff' d='M0 0h25v25H0z' />
				</clipPath>
			</defs>
		</svg>
	);
}

export default RefreshIcon;
