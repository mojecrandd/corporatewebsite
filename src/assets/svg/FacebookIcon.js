import * as React from 'react';

function FacebookIcon(props) {
	return (
		<svg
			width={16}
			height={32}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M14.94 10.714h-4.747V7.6c0-1.169.774-1.441 1.32-1.441h3.35V1.018L10.25 1C5.128 1 3.962 4.834 3.962 7.288v3.426H1v5.297h2.962V31h6.23V16.011h4.205l.543-5.297z'
				stroke='#fff'
			/>
		</svg>
	);
}

export default FacebookIcon;
