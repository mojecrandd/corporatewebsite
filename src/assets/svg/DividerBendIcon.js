import * as React from 'react';

function DividerBendIcon(props) {
	return (
		<svg
			width={12}
			height={18}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				stroke='#fff'
				strokeWidth={1.5}
				d='M1.094 17.364L10.607.886'
			/>
		</svg>
	);
}

export default DividerBendIcon;
