import * as React from 'react';

function BulletListIcon(props) {
	return (
		<svg
			width={15}
			height={14}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M14.667 6.87a6.87 6.87 0 11-13.741.001 6.87 6.87 0 0113.741 0zm-11.724 0a4.854 4.854 0 109.707 0 4.854 4.854 0 00-9.707 0z'
				fill='#2659A2'
			/>
		</svg>
	);
}

export default BulletListIcon;
