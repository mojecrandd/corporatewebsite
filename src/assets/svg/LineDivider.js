import * as React from 'react';

function LineDivider(props) {
	return (
		<svg
			width={1}
			height={47}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path d='M.5.5v46' stroke='#c2c1c1' strokeLinecap='square' />
		</svg>
	);
}

export default LineDivider;
