import * as React from 'react';

function OpenLinkIcon(props) {
	return (
		<svg
			width={18}
			height={17}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)' stroke='#fff'>
				<path d='M4.607 13.799L17.245 1.161M10.263 1.071l7.107.035-.036 7.036' />
				<path
					d='M1.385 3.728v12.384M13.499 16.142H2.385'
					strokeLinecap='square'
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path fill='#fff' d='M0 0h18v17H0z' />
				</clipPath>
			</defs>
		</svg>
	);
}

export default OpenLinkIcon;
