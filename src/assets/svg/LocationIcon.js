import * as React from 'react';

function LocationIcon(props) {
	return (
		<svg
			width={14}
			height={18}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				clipRule='evenodd'
				d='M11.41 11c.813-1.15 1.45-2.383 1.45-3.857a5.833 5.833 0 10-11.667 0c0 1.907.974 3.22 2.08 4.667l3.697 4.857c.067.086 1.283-1.537 1.387-1.667.466-.598.929-1.199 1.386-1.803.544-.727 1.144-1.44 1.667-2.197z'
				stroke='#454545'
				strokeLinecap='round'
				strokeLinejoin='round'
			/>
			<path
				d='M7.03 9.667c1.48 0 2.68-1.194 2.68-2.667 0-1.473-1.2-2.667-2.68-2.667A2.673 2.673 0 004.35 7c0 1.473 1.2 2.667 2.68 2.667z'
				stroke='#454545'
				strokeLinecap='round'
				strokeLinejoin='round'
			/>
		</svg>
	);
}

export default LocationIcon;
