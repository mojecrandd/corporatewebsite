import * as React from 'react';

function TwitterIcon(props) {
	return (
		<svg
			width={23}
			height={20}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					d='M10.878 6.877C8.874 1.496 15.896-.897 18.554 2.74c0 0 1.03-.037 2.91-1.053 0 0-.447 1.437-1.894 2.388.29-.238 1.1-.007 2.43-.602 0 0-.839 1.364-2.14 2.279 0 0-.03 2.047-.333 3.324-.304 1.278-.812 4.88-3.85 7.378-3.037 2.497-7.244 3.002-9.342 2.857-2.097-.145-4.672-.944-6.335-1.975 0 0 3.804.407 6.61-1.916 0 0-2.835.13-4.18-3.15 0 0 .97.174 2.01-.116 0 0-3.572-.653-3.616-4.515 0 0 1.172.58 2.04.523 0 0-3.414-2.614-1.33-5.996 0 0 3.415 4.545 9.344 4.711z'
					stroke='#fff'
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path fill='#fff' d='M0 0h23v20H0z' />
				</clipPath>
			</defs>
		</svg>
	);
}

export default TwitterIcon;
