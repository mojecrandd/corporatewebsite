import * as React from 'react';

function InstagramFooter(props) {
	return (
		<svg
			width={22}
			height={22}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M20.925 15.925c-.057 1.186-.255 1.836-.424 2.26-.217.565-.49.979-.913 1.403a3.815 3.815 0 01-1.403.913c-.434.17-1.074.367-2.26.424-1.29.056-1.667.075-4.925.075s-3.644-.01-4.925-.075c-1.186-.057-1.836-.255-2.26-.424a3.773 3.773 0 01-1.403-.913 3.816 3.816 0 01-.913-1.403c-.17-.434-.367-1.074-.424-2.26C1.02 14.635 1 14.249 1 11c0-3.249.01-3.644.075-4.925.057-1.186.255-1.836.424-2.26.217-.565.49-.979.913-1.403A3.815 3.815 0 013.815 1.5c.434-.17 1.074-.367 2.26-.424C7.365 1.02 7.751 1 11 1c3.249 0 3.644.01 4.925.075 1.186.057 1.836.255 2.26.424.565.217.979.49 1.403.913.423.424.687.83.913 1.403.17.434.367 1.074.424 2.26C20.98 7.365 21 7.751 21 11c0 3.249-.019 3.644-.075 4.925z'
				stroke='#fff'
			/>
			<path
				d='M11.5 7A4.502 4.502 0 007 11.5c0 2.483 2.017 4.5 4.5 4.5s4.5-2.017 4.5-4.5S13.983 7 11.5 7z'
				stroke='#fff'
			/>
			<path
				d='M17 6a1 1 0 100-2 1 1 0 000 2z'
				fill='#fff'
				stroke='#fff'
			/>
		</svg>
	);
}

export default InstagramFooter;
