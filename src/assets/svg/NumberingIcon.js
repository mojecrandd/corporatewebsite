import * as React from 'react';

function NumberingIcon(props) {
	return (
		<svg
			width={23}
			height={22}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path fill='#13376A' d='M8 7h15v15H8z' />
			<path fill='#6D92C8' d='M0 0h15v15H0z' />
		</svg>
	);
}

export default NumberingIcon;
