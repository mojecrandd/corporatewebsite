import * as React from 'react';

function PlayIcon(props) {
	return (
		<svg
			width={305}
			height={305}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				clipRule='evenodd'
				d='M152.5 279.583c70.186 0 127.083-56.897 127.083-127.083S222.686 25.416 152.5 25.416 25.416 82.315 25.416 152.5 82.315 279.583 152.5 279.583z'
				stroke='#FCFDFF'
				strokeWidth={2}
				strokeLinecap='round'
				strokeLinejoin='round'
			/>
			<path
				clipRule='evenodd'
				d='M127.083 101.667l76.251 50.833-76.251 50.833V101.667z'
				stroke='#FCFDFF'
				strokeWidth={2}
				strokeLinecap='round'
				strokeLinejoin='round'
			/>
		</svg>
	);
}

export default PlayIcon;
