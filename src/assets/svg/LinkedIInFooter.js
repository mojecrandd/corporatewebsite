import * as React from 'react';

function LinkedInFooter(props) {
	return (
		<svg
			width={24}
			height={23}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				clipRule='evenodd'
				d='M23 13.889v8.134h-4.715v-7.59c0-1.906-.683-3.207-2.389-3.207-1.304 0-2.079.877-2.42 1.725-.125.303-.156.725-.156 1.15v7.921H8.604s.063-12.853 0-14.184h4.716v2.01l-.031.046h.03v-.046c.626-.965 1.746-2.344 4.251-2.344 3.104 0 5.43 2.027 5.43 6.385zM3.48 1C1.981 1 1 2.064 1 3.464c0 1.37.952 2.467 2.422 2.467h.03c1.528 0 2.479-1.097 2.479-2.467C5.902 2.064 4.981 1 3.481 1zm-2.2 21.022h4.715V7.838H1.28v14.184z'
				stroke='#fff'
				strokeLinecap='round'
				strokeLinejoin='round'
			/>
		</svg>
	);
}

export default LinkedInFooter;
