import * as React from 'react';

function CareerIcon(props) {
	return (
		<svg
			width={54}
			height={30}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				d='M27 14.483A6.992 6.992 0 1027 .5a6.992 6.992 0 000 13.983z'
				stroke='#454545'
			/>
			<path
				d='M14.22 29.085c1.313-8.226 5.506-12.34 12.579-12.34s11.4 4.114 12.98 12.34'
				stroke='#454545'
				strokeLinecap='round'
			/>
			<path
				d='M10.254 13.602a5.229 5.229 0 100-10.458 5.229 5.229 0 000 10.458z'
				stroke='#454545'
			/>
			<path
				d='M1 28.203c1.083-7.05 4.543-10.576 10.378-10.576 1.365 0 2.607.193 3.724.579'
				stroke='#454545'
				strokeLinecap='round'
			/>
			<path
				d='M42.424 13.602c-3.132 0-5.67-2.341-5.67-5.23 0-2.887 2.538-5.228 5.67-5.228 3.13 0 5.67 2.341 5.67 5.229s-2.54 5.229-5.67 5.229z'
				stroke='#454545'
			/>
			<path
				d='M53 27.322c-1.083-7.05-4.542-10.576-10.378-10.576-1.365 0-2.606.193-3.724.579'
				stroke='#454545'
				strokeLinecap='round'
			/>
		</svg>
	);
}

export default CareerIcon;
