import * as React from 'react';

function DividerArrowDown(props) {
	return (
		<svg
			width={30}
			height={15}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					d='M29.5 1L15.465 14 .5 1'
					stroke='#c2c1c1'
					strokeLinecap='square'
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path
						fill='#fff'
						transform='rotate(-180 15 7.5)'
						d='M0 0h30v15H0z'
					/>
				</clipPath>
			</defs>
		</svg>
	);
}

export default DividerArrowDown;
