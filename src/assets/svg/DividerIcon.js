import * as React from 'react';

function DividerIcon(props) {
	return (
		<svg
			width={18}
			height={12}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<path
				stroke='#fff'
				strokeWidth={1.5}
				d='M.886 10.607l16.478-9.513'
			/>
		</svg>
	);
}

export default DividerIcon;
