import * as React from 'react';

function TwitterFooter(props) {
	return (
		<svg
			width={23}
			height={20}
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
			{...props}
		>
			<g clipPath='url(#prefix__clip0)'>
				<path
					d='M10.72 6.77C8.744 1.487 15.664-.863 18.283 2.71c0 0 1.016-.037 2.869-1.035 0 0-.442 1.411-1.867 2.345.285-.234 1.083-.007 2.394-.591 0 0-.827 1.34-2.11 2.237 0 0-.028 2.01-.327 3.264-.3 1.255-.8 4.792-3.794 7.244-2.993 2.451-7.138 2.947-9.205 2.805-2.067-.143-4.604-.927-6.243-1.939 0 0 3.749.4 6.514-1.881 0 0-2.794.128-4.12-3.093 0 0 .955.171 1.982-.114 0 0-3.52-.641-3.564-4.433 0 0 1.155.57 2.01.513 0 0-3.364-2.565-1.311-5.886 0 0 3.366 4.462 9.209 4.625z'
					stroke='#fff'
				/>
			</g>
			<defs>
				<clipPath id='prefix__clip0'>
					<path fill='#fff' d='M0 0h23v20H0z' />
				</clipPath>
			</defs>
		</svg>
	);
}

export default TwitterFooter;
