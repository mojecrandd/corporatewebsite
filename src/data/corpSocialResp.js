import csr1 from '../assets/images/csr1.jpg';
import csrCross from '../assets/images/csrCross.jpg';
import csrMeal from '../assets/images/csrMeal.jpg';

export const quotes = [
	{
		quoteWord:
			'Life’s most persistent and urgent question is, ‘What are you doing for others?’',
		quoteBy: 'Martin Luther King Jr., civil rights activist and clergyman',
		bgColor: 'bg-black bg-opacity-75',
	},
	{
		quoteWord: 'If you can’t feed a hundred people, feed just one.',
		quoteBy: 'Mother Teresa,  founder of The Missionaries of Charity',
		bgColor: 'bg-brand-blue',
	},
	{
		quoteWord:
			'When we live in a world that is very unjust, you have to be a dissident.',
		quoteBy:
			'Nawal El Saadawi, Egyptian feminist, writer, and psychiatrist',
		bgColor: 'bg-blue-900',
	},
];

export const data = [
	{
		img: csr1,
		altText: 'mama and children',
		headerText: 'Mama at the foundation',
		descText:
			'Delegates from Lagos State Ministry of Energy and Mineral Resources led by the Hon. Commissioner, Engr. Olalere Odusote.',
	},
	{
		img: csrCross,
		altText: 'covid',
		headerText: 'COVID relief fund donation',
		descText:
			'AS a company, we priotise the welfare of our immediate environs',
	},
	{
		img: csrMeal,
		altText: 'meal',
		headerText: 'Feed a child',
		descText:
			'Our feed a child program is geared towards reducing hunger and alleviating povety in Nigeria',
	},
];
