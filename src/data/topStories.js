import visit1 from '../assets/images/visit1.jpg';
// import team_winner from '../assets/images/team_winner.jpg';
// import smart_metr2 from '../assets/images/smart_metr2.jpg';
import VP from '../assets/images/VP.jpg';
import fashola from '../assets/images/fashola.png';

export const data = [
	{
		img: VP,
		altText: 'VP visit to mojec',
		headerText:
			'Speaking shortly after the commissioning, Osinbajo described Mojec’s metering endeavors as a big achievement and milestone that would create thousands of job for Nigerians.',
		descText:
			'Vice President, Federal Republic of Nigeria, Professor Yemi Osinbajo officially paid a visit to Mojec International Limited - Nigeria’s leading smart meters manufacturing company, to visit its world-class meter factory as well as the inauguration of its meter box and other plastics factory constructed with the aim of addressing the shortfall in meter accessibility by end users in Nigeria.',
	},
	{
		img: fashola,
		altText: 'fashola visit',
		headerText:
			'Fashola Inspects MOJEC International Meter Manufacturing Company In Lagos',
		descText:
			'Federal Minister of Works and Housing, Federal Republic of Nigeria, Babatunde Raji Fashola (SAN) officially paid a visit to Mojec International Limited - Nigeria’s leading smart meters manufacturing company, to inspect newly commissioned meters',
	},
	{
		img: visit1,
		altText: 'lagos state ministry of energy visit',
		headerText:
			'Visit from Lagos State Ministry of Energy and Mineral Resources',
		descText:
			'Delegates from Lagos State Ministry of Energy and Mineral Resources led by the Hon. Commissioner, Engr. Olalere Odusote.',
	},
];
