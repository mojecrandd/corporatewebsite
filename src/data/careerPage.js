import careergrowth from '../assets/images/careergrowth.jpg';
import joinUS from '../assets/images/r&d.jpg';
import fraud from '../assets/images/fraud.jpg';

export const images = [
	{
		img: careergrowth,
		tag: 'jobs',
		headerContent: 'join us',
		descContent:
			'Explore opportunities at several Mojec international companies, aggregated for you.',
	},
	{
		img: joinUS,
		tag: 'opportunity',
		headerContent: 'internal opportunities for employees',
		descContent:
			'If you are presently a Mojec employee, explore growth and opportunities at other Tata group companies.',
	},
	{
		img: fraud,
		tag: 'disclaimer',
		headerContent: 'beware of fake job scam',
		descContent:
			'We do not charge/accept any amount or security deposit from job seekers during the selection process or while inviting candidates for an interview.',
	},
];
