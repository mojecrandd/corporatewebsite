import randd from '../assets/images/r&d.jpg';
import energy from '../assets/images/energy.jpg';
import mojecOverlay from '../assets/images/mojecOverlay.jpg';

export const videos = [
	{
		img: mojecOverlay,
		altText: 'consumer playlist',
		title: 'MOJEC: The Consumer’s Delight',
		videoId: 'bLVHGK-Gj1I',
	},
	{
		img: energy,
		altText: 'energy playlist',
		title: 'Energy in mojec',
		videoId: 'IhakLDQhLeo&t=11s',
	},
	{
		img: randd,
		altText: 'energy playlist',
		title: 'Energy in in Power',
		videoId: 'qkz5LsCThew',
	},
];
