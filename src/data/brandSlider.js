import latestAward from '../assets/images/latestAwd.jpg';
import award9 from '../assets/images/award9.jpg';
import smart_metr2 from '../assets/images/smart_metr2.jpg';
import IMG_4325 from '../assets/images/newSlider/VPvisit/IMG_4325.jpg';
import _IMG_4350 from '../assets/images/newSlider/VPvisit/_IMG_4350.jpg';
import DSC_9229 from '../assets/images/newSlider/CoutVisit/DSC_9229.jpg';
import DSC_9349 from '../assets/images/newSlider/CoutVisit/DSC_9349.jpg';
import DSC_6356 from '../assets/images/newSlider/hackatton/DSC_6356.jpg';
import DSC_6752 from '../assets/images/newSlider/hackatton/DSC_6752.jpg';

export const data = [
	{
		img: latestAward,
		altText: 'award dinner',
		headerText: 'Award',
		title:
			'MOJEC certificate from the London Stock Exchange List of Companies that Inspire Africa 2019',
	},
	{
		img: smart_metr2,
		altText: 'smart meter',
		headerText: 'smart meter',
		title: 'The MD/CEO at the Lagos SmartMeter Hackathon 2020',
	},
	{
		img: award9,
		altText: 'award dinner',
		headerText: 'Award',
		title:
			'The MD receives her award for “The Most Enterprising Female of the Year, 2018”',
	},
	{
		img: DSC_9229,
		altText: 'courtesy visit',
		headerText: 'Award',
		title:
			'A courtesy visit to MOJEC International Limited Meter Factory by the delegations from the Lagos State Ministry of Energy and Mineral Resources.',
	},
	{
		img: DSC_9349,
		altText: 'courtesy visit',
		headerText: 'Award',
		title:
			'A courtesy visit to MOJEC International Limited Meter Factory by the delegations from the Lagos State Ministry of Energy and Mineral Resources.',
	},
	{
		img: IMG_4325,
		altText: 'vp visit',
		headerText: 'Award',
		title:
			"Professor Yemi Osinbajo's visit to MOJEC International Limited meter factory",
	},
	{
		img: _IMG_4350,
		altText: 'vp visit',
		headerText: 'Award',
		title:
			"Professor Yemi Osinbajo's visit to MOJEC International Limited meter factory ",
	},
	{
		img: DSC_6356,
		altText: 'hackatton',
		headerText: 'Award',
		title:
			'MOJEC MD, Ms. Chantelle Abdul, as one of the judges at the Lagos Smart Meter Hackthon',
	},
	{
		img: DSC_6752,
		altText: 'hackatton',
		headerText: 'Award',
		title:
			'MOJEC MD, Ms. Chantelle Abdul, as one of the judges at the Lagos Smart Meter Hackthon',
	},
];
