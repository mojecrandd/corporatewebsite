import generation from '../assets/images/generation.jpg';
import meter3 from '../assets/images/meter3.jpg';
import real_estate from '../assets/images/real_estate.png';
import retail from '../assets/images/retail.jpg';
import transmission from '../assets/images/transmission.jpg';
import meter2 from '../assets/images/meter2.jpg';
import agriculture from '../assets/images/agriculture.jpg';
import randd from '../assets/images/r&d.jpg';
import energy from '../assets/images/energy.jpg';
import mojecInstaller from '../assets/images/timeline/mojecInstaller.jpg';

export const verticals = [
	{
		id: 1,
		image: generation,
		title: 'Power Generation',
		desc:
			'MOJEC is currently one of the leading company in Nigeria in terms of electricity distribution and power generation',
		link: '/generation',
	},
	{
		id: 2,
		image: mojecInstaller,
		title: 'Power Distribution',
		desc:
			'MOJEC has been able to successfully measure energy from the high tension side to the low tension side of distribution networks across the country',
		link: '/distribution',
	},
	{
		id: 3,
		image: meter3,
		title: 'Energy Management',
		desc:
			'Leverage on MOJEC’s pool of technician, custom made electric items, and cost saving tips',
		link: '/energymanagement',
	},
	{
		id: 4,
		image: real_estate,
		title: 'Real Estate',
		desc:
			'Provision of office and corporate residential leases to foreign and local companies wishing to house their staff',
		link: '/realestate',
	},
	{
		id: 5,
		image: retail,
		title: 'Retail',
		desc:
			'MOJEC is part of an integrated system that makes the supply chain effective',
		link: '/retail',
	},
	{
		id: 6,
		image: transmission,
		title: 'Transmission',
		desc:
			'Our Engineers have designed transmission networks to transport the energy as efficiently as feasible, while at the same time taking into account economic factors, network safety and redundancy',
		link: '/transmission',
	},
	{
		id: 7,
		image: meter2,
		title: 'Mojec Meter Company',
		desc:
			'MOJEC Meter Company pioneered the concept of smart metering technology in Nigeria by setting up a state-of-the-art electricity meter plant in the country with a production capacity of 1,200,000 meters annually designed to serve the local African markets, the EMEA and neighboring markets; fully equipped to handle customer demand.',
		link: '/metering',
	},
	{
		id: 8,
		image: agriculture,
		title: 'Agriculture',
		desc:
			'With a close collaboration with key stakeholders MOJEC drives nation building through her role in Agribusiness.',
		link: '/agriculture',
	},
	{
		id: 9,
		image: randd,
		title: 'Research & Development',
		desc:
			"The MOJEC Group harnesses the skills of the World's finest Engineering Researchers, Developers and Consultants who play a combined role of conceptualizing, designing and implementing industry standard products",
		link: '/r&d',
	},
	{
		id: 10,
		image: energy,
		title: 'Oil and Gas',
		desc:
			'As an indigenous Nigerian oil and gas distribution company with a vision to be the leading distributor of gas to the Nigerian domestic market',
		link: '/energy',
	},
];
