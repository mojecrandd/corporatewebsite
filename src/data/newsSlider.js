import { useState, useEffect } from 'react';
import visit1 from '../assets/images/visit1.jpg';
import team_winner from '../assets/images/team_winner.jpg';
import VP from '../assets/images/VP.jpg';
import DSC_8984 from '../assets/images/newSlider/CoutVisit/DSC_8984.jpg';
import DSC_9106 from '../assets/images/newSlider/CoutVisit/DSC_9106.jpg';
import DSC_9149 from '../assets/images/newSlider/CoutVisit/DSC_9149.jpg';
import DSC_9188 from '../assets/images/newSlider/CoutVisit/DSC_9188.jpg';
import DSC_9200 from '../assets/images/newSlider/CoutVisit/DSC_9200.jpg';
import DSC_9214 from '../assets/images/newSlider/CoutVisit/DSC_9214.jpg';
import DSC_9229 from '../assets/images/newSlider/CoutVisit/DSC_9229.jpg';
import DSC_9349 from '../assets/images/newSlider/CoutVisit/DSC_9349.jpg';
import DSC_6356 from '../assets/images/newSlider/hackatton/DSC_6356.jpg';
import DSC_6752 from '../assets/images/newSlider/hackatton/DSC_6752.jpg';
import DSC_6759 from '../assets/images/newSlider/hackatton/DSC_6759.jpg';
import DSC_6941 from '../assets/images/newSlider/hackatton/DSC_6941.jpg';
import DSC_7080 from '../assets/images/newSlider/hackatton/DSC_7080.jpg';
import IMG_6960 from '../assets/images/newSlider/hackatton/IMG_6960.jpg';
import IMG_6982 from '../assets/images/newSlider/hackatton/IMG_6982.jpg';
import _MG_4061 from '../assets/images/newSlider/VPvisit/_MG_4061.jpg';
import _MG_4068 from '../assets/images/newSlider/VPvisit/_MG_4068.jpg';
import IMG_4325 from '../assets/images/newSlider/VPvisit/IMG_4325.jpg';
import _IMG_4350 from '../assets/images/newSlider/VPvisit/_IMG_4350.jpg';

export const useTwitter = () => {
	const [tweets, setTweets] = useState(null);
	const [isError, setIsError] = useState(false);
	const [isLoading, setIsLoading] = useState(false);

	useEffect(() => {
		setIsLoading(true);
		fetch('https://mojecstoreapi.herokuapp.com/api/userTimeline')
			.then((response) => response.json())
			.then((response) => {
				setIsLoading(false);
				setIsError(false);
				setTweets(response);
			})
			.catch((err) => {
				setIsLoading(false);
				setIsError(true);
			});
		return () => { };
	}, []);

	return [tweets, isError, isLoading];
};

export const data = [
	{
		img: visit1,
		altText: 'lagos state ministry of energy visit',
		headerText:
			'Visit from Lagos State Ministry of Energy and Mineral Resources',
		descText:
			'A courtesy visit to MOJEC International Limited Meter Factory by the delegations from the Lagos State Ministry of Energy and Mineral Resources.',
		images: [
			DSC_8984,
			DSC_9106,
			DSC_9149,
			DSC_9188,
			DSC_9200,
			DSC_9214,
			DSC_9229,
			DSC_9349,
		],
		details: [
			{
				id: 1,
				desc:
					'During a courtesy visit by a delegation from the Lagos State Ministry of Energy and Mineral Resources, the MD/CEO of MOJEC International, Ms Chantelle Abdul, reiterated the firm’s commitment in solving issues around metering since its inception.',
			},
			{
				id: 2,
				desc:
					'Ms. Abdul makes her intention known on partnership with the state government to ensure the transformation of the energy sector, closing the metering gap and providing affordable meters to electricity consumers.',
			},
			{
				id: 3,
				desc:
					'As described by her, “Lagos State stands as the state that contributes the most to the national GDP, and as such, any solutions to a nationwide problem should be coming out of Lagos. I couldn’t be happier that you have owned and pioneered providing solutions to the challenges affecting the metering segment of the energy sector.',
			},
			{
				id: 4,
				desc:
					'I am excited the government is having discussions with us to bridge the metering gap through local manufacturers as opposed to completely relying on importation. Importing meters into the country will not close the gap as fast as everyone thinks and this is because procurement is not the issue but the manpower and other factors that go into getting these meters on the walls of the end users.',
			},
			{
				id: 5,
				desc:
					'Therefore, in our engagement with the government, we continue to let them know that manufacturers are available in the country and we are able to produce these meters to the quantum of the immediate need of the country.',
			},
			{
				id: 6,
				desc:
					'Also speaking, the leader of the delegation and Commissioner for Energy and Mineral Resources, Mr. Olalere Odusote, congratulated MOJEC on its strides and the accolades it has been receiving.',
			},
			{
				id: 7,
				desc:
					'As said by Odusote, “What I have seen here is interesting, it is very impressive for us. We like the fact that there is a local company that is looking at helping us to solve our local problems rather than trying to import our way out of the problem.”',
			},
			{
				id: 8,
				desc:
					'While speaking on the next step for the Lagos Smart Meter hackathon winners, he stated that their meter prototypes will be refined after which a few meters will be produced for testing, adding that the next phase after that will be to engage meter manufacturers on how the IP can be leased to them for mass production.',
			},
			{
				id: 9,
				desc:
					'MOJEC’s commitment to close the metering gap was further reiterated recently with the sponsorship of the Lagos Smart Meter Hackathon, an initiative of the Lagos State Government, created to leverage the availability of local talents in Nigeria to develop and produce affordable smart electricity meters for consumers.',
			},
			{
				id: 10,
				desc:
					'Aside from closing the meter gap, this will also help in reducing revenue losses and ultimately improve last-mile electricity supply.',
			},
			{
				id: 11,
				desc:
					'MOJEC International is the largest meter manufacturer in Nigeria. As a leading indigenous manufacturer and contractor to power utilities in Nigeria and across the western hemisphere, MOJEC has an installed production capacity of up to four million meters annually.',
			},
			{
				id: 12,
				desc:
					'Presently, MOJEC remains at the forefront of metering technology and boasts over 60 per cent market penetration rate in the Nigerian electricity market with over nine out of 11 of the utilities as clients.',
			},
			{
				id: 13,
				desc:
					'Ms. Abdul explained that, “With ambitious future plans for growth, we aim to further entrench our leadership in the Nigerian power sector through research and development, training in human capital and investment in infrastructure, our people, products and processes.” ',
			},
		],
	},
	{
		img: team_winner,
		altText: 'lagos smart meter hackathon',
		headerText: 'Lagos state smart meter hackathon',
		descText:
			'MOJEC MD, Ms. Chantelle Abdul, as one of the judges at the Lagos Smart Meter Hackthon, an initiative of the Lagos State Government. Also, MOJEC donates a sum of two million naira to the first runner-up at the Hackathon ',
		images: [
			DSC_6356,
			DSC_6752,
			DSC_6759,
			DSC_6941,
			DSC_7080,
			IMG_6960,
			IMG_6982,
		],
		details: [
			{
				id: 1,
				desc:
					'MOJEC International Limited donates two million Naira to the first runner-up at the just concluded Lagos Smart Meter Hackathon,  an initiative of the Lagos State government in collaboration with the Eko Innovation Centre to engage local technological talents to design and produce smart and affordable meters in order to bridge the metering gap in Lagos State and Nigeria as a whole.',
			},
			{
				id: 2,
				desc:
					'As an actualization of its commitment to bridging the metering gap in the Nigerian power sector, MOJEC International did not hesitate to sponsor the hackathon at the Silver level. Two hundred and seven (274) participants registered for the hackathon in the software and hardware categories while only 65 hardware and software prototypes were submitted by applicants, with 10 teams emerged as the finalists. The shortlisted finalists under the hardware category were: Cosmo Automation, Smart Energy, Techwizard, Power Bit Crunchers, and Gadozz Electricals. And the software finalists were Vectorians, Zeena Platform, Magnitronics, Chosen Soft-Tech, and Gideon. The 3-day event which was held on the 16th-18th of September 2020 had the 10 shortlisted finalists pitching their innovative hardware and software ideas to a panel of 8 judges, one of which was Chantelle Abdul, Managing Director and CEO of MOJEC. ',
			},
			{
				id: 3,
				desc:
					'While speaking at the Grand Finale of the hackathon on behalf of the judges, Ms Chantelle Abdul expressed her delight and satisfaction with the exceptionally innovative ideas and creations of the finalist teams. “I am quite thrilled that Nigeria has the capacity and technological know-how to produce meters in Nigeria. It is a dream come true. These past 2 days have been eye-opening. What I am truly elated about is that I saw young Nigerians do brilliantly, in a short period of time do what we have tried to spend months sourcing out of Nigeria. I must also say that we as manufacturers and the DisCos are committed to developing this industry in Nigeria and ensuring that we continue to create jobs by producing meters in the country. I am super excited today that a lot of what we procure from China are actually purchased and available here in Nigeria. “ After a rigorous assessment by the panel of judges, Team Power Bit Crunchers (hardware) and Team Zeena Platform emerged winners of the hackathon. They will be working together to create smart and affordable smart meters. The first runners-up are Team Magnitronics and Team Cosmo Automation, who will be going home with 2 Million Naira donated by Ms. Chantelle. Although the grand prize was raised from N7 million to N10 million by the opening of the Grand Final, Ms. Abdul made a generous donation for the first runners-up. This is in a bid to appreciate and encourage the efforts of the teams. In her words “We are always only looking at the winner but the truth of the matter is everybody else tries so hard. Therefore, I would like to, on behalf of my firm, donate a sum of N2 million for the first runner up.”',
			},
		],
	},
	{
		img: VP,
		altText: 'vp visit',
		headerText:
			"Professor Yemi Osinbajo's visit to MOJEC International Limited meter factory",
		descText:
			'An official visit by the Vice President, Federal Republic of Nigeria, Professor Yemi Osinbajo to MOJEC International Limited meter factory and to inaugurate the meter box and other plastics factory constructed to address the shortfall in meter accessibility by end-users in Nigeria on Friday, August 10, 2018.',
		images: [_MG_4061, _MG_4068, IMG_4325, _IMG_4350],
		details: [
			{
				id: 1,
				desc:
					'The inauguration took place on Friday, August 10, 2018 at the company facility in Lagos.',
			},
			{
				id: 2,
				desc:
					'Speaking shortly after the commissioning, Osinbajo described Mojec’s metering endeavors as a big achievement and milestone that would not only boost local capacity but also create thousands of job for Nigerians.',
			},
			{
				id: 3,
				desc:
					'He added ”Let me congratulate the management and staff of Mojec International limited for their desire and dedication to this project. It gladdens my heart that all our efforts in engaging the private sector is materializing and this is really indicative of real progress and quantum leap for the Nigerian economy.',
			},
			{
				id: 4,
				desc:
					'While expressing optimism on the determination of government to achieve its target in the area of power generation, he noted that the facility is a huge step forward for the development of the power sector noting that it would help significantly in addressing the 6million energy meter deficit.',
			},
			{
				id: 5,
				desc:
					'Osinbajo who acknowledged and noted that the company was 100% local, promised that government will give its support and continue to provide the enabling environment for manufacturers such as Mojec to thrive noting that works are in progress to introduce cheaper financing that would enable them produce meters that can meet demand.',
			},
			{
				id: 6,
				desc:
					'In observing several of Mojec’s new innovations inmetering, smart homes  and power generation, the Vice President committed that “It is very obvious that Nigerian talent and creativity has proved time and time they can compete with the best anywhere in the world. Here in Mojec, it is very obvious. Practically, every one you see from the factory down to the reception, they are all Nigerians. It shows that we are at the forefront of innovation, manufacturing and human capital development.”',
			},
			{
				id: 7,
				desc:
					'In her remarks, the Managing Director, Mojec International Limited, Chantelle Abdul commended the Vice President for his commitment to promoting local content and expressed the company’s desire to work closely with Federal Government to find lasting solutions to the metering gap and lack of steady power supply in Nigeria.',
			},
			{
				id: 8,
				desc:
					'She stated that the current facilities and factory in Nigeria is capable of producing meters from start to finish.',
			},
			{
				id: 9,
				desc:
					'“This factory has provided enough proof that local companies can produce meters that can meet global standard which could consequently help in reversing government policy on local meter manufacturing” she said.',
			},
			{
				id: 10,
				desc:
					'She appealed to the Federal Government to assist local manufacturers by formulating policies that would make cheap financing accessible and available which could help reduce the cost of meters to Nigerians.',
			},
			{
				id: 11,
				desc:
					'“We commend and applaud NERC for the recently released MAP policy. However, only 70% of meters required are expected to be imported while 30% are to be locally produced. If the Nigerian meters market is about 6million metering gap, this means only 2million meters will be produced locally which can easily be manufactured by MOJEC meters alone.',
			},
			{
				id: 12,
				desc:
					'However, there are over six local manufacturers in the country who through patronage can help create jobs and contribute to our national, knowledge transfer, research and development can enable Nigeria become an innovation hub such that we will be able to supply Nigerian made meters to the rest of Africa and the world.',
			},
			{
				id: 13,
				desc:
					'With this situation, what we are asking is that the policy be reversed as this will enable other local manufacturers produce and create employment while encouraging foreign investors invest in theocuntry directly instead of dumping foreign made meters into the Nigerrian market. By so doing, we would be developing local capacity and creating enough jobs that would contribute greatly to our economy” she said.',
			},
			{
				id: 14,
				desc:
					'Ms. Abdul thanked the office of the Presidency, all the different stakeholders in the power sectors and the management and staff of MOJEC meter company for their support, hardwork and dedication all throughout the period of the project development.',
			},
			{
				id: 15,
				desc:
					'It would be noted that MOJEC International has delivered over 1.2million meter across all regions in Nigeria from CAPMI till date.',
			},
		],
	},
];
