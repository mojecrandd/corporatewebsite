export const awards = [
	{
		id: 1,
		type: 'achievement',
		desc: 'Largest Meter Manufacturer  & Distribution Network in Nigeria',
	},
	{
		id: 2,
		type: 'award',
		desc:
			'Nigerian Advancement  Awards for Labour Friendly Organisafion of the Year (2015)',
	},
	{
		id: 3,
		type: 'achievement',
		desc: '80% footprint in the  Nigerian Meter Market',
	},
	{
		id: 4,
		type: 'award',
		desc:
			'Association of Professional Women  Engineers of Nigeria Merit Award for OUTSTANDING Professional  Performance and Courtesy to  Human Capital Development (2015)',
	},
	{
		id: 5,
		type: 'achievement',
		desc: '2 Million Annual Production Capacity',
	},
	{
		id: 6,
		type: 'award',
		desc:
			'LARGEST AMI infrastructure and deployment nation wide with almost 1,000,000 smart meters in circulation',
	},
	{
		id: 7,
		type: 'achievement',
		desc: '8/11 Utilities as our  Clients',
	},
	{
		id: 8,
		type: 'award',
		desc: 'LARGEST Installation Network  Nationwide',
	},
	{
		id: 9,
		type: 'achievement',
		desc: '1.2 Million Homes use Mojec Product',
	},
	{
		id: 10,
		type: 'award',
		desc: 'LEADING partner to smaller  power users',
	},
	{
		id: 11,
		type: 'achievement',
		desc: 'First and largest  provider of  vendor ﬁnancing',
	},
];
