import award1 from '../assets/images/timeline/award1.jpg';
import founded from '../assets/images/timeline/founded.jpg';
import mdMTN from '../assets/images/timeline/mdMTN.jpg';
import award2 from '../assets/images/timeline/award2.jpg';
import carbon from '../assets/images/timeline/carbon.jpg';
import meterCheck from '../assets/images/timeline/meterCheck.jpg';
import meterMan from '../assets/images/timeline/meterMan.jpg';
import transformer from '../assets/images/timeline/2002.jpg';
import production from '../assets/images/timeline/2014.jpg';
import million1 from '../assets/images/timeline/2018.jpg';
import million2 from '../assets/images/timeline/2021.jpg';

export const timeline = [
	{
		id: 1,
		year: 1985,
		desc: 'MOJEC international was established',
		image: founded,
	},
	{
		id: 2,
		year: 2000,
		desc:
			'MOJEC established a Calcium Carbonate and Plastic Production Factory',
		image: carbon,
	},
	{
		id: 3,
		year: 2001,
		desc: 'MOJEC started supply of transformers to NEPA',
		image: transformer,
	},
	{
		id: 4,
		year: 2002,
		desc: 'MOJEC secured contracts to supply meters to NEPA',
		image: meterCheck,
	},
	{
		id: 5,
		year: 2012,
		desc: 'MOJEC established the Mojec Meter Factory',
		image: meterMan,
	},
	{
		id: 6,
		year: 2014,
		desc:
			'Increased meter production capacity from 400,000 to 600,000 per annum',
		image: production,
	},
	{
		id: 7,
		year: 2016,
		desc: "MOJEC's CEO won the best African Power Industry Award",
		image: mdMTN,
	},
	{
		id: 8,
		year: 2018,
		desc: "MOJEC's CEO wins the 2018 Nigeria Entrepreneurs Award",
		image: award1,
	},
	{
		id: 9,
		year: 2018,
		desc:
			'Increased meter production capacity from 600,000 to 1.2 Million per annum',
		image: million1,
	},
	{
		id: 10,
		year: 2019,
		desc:
			'London Stock Exchange Group lists MOJEC amongst companies to inspire Africa',
		image: award2,
	},
	{
		id: 11,
		year: 2021,
		desc:
			'Increased meter production capacity from 1.2 Million to 2.5 Million per annum',
		image: million2,
	},
];
