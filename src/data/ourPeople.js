import MD from '../assets/images/management/MD.jpg';
import Alirat from '../assets/images/management/Alirat.jpg';
import Akeem from '../assets/images/management/Akeem.jpg';
import Emmanuel from '../assets/images/management/Emmanuel.jpg';
import Kikelomo from '../assets/images/management/Kikelomo.jpg';
import Koku from '../assets/images/management/Koku.jpg';
import Micheal from '../assets/images/management/Micheal.jpg';
import Solanke from '../assets/images/management/Solanke.jpg';

export const data = [
	{
		id: 1,
		image: Alirat,
		name: 'Mojisola Abdul',
		position: 'executive chairman',
		header:
			'Mojisola Alirat Abdul is Chairman of the board at MOJEC International, the holding  company and promoter of all MOJEC Group companies.',
		details: [
			{
				id: 1,
				desc:
					'Mrs. Abdul is also the Chairman of MOJEC Oil and Gas; a company recently registered and is an already known name in the Oil and Gas sector in Nigeria. She is also the President of Glory Group International.',
			},
			{
				id: 2,
				desc:
					'She started out as a diligent employee of WEMA Bank PLC, a Nigerian financial institution and moved on to start her own business in Agbeni, Ibadan, Nigeria.',
			},
			{
				id: 3,
				desc:
					'With a desire for laudable accomplishments, fueled by hard work and passion, she became a major distributor for the Nigerian National Supply Company (NNSC); West African Milk Company (Friesland Foods); Michelin Tyres, where she won Michelin ambassador for three consecutive Years(1993-1995).',
			},
			{
				id: 4,
				desc:
					'She then became a major distributor to Power Holding Company of Nigeria PLC (PHCN). She supplied Technical Electrical Power Distribution Materials, such as Metering, Cables, Transformer and Transformer Oil, Surge Arrester and Transmission product such as: ACRS Cable, Galvanizes Steel wire, Glass Disc insulator, general electrical products and general merchant.',
			},
			{
				id: 5,
				desc:
					'In addition to her professional career, MOJEC received ECOWAS INTERNATIONAL GOLD AWARD in recognition of her contribution and achievements in the ECOWAS Sub-region, dated 27 May 2000 and has become a house hold name in both local and international energy and power trade fair.',
			},
			{
				id: 6,
				desc:
					'Mrs Mojisola Abdul has always demonstrated uncommon vision, foresight and resourcefulness in the management of human and material resources. ',
			},
			{
				id: 7,
				desc:
					'Her humanitarian efforts and willingness to effect real change in the World can be seen in the establishment of her NGO (Almighty God Compassion Home), caters for abandoned kids, homeless and helpless young boys. There are over seventy boys in her care presently at the compassion home and using this platform, she ensures that kids who desire an education will get the best there is and those who want to learn a trade, will also have the means to do so.',
			},
		],
	},
	{
		id: 2,
		image: MD,
		name: 'Chantelle Abdul',
		position: 'Managing Director',
		header:
			'Chantelle Oluwabunmi Abdul is the Managing Director/Chief Executive Officer of the MOJEC Group of Companies.',
		details: [
			{
				id: 1,
				desc:
					'A consummate business woman, key power sector stakeholder and serial entrepreneur whose experience and reach  traverses the globe, Ms Abdul presides over MOJEC’s business verticals that are leaders in their respective industries namely: MOJEC Power, MOJEC Meter Company, MOJEC Real Estate, Agriculture and the firm\'s latest venture: "Innov8" - The African Innovations Fund. ',
			},
			{
				id: 2,
				desc:
					"Under Ms. Abdul’s leadership in the Post-Power Sector privatization era, the firm's footprint and market share has grown from 20% to 80%, with eight out of the 11 utilities in the country as clients. She built a Nationwide network of Engineers and installers across her 8 Distribution client zones with a workforce of over 3000 that saw the deployment of MOJEC's Smart meters into hundreds of thousands of Nigerian homes.",
			},
			{
				id: 3,
				desc:
					'Today, MOJEC is not only the leading meter manufacturer and installer under the Government-led metering initiative, Credited Advance Payment for Metering Implementation (CAPMI) to the electricity Distribution companies(DISCOs) but is also the leading large meter roll out partner to major utilities in the Country.',
			},
			{
				id: 4,
				desc:
					'Chantelle Abdul as the Chief Executive Officer of MOJEC Meter Company and MOJEC Power has spearheaded the growth of the once small family business into one of the most Iconic brands in the Nigerian power sector today as well as the largest Smart Meter Manufacturer in Nigeria & possibly West Africa.',
			},
			{
				id: 5,
				desc:
					'With the Corporate mantra: "Building A World of Possibilities" and "Powering Your World " respectively, Ms Abdul has a Vision to transform Nigeria and improve the lives of Nigerians by providing "Power to the People" that would Lead to economic transformation as well as help achieve their dreams.',
			},
			{
				id: 6,
				desc:
					"One of her first achievements as the newly installed CEO of the MOJEC Group was to raise millions of dollars to build Nigeria's largest state-of-the-art Smart Meter Assembly facility with an installed capacity of over a million meters annually and a monthly capacity of 100,000meters monthly.",
			},
			{
				id: 7,
				desc:
					"She aims to position the firm to be one of the few indigenous companies floated on the New York Stock Exchange. To do so, Ms Abdul has set out to build a strong indigenous company with a global outlook. She expanded the firm's portfolio from Meter manufacturing to Power Generation, Transmission and Distribution Services.",
			},
			{
				id: 8,
				desc:
					'Before joining MOJEC, she was the Vice President, Development at The Executive Group (TEG), a Washington based Investment firm known for bi-lateral trade & advisory in critical sectors.',
			},
			{
				id: 9,
				desc:
					'Her first venture into Africa was the acquisition, production and broadcast rights of an African version of Donald Trump’s acclaimed business series titled: The Apprentice Africa across the US, UK and Africa. She later went on to launch her own series called: The Billionaire Executive Series.',
			},
			{
				id: 10,
				desc:
					'Ms. Abdul who graduated Cum Laude from The George Washington University in Washington, D.C, (a Bachelor’s Degree in Biotechnology and Economics), began her career with Solomon Smith Barney in Washington DC in 2002 and later at Morgan Stanley Dean Witter in 2003-2004. She’s in the process of acquiring her Executive MBA from the United States.',
			},
			{
				id: 11,
				desc:
					'She has received several accolades and Awards including CEO of the year award, Female Power sector Pioneer award-among others. In 2015 she was one of the women selected across the Continent by the UN & ECOWAS ECREE to feature in a documentary film about women entrepreneurs blazing the trail in male dominated sectors like the power sector.',
			},
			{
				id: 12,
				desc:
					"She's a frequent speaker across leadership, entrepreneurship and international business circuit. Often asked to feature in industry led conferences, panels and summits, Ms Abdul was nominated to join the President's economic delegation to BEIJING& India often speaking on the Nigerian Power sector. ",
			},
			{
				id: 13,
				desc:
					'In the bid to give back to Africa and its development she partnered with a prestigious US organization, to open HARATIO ALGERS NIGERIA as Well as raising funds that will identify and finance young entrepreneurs focused on innovation.',
			},
		],
	},
	{
		id: 3,
		image: Akeem,
		name: 'Akeem Balogun',
		position: 'Chief Operating Officer',
		header:
			'Akeem Balogun is an experienced Chief Executive Officer / Chief Operating Officer with a track record of directing corporate strategy and embedding robust corporate governance to enable global financial viability.',
		details: [
			{
				id: 1,
				desc:
					'Akeem Balogun’s impeccable strategic and business planning skills as Regional CFO of  WWF’s Eastern and Southern global offices made sure he turned around the lagging profits, poor audit rating and inconsistent financial processes into profitability, improved audit ratings, standardized processes and CHF1.3m+ in total profit.',
			},
			{
				id: 2,
				desc:
					'His leadership skills also set him apart at Pfizer were he served as the Direct Procurement & Intercompany Project Lead, also managing the implementation of the Oracle ERP and rolling out to 10 countries whilst enabling $50m in savings for the firm.',
			},
		],
	},
	{
		id: 4,
		image: Kikelomo,
		name: 'Kikelomo Akinwale ',
		position: 'Marketing Director',
		header:
			'Kikelomo Akinwale is a member of the Board Of Directors and serves as MOJEC’s Chief Marketing Officer. She is directly responsible for promoting MOJEC’s capabilities both locally and internationally.',
		details: [
			{
				id: 1,
				desc:
					'In addition, she is also partly responsible for gathering information on potentially macro or micro issues that may affect the firm.',
			},
		],
	},
	{
		id: 5,
		image: Solanke,
		name: 'Oluwole Solanke',
		position: 'Head IT, Research & Development',
		header:
			'Oluwole Solanke is a proven Electrical Engineer, software specialist and also a visionary that has not hesitated to display his ability to manage multiple projects in metering, IT and others that steer the company forward on the technological forefront.',
		details: [
			{
				id: 1,
				desc:
					'Responsible for MOJEC International’s Technical and Software projects. He’s the Head IT, Research & Development Manager.',
			},
			{
				id: 2,
				desc:
					'With him as the Research & Development Manager, MOJEC is always at the vanguard of the latest and most innovative technologies which sees innovative changes to smart metering and more durable, Anti-tamper and region adaptable smart meters.',
			},
			{
				id: 3,
				desc:
					'With impeccable vision, solid background in software and as the Head IT, he has managed projects that have steered the company forward on the technological forefront. Pioneered the Automatic Meter Reading (AMR) & Advanced Metering Infrastructure (AMI) in Nigeria with over 900,000+ smart meters deployed across the country.',
			},
			{
				id: 4,
				desc:
					'He is a master’s degree holder with an M.Sc. in Computer Science from Babcock University, B.Eng. in Electrical & information Engineering from Covenant University and Certification in Advance Project Management from University of Lagos. He’s also an active recognized associate member of (COREN) The Council For the regulation of Engineering in Nigeria, an associate member of the Nigerian Institute of management, an associate member of the Nigerian Society of Engineers and the (IEEE) International Institute of Electrical Electronics Engineers.',
			},
		],
	},
	{
		id: 6,
		image: Koku,
		name: 'Koku Olayinka',
		position: 'Factory Manager',
		header:
			'Engr. Koku Olayinka Sunday is the factory manager of MOJEC International Limited.',
		details: [
			{
				id: 1,
				desc:
					'He possesses a Bachelor’s degree in Electrical Electronics Engineering, with a specialization in Power and Machines and has a Masters in Information Technology.',
			},
			{
				id: 2,
				desc:
					'He has worked in several companies in Nigeria namely Glaxo-smith Kline Beecham,Cadbury Nigeria plc. and Tulip Cocoa before joining MOJEC International Limited. He also a member of the NATE-OREN.',
			},
			{
				id: 3,
				desc:
					'Mr Olayinka oversees all aspects of factory management including production line, rawmaterial   planning,  personnel  and   productivity   management.   He   is   also  known   forensuring that MOJEC’s products satisfies all Quality and Assurance Standards ordered by her clients.',
			},
		],
	},
	{
		id: 7,
		image: Micheal,
		name: 'Onourah Michael',
		position: 'Head, PMO & Supply Chain.',
		header:
			'Onourah Chinedu Michael is the Head, PMO & Supply Chain of MOJEC International Limited.',
		details: [
			{
				id: 1,
				desc:
					'Michael is an international energy manager with 15+ years of cognate value chain experience, his roles have included functions in business strategy, internal control and Audit, Operations, Business support, Business development and Programs management. Michael is charged with developing, coordinating and executing strategic and operational initiatives focused on new growth opportunities for Mojec Meter Asset management company. (M3AC) He is also responsible for defining, planning and managing groupwide projects, business development and supply chain coordination for Mojec International Limited.',
			},
			{
				id: 2,
				desc:
					'Michael holds a first degree in Biochemistry from Lagos State University plus international certifications in Energy economics, Energy project management and petroleum business development.',
			},
		],
	},
];
