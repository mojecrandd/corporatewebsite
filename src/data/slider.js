import hotel from '../assets/images/slider/hotel.jpg';
import powergenie from '../assets/images/slider/powergenie.jpg';
import mall from '../assets/images/slider/mall.jpg';
import meter1 from '../assets/images/meter1.jpg';
import mojecpower from '../assets/images/slider/mojecpower.jpg';
import mojecrealestate from '../assets/images/slider/mojecrealestate.jpg';
import retailjpg from '../assets/images/slider/retailjpg.jpg';

export const images = [
	{
		img: meter1,
		headerContent: 'MOJEC Meters',
		descContent:
			'MOJEC Meters Company pioneered the concept of smart metering technology in Nigeria by setting up a state-of-the-art electrical meter plant in the country with a production capacity of 1.5million meters annually designed to serve the local African market.',
	},
	{
		img: mall,
		headerContent: 'MOJEC Smart Electric City',
		descContent:
			'Africa’s first private sector driven innovation hub with emphasis on modernity and optimized environment for social, technological and economic capital.',
	},
	{
		img: mojecpower,
		headerContent: 'MOJEC Power',
		descContent:
			'MOJEC Power Company generates, transmits and distributes electricity.',
	},
	{
		img: retailjpg,
		headerContent: 'MOJEC Retail',
		descContent:
			'A must-visit destination for local and foreign shoppers right in the middle of Lagos’ most prestigious address.',
	},
	{
		img: powergenie,
		headerContent: 'Power Genie',
		descContent:
			'Pay your electricity bills on-the-go from the comfort of your home.',
	},
	{
		img: mojecrealestate,
		headerContent: 'MOJEC Real Estate',
		descContent:
			'A consumer-focused and result-driven franchise that is defined by service and expertise… Exceptional Properties, Exceptional Clients',
	},
	{
		img: hotel,
		headerContent: 'MOJEC Suites',
		descContent:
			'Situated in the center of a modern renaissance, every experience invites you to open your mind to what luxury can mean. We offer the thrill of the Nigerian culture, the cities and historically important icons and welcome you to see Africa a little differently.',
	},
];
