import access1 from '../assets/images/products/access1.jpg';
import access2 from '../assets/images/products/access2.jpg';
import postpaid1 from '../assets/images/products/postpaid1.jpg';
import postpaid2 from '../assets/images/products/postpaid2.jpg';
import prepaid1 from '../assets/images/products/prepaid1.jpg';
import prepaid2 from '../assets/images/products/prepaid2.jpg';
import prepaid3 from '../assets/images/products/prepaid3.jpg';

export const prePaid = {
	images: [
		{
			img: prepaid1,
			tag: 'product',
			headerContent: 'Pre-Paid meters',
			descContent: 'Mojec offers top-notch pre-paid meters',
			title: 'Three Phase Mojec Meters',
		},
		{
			img: prepaid2,
			tag: 'product',
			headerContent: 'Pre-Paid meters',
			descContent: 'Mojec offers top-notch pre-paid meters',
			title: 'Dinrail Box',
		},
		{
			img: prepaid3,
			tag: 'product',
			headerContent: 'Pre-Paid meters',
			descContent: 'Mojec offers top-notch pre-paid meters',
			title: 'Dinrail',
		},
	],
	content: [
		{
			id: 1,
			text: 'Three Phase Split Prepayment Energy Meters',
		},
		{
			id: 2,
			text: 'CT Operated Prepayment Meter',
		},
		{
			id: 3,
			text: 'Single Phase Split Keypad Prepayment Energy Meters',
		},
	],
	overview: [
		{
			prodName: 'P12G04 Single Phase DIN-Rail Split Prepayment Meter',
			desc:
				'P12G04 is a DIN RAIL mounting split type single phase prepayment meter operatingwith STS standard encryption algorithm. The meter consists of two parts, the Energymeter and Customer Interface Unit (CIU). The meter is installed in a group set of Six,within an enclosure containing a Data Concentrator Unit (DCU) and hung on a wall(Wall mounted) or on a pole (Pole mounted). The energy meter captures the usageof energy with a premise and communicates with the AMI via the DCU,  and datacan also be read using the CIU',
			list: null,
		},
		{
			prodName: 'Single-Phase Smart Meter S12U6',
			desc:
				'The Energy Meter S12U6 is designed for commercial and residential customers. This smart meter has Communication capabilities over GSM/GPRS, RF, OPTICAL, RS485, the meters are designed to be used for AMI/AMR/AMM and smart grids. It is built to withstand adverse energy fluctuations, weather condition and environmental pollution while maintaining functioning optimally. The S12U6 is designed to sustain loads of up to 240V / 60A without breakdown.',
			list: null,
		},
		{
			prodName: 'P2000-T Three Phase CT/CTVT Smart Meter',
			desc:
				'The A2000-T Smart Energy meter is designed for commercial amd industrial use. It has Communication capabilities over GSM/GPRS, RF, OPTICAL, RS485, the A2000-T meters are designed to be used for AMI/AMR/AMM and smart grids',
			list: [
				{
					id: 1,
					desc:
						'Perfect product for three phase large customer or statistical metering',
				},
				{
					id: 2,
					desc: '3 Phase 3/4 Wire Compatible',
				},
				{
					id: 3,
					desc:
						'57.7/100 - 240/415V Wide Voltage Range for both CT and CTVT',
				},
				{
					id: 4,
					desc:
						'Bi-directional energy and demand registration with 4-quadrant measurement',
				},
				{
					id: 5,
					desc:
						'Time of Use (TOU) control with flexible programming for up-to 8 tariffs',
				},
				{
					id: 6,
					desc:
						'Large non-volatile storage to support 2x load profiles and 4x event logs',
				},
				{
					id: 7,
					desc:
						'Power quality monitoring of harmonics, voltage sag & swell',
				},
				{
					id: 8,
					desc:
						'Supports DLMS, IEC 62056-21, or MODBUS communication protocols',
				},
				{
					id: 9,
					desc:
						'AMR-ready with hot-pluggable internal communication modules',
				},
			],
		},
	],
};
export const postPaid = {
	images: [
		{
			img: postpaid1,
			tag: 'product',
			headerContent: 'Post-Paid meters',
			descContent: 'Mojec offers top-notch post-paid meters',
			title: 'MD Mojec Meters',
		},
		{
			img: postpaid2,
			tag: 'product',
			headerContent: 'Post-Paid meters',
			descContent: 'Mojec offers top-notch post-paid meters',
			title: 'MD Mojec Meters',
		},
	],
	content: [
		{
			id: 1,
			text: 'Three Phase Split Postpaid Energy Meters',
		},
		{
			id: 2,
			text: 'Three Phase Energy Meter',
		},
		{
			id: 3,
			text: 'Single Phase Energy Meter',
		},
		{
			id: 4,
			text: 'Whole Current Energy Meter Meter',
		},
		{
			id: 5,
			text: '11/33kV HT Metering Panel',
		},
	],
	overview: [
		{
			prodName: 'P2000-D Three Phase Whole Current Smart Meter',
			desc:
				'TheP2000D Smart Prepayment Energy meter is designed for commercial, industrial and residential customers. It has Communication capabilities over GSM/GPRS, RF, OPTICAL, RS485, the P2000D meters are designed to be used for AMI/AMR/AMM and smart grids',
			list: [
				{
					id: 1,
					desc:
						'Optimal solution for metering three phase whole current customers',
				},
				{
					id: 2,
					desc: '3 Phase 4 Wire Direct Connected, Up-to 160A Imax',
				},
				{
					id: 3,
					desc: 'Bi-directional Energy and Demand Registration',
				},
				{
					id: 4,
					desc:
						'Optionally with internal contactor for load control or prepayment',
				},
				{
					id: 5,
					desc:
						'Time of Use (TOU) control with flexible programming for up-to 8 tariffs',
				},
				{
					id: 6,
					desc:
						'Supports DLMS, IEC 62056-21, or MODBUS communication protocols',
				},
				{
					id: 7,
					desc: 'AMR-ready with internal communication modules',
				},
			],
		},
	],
};
export const accessorys = {
	images: [
		{
			img: access1,
			tag: 'product',
			headerContent: 'Meter Accessories',
			descContent:
				'Mojec offers top-notch meters accessories for both pre-installation and post-installation maintenance',
			title: 'LV MD',
		},
		{
			img: access2,
			tag: 'product',
			headerContent: 'Meter Accessories',
			descContent:
				'Mojec offers top-notch meters accessories for both pre-installation and post-installation maintenance',
			title: 'Meter Box 2',
		},
	],
	content: [
		{
			id: 1,
			text: 'Handheld Electricity Meter Reader',
		},
		{
			id: 2,
			text: 'Meter Box',
		},
		{
			id: 3,
			text: 'Terminal Block',
		},
		{
			id: 4,
			text: 'Automatic Meter Reading System',
		},
		{
			id: 5,
			text: 'Circuit Breakers',
		},
		{
			id: 6,
			text: 'CT Ring',
		},
		{
			id: 7,
			text: 'Energy Meter Data Concentrator',
		},
	],
	overview: [
		{
			prodName: 'CIU-MH03 Customer Interface Unit',
			desc:
				'CIU (Customer Interface Unit) is used together with split type prepayment keypad meter, with main functions: display, communication, keypad input, credit alarm, etc. The Data Processing Unit consists of MCU and data memory. The Input-output Unit consists of LCD display, keypad, audible and visual alarm and M-bus communication (wired cable).',
			list: null,
		},
		{
			prodName: 'Meter Boxes',
			desc: '',
			list: [
				{
					id: 1,
					desc:
						'High quality outdoor meter boxes tailored for our smart meters',
				},
				{
					id: 2,
					desc:
						'Integrated with CTs, test terminal blocks and/or MCBs',
				},
				{
					id: 3,
					desc:
						'Durable materials include fiber glass, coated steel or stainless steel',
				},
				{
					id: 4,
					desc:
						'Meter reading without opening door and AMR/AMI facility',
				},
			],
		},
	],
};
