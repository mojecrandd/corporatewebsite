import styled from 'styled-components';
import Nav from './Home/Nav';
import { motion } from 'framer-motion';
import ArrowNavigation from './ArrowNavigation';
import FaderWrap from './FaderWrap';

const LayoutWrap = styled(motion.div)`
	background: #efefef;
	.heading {
		top: 30%;
	}
`;

const HeroSectionWrap = styled.section`
	background: linear-gradient(180deg, #111b28 0%, rgba(0, 0, 0, 0.33) 100%);
	height: 103vh;
	z-index: 15;
	position: relative;
	bottom: 72px;

	@media (min-width: 1024px) {
		height: 100vh;
	}
`;

const ContentVaraint = {
	rest: {
		y: 300,
		opacity: 0,
	},

	hover: {
		y: 0,
		opacity: 1,
		transition: {
			delay: 0.5,
			type: 'tween',
			ease: 'easeOut',
			duration: 1.5,
		},
	},

	borderRest: {
		y: 50,
		opacity: 0,
	},

	borderAnimate: {
		y: 0,
		opacity: 1,
		transition: {
			delay: 1.5,
			type: 'tween',
			ease: 'easeOut',
			duration: 0.5,
		},
	},
};

const LayoutGeneric = ({ children, image, title, desc, label, to }) => {
	return (
		<LayoutWrap className='h-auto'>
			<Nav className='z-50 relative' />
			<HeroSectionWrap></HeroSectionWrap>
			<section className='absolute top-0 h-screen w-full'>
				<ArrowNavigation to={to} />
				<motion.img
					src={image}
					className='z-10 h-screen w-full object-cover relative'
					alt='generation image'
				/>
				<div className='flex justify-center'>
					<div className='w-full 2xl:w-8/12'>
						<motion.header
							initial='rest'
							animate='hover'
							variants={ContentVaraint}
							className='absolute z-30 heading flex lg:w-9/12 2xl:w-3/12 flex-col px-4 py-2 lg:px-16'
						>
							<span className='bg-brand-blue flex justify-center items-center w-24 h-8 italic text-sm capitalize text-white mb-4'>
								{label}
							</span>
							<h1 className='text-white text-3xl font-bold lg:text-5xl mb-4 capitalize'>
								{title}
							</h1>
							<section className='flex justify-center items-center'>
								<div className='flex w-full lg:w-5/6 h-auto'>
									<motion.div
										initial='borderRest'
										animate='borderAnimate'
										variants={ContentVaraint}
										className='border-brand-yellow border-l mr-2'
									></motion.div>
									<div className=''>
										<p className='text-white text-xs lg:text-base font-medium mb-2'>
											{desc}
										</p>
									</div>
								</div>
							</section>
						</motion.header>
					</div>
				</div>
			</section>
			<FaderWrap>
				<div className='flex justify-center'>
					<section className='py-2 lg:px-32 px-8 pb-16 w-full 2xl:w-8/12'>
						{children}
					</section>
				</div>
			</FaderWrap>
		</LayoutWrap>
	);
};

export default LayoutGeneric;
