import { motion } from 'framer-motion';
import { useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { data } from '../../data/brandSlider';
import HoverArrowIcon from '../../assets/svg/HoverArrowIcon';
import LeftNavIcon from '../../assets/svg/LeftNavIcon';
import OpenLinkIcon from '../../assets/svg/OpenLinkIcon';
import RightNavIcon from '../../assets/svg/RightNavIcon';

const BrandWrapper = styled.section`
	.slider-wrap {
		transition: transform 0.6s ease-in-out;
		height: 20vh;

		.slider-img-indic {
			transition: all 0.3s ease-in-out;
			flex: 0 0 auto;

			&:hover {
				transform: translateY(-5px);
			}

			.img-overlay2 {
				background: rgba(0, 0, 0, 0.5);
				display: none;
			}
		}

		.arrow-ind {
			display: none;
		}

		.overlay-wrap-main {
			transition: all 0.6s ease-in-out;
		}

		.overlay-wrap-main:hover + .img-overlay2,
		.img-overlay2:hover {
			display: flex;
		}
	}
	.image-wrap {
		height: 80vh;
	}

	.img-overlay {
		background: rgba(0, 0, 0, 0.2);
		transition: transform 0.6s ease-in-out;

		.brand-bg {
			background: #043a5b;
		}
	}

	.active-indic {
		display: flex !important;
	}

	.active-indic-parent {
		transform: translateY(-5px);
	}

	.right-slider {
		transition: all 0.6s ease-in-out;
	}

	.right-slider:hover {
		background: rgba(0, 0, 0, 0.16);
		border: 1px solid #e5bd5c;
	}
`;

const BrandSlider = (props) => {
	const [index, setIndex] = useState(0);
	const [isScroll, setIsScroll] = useState(true);

	const ContentVaraint = {
		rest: {
			x: isScroll ? 300 : -300,
		},

		hover: {
			x: 0,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 0.8,
			},
		},

		borderRest: {
			opacity: 0,
		},

		borderAnimate: {
			opacity: 1,
			transition: {
				delay: 0.5,
				type: 'tween',
				ease: 'easeOut',
				duration: 0.5,
			},
		},
	};

	const ref = useRef(null);
	const handleScroll = (scrollOffset) =>
		(ref.current.scrollLeft += scrollOffset);

	return (
		<BrandWrapper className='h-screen' {...props}>
			<div className=''>
				<section className='image-wrap w-full relative bg-black overflow-hidden'>
					<motion.img
						initial='rest'
						animate='hover'
						variants={ContentVaraint}
						exit={{ visibility: 'hidden' }}
						key={data[index].img}
						exit={{ visibility: 'hidden' }}
						src={data[index].img}
						alt='meter'
						className='h-full object-cover w-full'
					/>
					<div className='absolute top-0 h-full w-full img-overlay z-10 flex items-center justify-center px-4 py-2 lg:px-8'>
						<motion.section
							className='h-10 w-full 2xl:w-8/12'
							initial='borderRest'
							animate='borderAnimate'
							variants={ContentVaraint}
							key={data[index].img}
							exit={{ visibility: 'hidden' }}
						>
							<span className='brand-bg flex justify-center items-center w-40 h-8 italic text-sm capitalize text-white mb-4'>
								MOJEC in Pictures
							</span>
							<Link
								to=''
								className='flex items-center ml-4 mb-4 w-4/5 lg:w-1/2'
							>
								<h3 className='capitalize text-white text-xl lg:text-3xl font-semibold mr-4'>
									{data[index].title
										? data[index].title
										: data[index].headerText}
								</h3>
								<div className='w-6 h-6'>
									<OpenLinkIcon />
								</div>
							</Link>
						</motion.section>
					</div>
				</section>
				<section className='slider-wrap w-full bg-black flex px-4 py-2 lg:px-0 justify-center'>
					<div className='flex w-full lg:w-10/12 2xl:w-8/12 justify-center items-center'>
						<span
							className='w-12 h-12 bg-brand-yellow right-slider cursor-pointer flex justify-center items-center'
							onClick={() => handleScroll(-200)}
						>
							<LeftNavIcon color='#ffffff' />
						</span>
						<section
							className='px-4 w-full mx-4 py-2 h-full overflow-x-auto scrollbar-hidden'
							ref={ref}
						>
							<ul className='flex h-full items-center space-x-2 justify-between'>
								{data.map((data, i) => (
									<li
										className={`h-full w-32 slider-img-indic relative flex flex-col items-center whitespace-nowrap cursor-pointer ${
											index === i
												? 'active-indic-parent'
												: ''
										}`}
										key={i}
										onClick={() => setIndex(i)}
									>
										<section className='overlay-wrap-main'>
											<HoverArrowIcon
												className={`absolute -top-3 arrow-ind ${
													index === i
														? 'active-indic'
														: ''
												}`}
											/>
											<img
												src={data.img}
												alt={data.altText}
												className='h-20 object-cover w-full'
											/>
										</section>
										<div
											className={`absolute w-full h-full img-overlay2 z-10 top-0 flex items-center justify-center ${
												index === i
													? 'active-indic'
													: ''
											}`}
										>
											<h5 className='capitalize block text-center font-bold text-white'>
												{data.headerText}
											</h5>
										</div>
									</li>
								))}
							</ul>
						</section>
						<span
							className='w-12 h-12 bg-brand-yellow right-slider cursor-pointer flex justify-center items-center'
							onClick={() => handleScroll(200)}
						>
							<RightNavIcon color='#ffffff' />
						</span>
					</div>
				</section>
			</div>
		</BrandWrapper>
	);
};

export default BrandSlider;
