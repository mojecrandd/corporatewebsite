import { AnimatePresence, motion } from 'framer-motion';
import styled from 'styled-components';
import { data } from '../../data/topStories';
import DividerBendIcon from '../../assets/svg/DividerBendIcon';
import DividerIcon from '../../assets/svg/DividerIcon';
import LeftNavIcon from '../../assets/svg/LeftNavIcon';
import RightNavIcon from '../../assets/svg/RightNavIcon';
import { useState } from 'react';
import FaderWrap from '../FaderWrap';

const TopWrapper = styled.section`
	height: 110vh;

	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: -1.5rem;
		left: 1rem;
	}

	.borderWrap {
		position: absolute;
		border-left: 0.2rem solid #e5bd5c;
		border-top: 0.2rem solid #e5bd5c;
		height: 15rem;
		width: 20rem;
		top: -0.2rem;
		left: -0.2rem;
	}

	.nav-hover {
		transition: all 0.5s ease-in-out;
	}

	.nav-hover:hover {
		background: rgba(0, 0, 0, 0.29);
	}

	@media (min-width: 1024px) {
		height: 100vh;
	}

	@media (min-width: 1536px) {
		h2 {
			left: 34rem;
		}
	}
`;

const TopStories = (props) => {
	const [newsIndex, setNewsIndex] = useState(0);
	const [isScroll, setIsScroll] = useState(true);

	const ContentVaraint = {
		rest: {
			x: isScroll ? 300 : -300,
		},

		hover: {
			x: 0,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 0.8,
			},
		},

		borderRest: {
			y: 50,
			opacity: 0,
		},

		borderAnimate: {
			y: 0,
			opacity: 1,
			transition: {
				delay: 1,
				type: 'tween',
				ease: 'easeOut',
				duration: 0.5,
			},
		},
	};

	//news slider control
	const slideNewsRight = () => {
		setNewsIndex((newsIndex + 1) % data.length); // increases index by 1
		setIsScroll(true);
	};

	const slideNewsLeft = () => {
		const nextIndex = newsIndex - 1;
		setIsScroll(false);
		if (nextIndex < 0) {
			setNewsIndex(data.length - 1); // returns last index of images array if index is less than 0
		} else {
			setNewsIndex(nextIndex);
		}
	};

	return (
		<FaderWrap>
			<TopWrapper className='w-full h-screen relative mb-16' {...props}>
				<img
					src={data[newsIndex].img}
					alt={data[newsIndex].altText}
					className='h-full object-cover w-full'
				/>
				<div className='absolute z-10 w-full h-full top-0 left-0 bg-black opacity-60'></div>
				<section className='absolute z-20 top-16 lg:top-28 left-0 w-full h-full lg:w-11/12 flex justify-center'>
					<div className='w-11/12 h-4/5 relative flex flex-col lg:flex-row 2xl:w-6/12'>
						<div className='overflow-hidden w-full'>
							<AnimatePresence>
								<motion.section
									className='w-full lg:w-10/12 h-full'
									initial='rest'
									animate='hover'
									variants={ContentVaraint}
									key={data[newsIndex].img}
									exit={{ visibility: 'hidden' }}
								>
									<div className='borderWrap'></div>
									<div className='w-full lg:w-6/12 h-72 lg:h-2/3 mb-4'>
										<img
											src={data[newsIndex].img}
											alt={data[newsIndex].altText}
											className='h-full object-cover w-full'
										/>
									</div>
									<h3 className='text-xl font-bold lg:text-xl text-brand-yellow mb-2'>
										{data[newsIndex].headerText}
									</h3>
									<section className='hidden lg:flex h-auto relative pl-6 lg:pl-12 w-full lg:w-10/12'>
										<motion.div
											initial='borderRest'
											animate='borderAnimate'
											variants={ContentVaraint}
											key={data[newsIndex].img}
											exit={{ visibility: 'hidden' }}
											className='border-brand-yellow border-l h-auto mr-2'
										></motion.div>
										<div className=''>
											<p className='text-xs font-medium text-white mb-1'>
												{data[newsIndex].descText}
											</p>
										</div>
									</section>
								</motion.section>
							</AnimatePresence>
						</div>
						<section className='flex flex-row lg:flex-col-reverse nav-arrow-sec bottom-0 lg:w-20 w-full justify-center mt-4 lg:mt-0'>
							<div
								className='border border-white flex justify-center items-center h-20 w-full nav-hover cursor-pointer'
								onClick={() => slideNewsLeft()}
							>
								<LeftNavIcon color='#ffffff' />
							</div>
							<div className='border border-white flex justify-center items-center h-20 w-full'>
								<section className='hidden lg:block'>
									<span className='font-bold text-brand-yellow text-lg mb-3'>
										{newsIndex + 1}
									</span>
									<DividerIcon />
									<span className='text-white ml-2'>
										{data.length}
									</span>
								</section>
								<section className='flex items-center lg:hidden'>
									<span className='font-bold text-brand-yellow text-lg mr-3'>
										{newsIndex + 1}
									</span>
									<DividerBendIcon />
									<span className='text-white ml-2'>
										{data.length}
									</span>
								</section>
							</div>
							<div
								className='border border-white flex justify-center items-center h-20 w-full nav-hover cursor-pointer'
								onClick={() => slideNewsRight()}
							>
								<RightNavIcon color='#ffffff' />
							</div>
						</section>
					</div>
					<h2 className='font-bold capitalize text-3xl inline-block text-gray-600 absolute z-20'>
						top stories
					</h2>
				</section>
			</TopWrapper>
		</FaderWrap>
	);
};

export default TopStories;
