import { AnimatePresence, motion } from 'framer-motion';
import styled from 'styled-components';
import { data, useTwitter } from '../../data/newsSlider';
import OpenLinkIcon from '../../assets/svg/OpenLinkIcon';
import TwitterIcon from '../../assets/svg/TwitterIcon';
import { useEffect, useState } from 'react';
import FaderWrap from '../FaderWrap';
import VideoCard from './VideoCard';
import NextIcon from '../../assets/svg/NextIcon';

const NewsWrapper = styled.section`
	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: -1.5rem;
		left: -1rem;
	}

	.section-1 {
		width: 100%;
		.cont-height {
			height: 28rem;
		}
	}
	.section-2 {
		height: 28rem;
		width: 100%;
		background: #307fe2;
	}

	.mini-caurosel {
		top: 350px;
		left: 45%;
	}

	.social-caurosel {
		top: 350px;
		left: 45%;
	}

	.arrow::after {
		border-bottom: 15px solid transparent;
		border-top: 15px solid transparent;
		border-right: 15px solid #fff;
		content: '';
		height: 0;
		position: absolute;
		right: 50%;
		top: 25px;
		width: 0;
		z-index: 1;
		display: none;
	}

	.arrow::before {
		border-bottom: 15px solid #fff;
		border-left: 15px solid transparent;
		border-right: 15px solid transparent;
		content: '';
		height: 0;
		position: absolute;
		right: 45%;
		top: 385px;
		width: 0;
		z-index: 1;
	}

	.wrapper-2 {
		transition: all 1s ease;
		.img-cover1 {
			background: linear-gradient(
				180deg,
				#111b28 0%,
				rgba(0, 0, 0, 0.43) 100%
			);
			height: 100%;

			& :hover {
				opacity: 0.8;
			}
		}
		.img-cover2 {
			background: linear-gradient(
				76.97deg,
				#111b28 34.77%,
				rgba(0, 0, 0, 0.43) 90.6%
			);
			height: 100%;

			& :hover {
				opacity: 0.8;
			}
		}

		.section-hgt {
			height: 25rem;
		}
	}

	@media (min-width: 1024px) {
		.section-1 {
			width: 65%;
		}

		.section-2 {
			width: 30%;
		}

		.arrow::after {
			display: block;
		}

		.arrow::before {
			display: none;
		}

		.social-caurosel {
			left: 45%;
		}

		.mini-caurosel {
			left: 15%;
		}
	}

	@media (min-width: 1536px) {
		.social-caurosel {
			left: 48%;
		}

		.mini-caurosel {
			left: 27%;
		}

		.arrow::after {
			right: 50%;
		}
	}
`;

const NewsSlider = (props) => {
	const [newsIndex, setNewsIndex] = useState(0);
	const [socialIndex, setSocialIndex] = useState(0);
	const [isScroll, setIsScroll] = useState(true);
	const [isTweetScroll, setIsTweetScroll] = useState(true);

	const [tweets, isError, isLoading] = useTwitter();

	const ContentVaraint = {
		rest: {
			x: isScroll ? 300 : -300,
		},

		hover: {
			x: 0,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 0.8,
			},
		},

		rest2: {
			x: isTweetScroll ? 300 : -300,
		},

		hover2: {
			x: 0,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 0.8,
			},
		},

		borderRest: {
			y: 50,
			opacity: 0,
		},

		borderAnimate: {
			y: 0,
			opacity: 1,
			transition: {
				delay: 1,
				type: 'tween',
				ease: 'easeOut',
				duration: 0.5,
			},
		},
	};

	//news slider control
	const slideNewsRight = () => {
		setNewsIndex((newsIndex + 1) % data.length); // increases index by 1
		setSocialIndex((socialIndex + 1) % [0, 1, 2].length);
	};
	const slideSocials = () => {
		setSocialIndex((socialIndex + 1) % [0, 1, 2].length);
	};

	useEffect(() => {
		const timer = setTimeout(() => {
			slideNewsRight();
		}, 6000);
		const indexTimer = setTimeout(() => {
			slideSocials();
		}, 6000);

		return () => {
			clearTimeout(timer);
			clearTimeout(indexTimer);
		};
	}, [newsIndex, socialIndex]);

	return (
		<FaderWrap>
			<NewsWrapper
				className='px-4 py-2 lg:px-16 relative mb-10'
				{...props}
			>
				<div className='relative flex justify-center'>
					<ol className='flex w-10 justify-between h-2 mini-caurosel absolute z-10'>
						<li
							className={`cursor-pointer inline-block w-1/5 border border-white ${
								newsIndex === 0 ? 'bg-white' : ''
							}`}
							onClick={() => {
								setNewsIndex(0);
								setIsScroll(false);
							}}
						></li>
						<li
							className={`cursor-pointer inline-block w-1/5 border border-white ${
								newsIndex === 1 ? 'bg-white' : ''
							}`}
							onClick={() => {
								setNewsIndex(1);
								if (newsIndex > 1) {
									setIsScroll(false);
								} else {
									setIsScroll(true);
								}
							}}
						></li>
						<li
							className={`cursor-pointer inline-block w-1/5 border border-white ${
								newsIndex === 2 ? 'bg-white' : ''
							}`}
							onClick={() => {
								setNewsIndex(2);
								setIsScroll(true);
							}}
						></li>
					</ol>
					<div className='lg:justify-between lg:flex-row relative 2xl:w-8/12'>
						<div className='flex flex-col lg:justify-between lg:flex-row overflow-hidden relative'>
							<div className='relative overflow-hidden section-1 w-full'>
								<AnimatePresence>
									<motion.section
										className='flex flex-col lg:flex-row relative'
										initial='rest'
										animate='hover'
										variants={ContentVaraint}
										key={data[newsIndex].img}
										exit={{ visibility: 'hidden' }}
									>
										<div className='bg-gray-300 w-full lg:w-3/6 cont-height'>
											<img
												src={data[newsIndex].img}
												alt={data[newsIndex].altText}
												className='h-full object-cover w-full'
											/>
										</div>
										<div className='bg-white w-full lg:w-3/6 arrow px-4 py-8'>
											<p className='uppercase text-xs text-blue-500 mb-14'>
												press release
											</p>
											<motion.header className='relative flex flex-col'>
												<h1 className='text-gray-700 text-xl font-bold mb-6'>
													{data[newsIndex].headerText}
												</h1>
												<section className='flex justify-center items-center'>
													<div className='flex w-3/4 h-auto'>
														<motion.div
															initial='borderRest'
															animate='borderAnimate'
															variants={
																ContentVaraint
															}
															className='border-brand-yellow border-l mr-2'
														></motion.div>
														<div className=''>
															<p className='text-gray-600 text-sm font-medium mb-2'>
																{
																	data[
																		newsIndex
																	].descText
																}
															</p>
															<a
																href={`articles/${newsIndex}`}
																className='flex items-center justify-end'
															>
																<span className='mr-2 text-brand-blue font-bold'>
																	Learn More
																</span>
																<NextIcon color='#2659A2' />
															</a>
														</div>
													</div>
												</section>
											</motion.header>
										</div>
									</motion.section>
								</AnimatePresence>
							</div>
							<section className='section-2 relative mt-8 lg:mt-0'>
								<ol className='flex w-10 justify-between h-2 social-caurosel absolute z-10'>
									{[0, 1, 2].map((i) => (
										<li
											key={i}
											className={`cursor-pointer inline-block w-1/5 border border-white ${
												socialIndex === i
													? 'bg-white'
													: ''
											}`}
											onClick={() => {
												setSocialIndex(i);
												if (i < 1) {
													setIsTweetScroll(false);
												} else {
													setIsTweetScroll(true);
												}
											}}
										></li>
									))}
								</ol>

								<a
									href='https://twitter.com/search?q=mojec'
									target='_blank'
									rel='noopener noreferrer'
									className='section-2 relative mb-8 lg:mb-0'
									className='flex pl-8 pr-4 py-8 flex-col'
								>
									<TwitterIcon className='mb-12' />
									<div className='overflow-hidden'>
										{isLoading && (
											<p className='text-white text-center'>
												Loading tweets....
											</p>
										)}
										{isError && (
											<p className='text-white text-center'>
												Error loading tweets!
											</p>
										)}
										{tweets && tweets.length < 1 ? (
											<p className='text-white text-center'>
												No tweet to display!!
											</p>
										) : (
											<AnimatePresence>
												<motion.section
													className='flex h-auto relative'
													initial='rest2'
													animate='hover2'
													variants={ContentVaraint}
													key={
														tweets &&
														tweets[socialIndex].id
													}
													exit={{
														visibility: 'hidden',
													}}
												>
													<motion.div
														initial='borderRest'
														animate='borderAnimate'
														variants={
															ContentVaraint
														}
														className='border-white border-l h-auto mr-2'
													></motion.div>
													<div className=''>
														<p className='text-sm font-medium text-white mb-1'>
															{tweets &&
																tweets[
																	socialIndex
																].text}
														</p>
														<p className='text-white italic text-xs'>
															{tweets &&
																tweets[
																	socialIndex
																].created_at.substr(
																	0,
																	10
																)}
														</p>
														<div className='flex justify-end items-center absolute right-0 top-64'>
															<OpenLinkIcon />
														</div>
													</div>
												</motion.section>
											</AnimatePresence>
										)}
									</div>
								</a>
							</section>
						</div>
						<h2 className='font-bold capitalize text-3xl inline-block text-gray-600 absolute z-10'>
							in the news
						</h2>
					</div>
				</div>
			</NewsWrapper>
			<VideoCard />
		</FaderWrap>
	);
};

export default NewsSlider;
