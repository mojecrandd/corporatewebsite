import { useState } from 'react';
import styled from 'styled-components';
import mojecLogo from '../../assets/images/mojecc.png';
import mojecLogoW from '../../assets/images/mojeclogoW.png';
import MenuClose from '../../assets/svg/MenuClose';
import MenuOpen from '../../assets/svg/MenuOpen';
import BusinessMini from './navItems/BusinessMini';
import CorpInfoMini from './navItems/CorpInfoMini';
import MojecPowerMini from './navItems/MojecPowerMini';
import ProductMini from './navItems/ProductMini';
import { motion } from 'framer-motion';
import ContactUs from '../../assets/svg/ContactUs';
import BusinessMain from './navItems/BusinessMain';
import { ProductMain } from './navItems/ProductMain';
import MojecPowerMain from './navItems/MojecPowerMain';
import CorpInfoMain from './navItems/CorpInfoMain';
import { Link, useLocation } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import MSHMain from './navItems/MSHMain';
import MSHMini from './navItems/MSHMini';

const NavWrapper = styled(motion.nav)`
	position: relative;
	z-index: 40;
	.parent-nav {
		z-index: initial;
	}

	.bus-nav,
	.prod-nav,
	.moj-nav,
	.corp-nav,
	.msh-nav {
		display: none;
		opacity: 0;
		-webkit-animation: fadein ease-in 1;
		-moz-animation: fadein ease-in 1;
		animation: fadein ease-in 1;
		-webkit-animation-fill-mode: forwards;
		-moz-animation-fill-mode: forwards;
		animation-fill-mode: forwards;
		-webkit-animation-duration: 1s;
		-moz-animation-duration: 1s;
		animation-duration: 1s;
	}

	.bus-hov:hover {
		border-bottom: 1px solid #e5bd5c;
	}

	.bus-hov:hover + .bus-nav,
	.bus-nav:hover,
	.prod-hov:hover + .prod-nav,
	.prod-nav:hover,
	.moj-hov:hover + .moj-nav,
	.moj-nav:hover,
	.corp-hov:hover + .corp-nav,
	.corp-nav:hover,
	.msh-hov:hover + .msh-nav,
	.msh-nav:hover {
		opacity: 1;
		display: block;
	}

	@keyframes fadein {
		from {
			opacity: 0;
		}
		to {
			opacity: 1;
		}
	}
	@-moz-keyframes fadein {
		from {
			opacity: 0;
		}
		to {
			opacity: 1;
		}
	}
	@-webkit-keyframes fadein {
		from {
			opacity: 0;
		}
		to {
			opacity: 1;
		}
	}
	@-o-keyframes fadein {
		from {
			opacity: 0;
		}
		to {
			opacity: 1;
		}
	}
`;

const MiniNavWrap = styled(motion.div)`
	background: rgba(0, 0, 0, 0.5);
	backdrop-filter: blur(0.5px);

	.menu-wrap {
		border-bottom: 0.1px solid rgba(255, 255, 255, 0.2);
	}
`;

const MainNavWrap = styled.div`
	display: none;

	@media (min-width: 1024px) {
		display: flex;
	}
`;

//framer variant
const HoverVariant = {
	// rest: {
	// 	y: '-100vh',
	// 	transition: {
	// 		duration: 1.5,
	// 		type: 'tween',
	// 		ease: 'easeIn',
	// 	},
	// },
	// hover: {
	// 	y: 0,
	// 	transition: {
	// 		duration: 1,
	// 		type: 'tween',
	// 		ease: 'easeOut',
	// 	},
	// },
};

const ContentVaraint = {
	// rest: {
	// 	opacity: 0,
	// },
	// hover: {
	// 	opacity: 1,
	// 	transition: {
	// 		delay: 1,
	// 		type: 'tween',
	// 		ease: 'easeIn',
	// 	},
	// },
};

const MotionLink = motion.custom(Link);

const Nav = (props) => {
	const location = useLocation();
	const [isOpen, setIsOpen] = useState(false);
	const [isGrey, setIsGrey] = useState(true);
	const [expand, setExpand] = useState({
		business: false,
		product: false,
		mojec_power: false,
		corp_info: false,
		msh: false,
	});
	const [active, setActive] = useState(false);

	const handleCloseModal = (e) => {
		if (e.target.classList.contains('mini-nav-wrap')) {
			setIsOpen(false);
			setActive(!active);
		}
	};

	return (
		<NavWrapper {...props}>
			<motion.div
				className='relative 2xl:flex 2xl:justify-center'
				initial={{ y: -100 }}
				animate={{ y: 0 }}
				transition={{ delay: 0.3 }}
			>
				<div className='flex items-center justify-between px-4 py-2 lg:px-16 2xl:w-8/12'>
					<MotionLink to='/home' className='block z-40'>
						{location.pathname === '/privacy' ||
						location.pathname === '/termsofuse' ||
						location.pathname === '/disclaimer' ||
						location.pathname === '/contact' ? (
							<img
								src={mojecLogo}
								className='h-10 lg:h-14'
								alt='Mojec-Logo'
							/>
						) : (
							<img
								src={mojecLogoW}
								className='h-10 lg:h-14'
								alt='Mojec-Logo'
							/>
						)}
					</MotionLink>
					<button
						className='block text-white hover:text-white focus:text-white lg:hidden'
						type='button'
						onClick={() => {
							setIsOpen(true);
							setActive(!active);
						}}
					>
						<MenuOpen
							color={
								(location.pathname === '/privacy' ||
									location.pathname === '/termsofuse' ||
									location.pathname === '/disclaimer' ||
									location.pathname === '/contact') &&
								isGrey
									? '#000000'
									: '#ffffff'
							}
						/>
					</button>

					{/* menu item lg-screen */}
					<MainNavWrap>
						<ul className='flex justify-between'>
							<motion.li
								className='mr-8'
								initial='rest'
								whileHover='hover'
								animate='rest'
							>
								<motion.a
									className={`z-40 relative cursor-pointer capitalize ${
										(location.pathname === '/privacy' ||
											location.pathname ===
												'/termsofuse' ||
											location.pathname ===
												'/disclaimer' ||
											location.pathname === '/contact') &&
										isGrey
											? 'text-black'
											: 'text-white'
									} font-medium inline-block pb-2 bus-hov`}
									initial={{ border: -100 }}
									whileHover='hover'
									variants={HoverVariant}
									onMouseEnter={() => setIsGrey(false)}
									onMouseLeave={() => setIsGrey(true)}
								>
									business
								</motion.a>
								<div className='bus-nav'>
									<BusinessMain
										// ContentVaraint={ContentVaraint}
										// HoverVariant={HoverVariant}
										onMouseEnter={() => setIsGrey(false)}
										onMouseLeave={() => setIsGrey(true)}
									/>
								</div>
							</motion.li>
							<motion.li
								className='mr-8'
								initial='rest'
								whileHover='hover'
								animate='rest'
							>
								<motion.a
									className={`z-40 relative cursor-pointer capitalize ${
										(location.pathname === '/privacy' ||
											location.pathname ===
												'/termsofuse' ||
											location.pathname ===
												'/disclaimer' ||
											location.pathname === '/contact') &&
										isGrey
											? 'text-black'
											: 'text-white'
									} font-medium bus-hov inline-block pb-2 prod-hov`}
									initial={{ border: -100 }}
									whileHover={{ border: 0, opacity: 0.8 }}
									onMouseEnter={() => setIsGrey(false)}
									onMouseLeave={() => setIsGrey(true)}
								>
									Product
								</motion.a>
								<div className='prod-nav'>
									<ProductMain
										ContentVaraint={ContentVaraint}
										HoverVariant={HoverVariant}
										onMouseEnter={() => setIsGrey(false)}
										onMouseLeave={() => setIsGrey(true)}
									/>
								</div>
							</motion.li>
							{/* <motion.li
								className='mr-8'
								initial='rest'
								whileHover='hover'
								animate='rest'
							>
								<motion.a
									className={`z-40 relative cursor-pointer capitalize ${
										(location.pathname === '/privacy' ||
											location.pathname ===
												'/termsofuse' ||
											location.pathname ===
												'/disclaimer' ||
											location.pathname === '/contact') &&
										isGrey
											? 'text-black'
											: 'text-white'
									} font-medium bus-hov inline-block pb-2 moj-hov`}
									initial={{ border: -100 }}
									whileHover={{ border: 0, opacity: 0.8 }}
									onMouseEnter={() => setIsGrey(false)}
									onMouseLeave={() => setIsGrey(true)}
								>
									mojec power
								</motion.a>
								<div className='moj-nav'>
									<MojecPowerMain
										ContentVaraint={ContentVaraint}
										HoverVariant={HoverVariant}
										onMouseEnter={() => setIsGrey(false)}
										onMouseLeave={() => setIsGrey(true)}
									/>
								</div>
							</motion.li> */}
							<motion.li
								className='mr-8'
								initial='rest'
								whileHover='hover'
								animate='rest'
							>
								<motion.a
									className={`z-40 relative cursor-pointer capitalize ${
										(location.pathname === '/privacy' ||
											location.pathname ===
												'/termsofuse' ||
											location.pathname ===
												'/disclaimer' ||
											location.pathname === '/contact') &&
										isGrey
											? 'text-black'
											: 'text-white'
									} font-medium bus-hov inline-block pb-2 msh-hov`}
									initial={{ border: -100 }}
									whileHover={{ border: 0, opacity: 0.8 }}
									onMouseEnter={() => setIsGrey(false)}
									onMouseLeave={() => setIsGrey(true)}
								>
									MSH
								</motion.a>
								<div className='msh-nav'>
									<MSHMain
										ContentVaraint={ContentVaraint}
										HoverVariant={HoverVariant}
										onMouseEnter={() => setIsGrey(false)}
										onMouseLeave={() => setIsGrey(true)}
									/>
								</div>
							</motion.li>
							<motion.li
								className='mr-8'
								initial='rest'
								whileHover='hover'
								animate='rest'
							>
								<motion.a
									className={`z-40 relative cursor-pointer capitalize ${
										(location.pathname === '/privacy' ||
											location.pathname ===
												'/termsofuse' ||
											location.pathname ===
												'/disclaimer' ||
											location.pathname === '/contact') &&
										isGrey
											? 'text-black'
											: 'text-white'
									} font-medium bus-hov inline-block pb-2 corp-hov`}
									initial={{ border: -100 }}
									whileHover={{ border: 0, opacity: 0.8 }}
									onMouseEnter={() => setIsGrey(false)}
									onMouseLeave={() => setIsGrey(true)}
								>
									corporate information
								</motion.a>
								<div className='corp-nav'>
									<CorpInfoMain
										ContentVaraint={ContentVaraint}
										HoverVariant={HoverVariant}
										onMouseEnter={() => setIsGrey(false)}
										onMouseLeave={() => setIsGrey(true)}
									/>
								</div>
							</motion.li>
							<motion.li
								className='mr-8'
								initial='rest'
								whileHover='hover'
								animate='rest'
							>
								<MotionLink
									to='disclaimer'
									className={`z-40 relative cursor-pointer capitalize ${
										(location.pathname === '/privacy' ||
											location.pathname ===
												'/termsofuse' ||
											location.pathname ===
												'/disclaimer' ||
											location.pathname === '/contact') &&
										isGrey
											? 'text-black'
											: 'text-white'
									} font-medium bus-hov inline-block pb-2 corp-hov`}
									initial={{ border: -100 }}
									whileHover={{ border: 0, opacity: 0.8 }}
								>
									legal disclaimer
								</MotionLink>
							</motion.li>
							<li className='cursor-pointer mr-8'>
								<MotionLink
									to='/contact'
									className='z-40 relative capitalize text-white font-medium bus-hov flex justify-center items-center pb-2'
									initial={{ border: -100 }}
									whileHover={{ border: 0, opacity: 0.8 }}
								>
									<ContactUs
										className='mt-1'
										color={
											(location.pathname === '/privacy' ||
												location.pathname ===
													'/termsofuse' ||
												location.pathname ===
													'/disclaimer' ||
												location.pathname ===
													'/contact') &&
											isGrey
												? '#000000'
												: '#ffffff'
										}
										data-tip
										data-for='contact'
									/>
									<ReactTooltip
										id='contact'
										place='bottom'
										// effect='solid'
										type='light'
									>
										Contact Us
									</ReactTooltip>
								</MotionLink>
							</li>
						</ul>
					</MainNavWrap>
				</div>
				<MiniNavWrap
					className={`w-screen min-h-screen ${
						isOpen ? 'fixed' : 'hidden'
					} top-0 flex justify-end overflow-hidden lg:hidden mini-nav-wrap z-20`}
					initial={{ x: active ? 0 : 100, opacity: active ? 1 : 0 }}
					animate={{ x: active ? 0 : 100, opacity: active ? 1 : 0 }}
					transition={{
						type: 'spring',
						stiffness: 120,
						duration: 0.5,
					}}
					onClick={handleCloseModal}
				>
					<div className='bg-black h-screen w-10/12 overflow-y-scroll'>
						<div className='flex justify-between p-4 items-center menu-wrap'>
							<p className='text-white'>Menu</p>
							<MenuClose
								className='cursor-pointer'
								onClick={() => {
									setIsOpen(false);
									setActive(!active);
								}}
							/>
						</div>
						<ul className='p-4'>
							{/* section */}
							<BusinessMini
								expand={expand}
								setExpand={setExpand}
							/>
							<ProductMini
								expand={expand}
								setExpand={setExpand}
							/>
							{/* <MojecPowerMini
								expand={expand}
								setExpand={setExpand}
							/> */}
							<MSHMini expand={expand} setExpand={setExpand} />
							<CorpInfoMini
								expand={expand}
								setExpand={setExpand}
							/>
							<li className='mb-3'>
								<Link
									to='/contact'
									className='text-white font-medium capitalize hover:text-brand-yellow'
								>
									contact us
								</Link>
							</li>
						</ul>
					</div>
				</MiniNavWrap>
			</motion.div>
		</NavWrapper>
	);
};

export default Nav;
