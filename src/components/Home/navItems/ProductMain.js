import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const ContentWrap = styled(motion.div)`
	.item-wrap {
		border-bottom: 0.1px solid rgba(255, 255, 255, 0.2);
	}
`;

export const ProductMain = (props) => {
	return (
		<motion.div
			className='z-30 w-full top-0 left-0 bg-black absolute justify-end flex'
			variants={props.HoverVariant}
			{...props}
		>
			<ContentWrap
				className='w-6/12 mt-16 pl-12 pr-12'
				variants={props.ContentVaraint}
			>
				<ul className='pl-2'>
					<li className='mb-4 item-wrap pb-2'>
						<span className='text-white text-2xl font-bold'>
							Our Products
						</span>
					</li>
					<li>
						<ul className='grid grid-cols-2'>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='prepaidmeter'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									Pre-Paid meters
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='postpaidmeter'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									post-Paid Meters
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='meteraccessories'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									meter accessories
								</Link>
							</motion.li>
						</ul>
					</li>
				</ul>
			</ContentWrap>
		</motion.div>
	);
};
