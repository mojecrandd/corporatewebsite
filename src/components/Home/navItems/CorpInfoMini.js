import { Link } from "react-router-dom";
import ArrowDown from "../../../assets/svg/ArrowDown";
import ArrowUp from "../../../assets/svg/ArrowUp";

const CorpInfoMini = ({ expand, setExpand }) => {
	return (
		<li>
			<div
				className='flex justify-between items-center cursor-pointer mb-4'
				onClick={() =>
					setExpand({
						...expand,
						business: false,
						product: false,
						mojec_power: false,
						corp_info: !expand.corp_info,
						msh: false,
					})
				}
			>
				<p
					className={`${
						expand.corp_info ? 'text-brand-yellow' : 'text-white'
					} capitalize font-medium  hover:text-brand-yellow`}
				>
					corporate information
				</p>
				{expand.corp_info ? (
					<ArrowDown
						color={expand.corp_info ? '#E5BD5C' : '#ffffff'}
					/>
				) : (
					<ArrowUp color={expand.corp_info ? '#E5BD5C' : '#ffffff'} />
				)}
			</div>
			{/* corp_info headings */}
			<div className={` ${expand.corp_info ? 'block' : 'hidden'}`}>
				<ul className='pl-2'>
					<li className='mb-4'>
						<span className='text-white text-2xl font-bold'>
							MOJEC International
						</span>
					</li>
					<li className='mb-3'>
						<Link
							to='/history'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							history
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/ourpeople'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							our people
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/r&d'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							research & development
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/energy'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							energy
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/corporatevalue'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							corporate values
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/career'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							career
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/corpsocial'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Corp. Social Responsibility
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='manufacturingfacility'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Manufacturing Facilities
						</Link>
					</li>
				</ul>
			</div>
		</li>
	);
};

export default CorpInfoMini
