import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const ContentWrap = styled(motion.div)`
	.item-wrap {
		border-bottom: 0.1px solid rgba(255, 255, 255, 0.2);
	}
`;

const MojecPowerMain = (props) => {
	return (
		<motion.div
			className='z-30 w-full top-0 left-0 bg-black absolute justify-end flex'
			variants={props.HoverVariant}
			{...props}
		>
			<ContentWrap
				className='w-6/12 mt-16 pl-12 pr-12'
				variants={props.ContentVaraint}
			>
				<ul className='pl-2'>
					<li className='mb-4 item-wrap pb-2'>
						<span className='text-white text-2xl font-bold'>
							For Businesses
						</span>
					</li>
					<li>
						<ul className='grid grid-cols-2'>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/generation'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									generation
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/transmission'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									transmission
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/distribution'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									distribution
								</Link>
							</motion.li>
						</ul>
					</li>
				</ul>
			</ContentWrap>
		</motion.div>
	);
};

export default MojecPowerMain;
