import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const ContentWrap = styled(motion.div)`
	.item-wrap {
		border-bottom: 0.1px solid rgba(255, 255, 255, 0.2);
	}
`;

const CorpInfoMain = (props) => {
	return (
		<motion.div
			className='z-30 w-full top-0 left-0 bg-black absolute justify-end flex'
			variants={props.HoverVariant}
			{...props}
		>
			<ContentWrap
				className='w-6/12 mt-16 pl-12 pr-12'
				variants={props.ContentVaraint}
			>
				<ul className='pl-2'>
					<li className='mb-4 item-wrap pb-2'>
						<span className='text-white text-2xl font-bold'>
							MOJEC International
						</span>
					</li>
					<li>
						<ul className='grid grid-cols-2'>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/history'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									history
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/ourpeople'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									our people
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/r&d'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									research & development
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/energy'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									energy
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/corporatevalue'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									corporate values
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/career'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									career
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/corpsocial'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									Corporate social responsibility
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='manufacturingfacility'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									manufacturing facilities
								</Link>
							</motion.li>
						</ul>
					</li>
				</ul>
			</ContentWrap>
		</motion.div>
	);
};

export default CorpInfoMain
