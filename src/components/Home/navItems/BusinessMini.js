import ArrowDown from '../../../assets/svg/ArrowDown';
import ArrowUp from '../../../assets/svg/ArrowUp';
import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';

const BusinessMini = ({ expand, setExpand }) => {
	return (
		<li>
			<div
				className='flex justify-between items-center cursor-pointer mb-4'
				onClick={() =>
					setExpand({
						...expand,
						business: !expand.business,
						product: false,
						mojec_power: false,
						corp_info: false,
						msh: false,
					})
				}
			>
				<p
					className={`${
						expand.business ? 'text-brand-yellow' : 'text-white'
					} capitalize font-medium  hover:text-brand-yellow`}
				>
					business
				</p>
				{expand.business ? (
					<ArrowDown
						color={expand.business ? '#E5BD5C' : '#ffffff'}
					/>
				) : (
					<ArrowUp color={expand.business ? '#E5BD5C' : '#ffffff'} />
				)}
			</div>
			{/* Business headings */}
			<motion.div
				className={` ${expand.business ? 'block' : 'hidden'}`}
				initial={{ y: 100 }}
				animate={{ y: 0 }}
				transition={{ delay: 0.5, duration: 0.5 }}
			>
				<ul className='pl-2'>
					<li className='mb-4'>
						<span className='text-white text-2xl font-bold'>
							Business overview
						</span>
					</li>
					<li className='mb-3'>
						<Link
							to='/learnmore'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Learn more
						</Link>
					</li>
					<li className='mb-4'>
						<span className='text-white text-2xl font-bold'>
							Business Verticals
						</span>
					</li>
					<li className='mb-3'>
						<Link
							to='/generation'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							generation
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/transmission'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							transmission
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/distribution'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							distribution
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/metering'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							metering
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/energymanagement'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							energy management
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/agriculture'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							agriculture
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/realestate'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							real estate
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/r&d'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							research & development
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/retail'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							retail
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/energy'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							energy
						</Link>
					</li>
				</ul>
				<ul className='pl-2'>
					<li className='mb-4'>
						<span className='text-white text-2xl font-bold'>
							Quick Access
						</span>
					</li>
					<li className='mb-3'>
						<a
							href='http://reconcile.mojec.com/'
							target='_blank'
							rel='noopener noreferrer'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Verify & Update Payment Info
						</a>
					</li>
					<li className='mb-3'>
						<a
							href='http://retrieval.mojec.com/'
							target='_blank'
							rel='noopener noreferrer'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Retrieve Payment Ref
						</a>
					</li>
					<li className='mb-3'>
						<a
							href='http://powergenie.com.ng/'
							target='_blank'
							rel='noopener noreferrer'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Power Genie
						</a>
					</li>
				</ul>
			</motion.div>
		</li>
	);
};

export default BusinessMini;
