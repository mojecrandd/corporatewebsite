import { Link } from "react-router-dom";
import ArrowDown from "../../../assets/svg/ArrowDown";
import ArrowUp from "../../../assets/svg/ArrowUp";

const MojecPowerMini = ({ expand, setExpand }) => {
	return (
		<li>
			<div
				className='flex justify-between items-center cursor-pointer mb-4'
				onClick={() =>
					setExpand({
						...expand,
						business: false,
						product: false,
						mojec_power: !expand.mojec_power,
						corp_info: false,
						msh: false,
					})
				}
			>
				<p
					className={`${
						expand.mojec_power ? 'text-brand-yellow' : 'text-white'
					} capitalize font-medium  hover:text-brand-yellow`}
				>
					mojec power
				</p>
				{expand.mojec_power ? (
					<ArrowDown
						color={expand.mojec_power ? '#E5BD5C' : '#ffffff'}
					/>
				) : (
					<ArrowUp
						color={expand.mojec_power ? '#E5BD5C' : '#ffffff'}
					/>
				)}
			</div>
			{/* mojec_power headings */}
			<div className={` ${expand.mojec_power ? 'block' : 'hidden'}`}>
				<ul className='pl-2'>
					<li className='mb-4'>
						<span className='text-white text-2xl font-bold'>
							For Businesses
						</span>
					</li>
					<li className='mb-3'>
						<Link
							to='/generation'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							generation
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/transmission'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							transmission
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='/distribution'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							distribution
						</Link>
					</li>
				</ul>
			</div>
		</li>
	);
};

export default MojecPowerMini
