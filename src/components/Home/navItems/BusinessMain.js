import { motion } from 'framer-motion';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const ContentWrap = styled(motion.div)`
	.item-wrap {
		border-bottom: 0.1px solid rgba(255, 255, 255, 0.2);
	}
`;

const BusinessMain = (props) => {
	return (
		<motion.div
			className='z-30 w-full top-0 left-0 bg-black absolute justify-end flex'
			variants={props.HoverVariant}
			{...props}
		>
			<ContentWrap
				className='w-6/12 mt-16 pl-12 pr-12'
				variants={props.ContentVaraint}
			>
				<ul className='pl-2'>
					<li className='mb-4 item-wrap pb-2'>
						<span className='text-white text-2xl font-bold'>
							Business overview
						</span>
					</li>
					<motion.li
						className='mb-3'
						whileHover={{
							scale: 1.1,
						}}
					>
						<Link
							to='/learnmore'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							learn more
						</Link>
					</motion.li>
					<li className='mb-4 item-wrap pb-2'>
						<span className='text-white text-2xl font-bold'>
							Business Verticals
						</span>
					</li>
					<li>
						<ul className='grid grid-cols-2'>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/generation'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									generation
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/transmission'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									transmission
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/distribution'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									distribution
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/metering'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									mojec meter company
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/energymanagement'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									energy management
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/agriculture'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									agriculture
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/realestate'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									real estate
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/r&d'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									research & development
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/retail'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									retail
								</Link>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<Link
									to='/energy'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									oil and gas
								</Link>
							</motion.li>
						</ul>
					</li>
				</ul>
				<ul className='pl-2'>
					<li className='mb-4 item-wrap pb-2'>
						<span className='text-white text-2xl font-bold'>
							Quick Access
						</span>
					</li>
					<li>
						<ul className='grid grid-cols-2'>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<a
									href='http://reconcile.mojec.com/'
									target='_blank'
									rel='noopener noreferrer'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									Verify & Update Payment Info
								</a>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<a
									href='http://retrieval.mojec.com/'
									target='_blank'
									rel='noopener noreferrer'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									Retrieve Payment Ref
								</a>
							</motion.li>
							<motion.li
								className='mb-3'
								whileHover={{
									scale: 1.1,
								}}
							>
								<a
									href='http://powergenie.com.ng/'
									target='_blank'
									rel='noopener noreferrer'
									className='text-white font-light capitalize hover:text-brand-yellow'
								>
									Power Genie
								</a>
							</motion.li>
						</ul>
					</li>
				</ul>
			</ContentWrap>
		</motion.div>
	);
};

export default BusinessMain;
