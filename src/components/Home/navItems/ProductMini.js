import { Link } from 'react-router-dom';
import ArrowDown from '../../../assets/svg/ArrowDown';
import ArrowUp from '../../../assets/svg/ArrowUp';

const ProductMini = ({ expand, setExpand }) => {
	return (
		<li>
			<div
				className='flex justify-between items-center cursor-pointer mb-4'
				onClick={() =>
					setExpand({
						...expand,
						product: !expand.product,
						business: false,
						mojec_power: false,
						corp_info: false,
						msh: false,
					})
				}
			>
				<p
					className={`${
						expand.product ? 'text-brand-yellow' : 'text-white'
					} capitalize font-medium  hover:text-brand-yellow`}
				>
					product
				</p>
				{expand.product ? (
					<ArrowDown color={expand.product ? '#E5BD5C' : '#ffffff'} />
				) : (
					<ArrowUp color={expand.product ? '#E5BD5C' : '#ffffff'} />
				)}
			</div>
			{/* product headings */}
			<div className={` ${expand.product ? 'block' : 'hidden'}`}>
				<ul className='pl-2'>
					<li className='mb-4'>
						<span className='text-white text-2xl font-bold'>
							Our Products
						</span>
					</li>
					<li className='mb-3'>
						<Link
							to='prepaidmeter'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							pre-paid meters
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='postpaidmeter'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							post-paid meters
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='meteraccessories'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							meter accessories
						</Link>
					</li>
				</ul>
			</div>
		</li>
	);
};

export default ProductMini;
