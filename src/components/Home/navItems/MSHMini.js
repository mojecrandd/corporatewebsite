import { Link } from 'react-router-dom';
import ArrowDown from '../../../assets/svg/ArrowDown';
import ArrowUp from '../../../assets/svg/ArrowUp';

const MSHMini = ({ expand, setExpand }) => {
	return (
		<li>
			<div
				className='flex justify-between items-center cursor-pointer mb-4'
				onClick={() =>
					setExpand({
						...expand,
						business: false,
						product: false,
						mojec_power: false,
						corp_info: false,
						msh: !expand.msh,
					})
				}
			>
				<p
					className={`${
						expand.msh ? 'text-brand-yellow' : 'text-white'
					} capitalize font-medium  hover:text-brand-yellow`}
				>
					MSH
				</p>
				{expand.msh ? (
					<ArrowDown
						color={expand.msh ? '#E5BD5C' : '#ffffff'}
					/>
				) : (
					<ArrowUp
						color={expand.msh ? '#E5BD5C' : '#ffffff'}
					/>
				)}
			</div>
			{/* msh headings */}
			<div className={` ${expand.msh ? 'block' : 'hidden'}`}>
				<ul className='pl-2'>
					<li className='mb-4'>
						<span className='text-white text-2xl font-bold'>
							Meter Service Hub
						</span>
					</li>
					<li className='mb-3'>
						<Link
							to='vendorfinancing'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Vendor Financing
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='deliveryinstallation'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							Delivery Installation
						</Link>
					</li>
					<li className='mb-3'>
						<Link
							to='afterss'
							className='text-white font-light capitalize hover:text-brand-yellow'
						>
							After Sales & Services
						</Link>
					</li>
				</ul>
			</div>
		</li>
	);
};

export default MSHMini;
