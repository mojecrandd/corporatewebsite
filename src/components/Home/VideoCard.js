import { useState } from 'react';
import styled from 'styled-components';
import mojecOverlay from '../../assets/images/mojecOverlay.jpg';
import ModalVideo from 'react-modal-video';
import PlayIcon from '../../assets/svg/PlayIcon';
import SkewWraper from '../SkewWraper';
import { videos } from '../../data/videos';

const VideoWrap = styled.section`
	/* height: 25rem; */
	transition: all 1s ease;

	.cont-height {
		height: 24rem;
	}

	.img-cover {
		background: linear-gradient(
			76.97deg,
			#111b28 34.77%,
			rgba(0, 0, 0, 0.43) 90.6%
		);
		height: 100%;

		& :hover {
			opacity: 0.8;
		}
	}
`;

const ModalWrap = styled.div`
	@keyframes modal-video {
	from {
		opacity: 0;
	}

	to {
		opacity: 1;
	}
}

@keyframes modal-video-inner {
	from {
		transform: translate(0, 100px);
	}

	to {
		transform: translate(0, 0);
	}
}

.modal-video {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: rgba(0, 0, 0, .5);
	z-index: 1000000;
	cursor: pointer;
	opacity: 1;
	animation-timing-function: ease-out;
	animation-duration: .3s;
	animation-name: modal-video;
	-webkit-transition: opacity .3s ease-out;
	-moz-transition: opacity .3s ease-out;
	-ms-transition: opacity .3s ease-out;
	-o-transition: opacity .3s ease-out;
	transition: opacity .3s ease-out;
}

.modal-video-effect-exit {
	opacity: 0;

	& .modal-video-movie-wrap {
		-webkit-transform: translate(0, 100px);
		-moz-transform: translate(0, 100px);
		-ms-transform: translate(0, 100px);
		-o-transform: translate(0, 100px);
		transform: translate(0, 100px);
	}
}

.modal-video-body {
	max-width: 960px;
	width: 100%;
	height: 100%;
	margin: 0 auto;
	padding: 0 10px;
    display: flex;
    justify-content: center;
	box-sizing: border-box;
}

.modal-video-inner {
	display: flex;
    justify-content: center;
    align-items: center;
	width: 100%;
	height: 100%;

    @media (orientation: landscape) {
        padding: 10px 60px;
        box-sizing: border-box;
    }
}

.modal-video-movie-wrap {
	width: 100%;
	height: 0;
	position: relative;
	padding-bottom: 56.25%;
	background-color: #333;
	animation-timing-function: ease-out;
	animation-duration: .3s;
	animation-name: modal-video-inner;
	-webkit-transform: translate(0, 0);
	-moz-transform: translate(0, 0);
	-ms-transform: translate(0, 0);
	-o-transform: translate(0, 0);
	transform: translate(0, 0);
	-webkit-transition: -webkit-transform .3s ease-out;
	-moz-transition: -moz-transform .3s ease-out;
	-ms-transition: -ms-transform .3s ease-out;
	-o-transition: -o-transform .3s ease-out;
	transition: transform .3s ease-out;

	& iframe {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
}

.modal-video-close-btn {
	position: absolute;
	z-index: 2;
	top: -45px;
	right: 0px;
	display: inline-block;
	width: 35px;
	height: 35px;
	overflow: hidden;
	border: none;
	background: transparent;

    @media (orientation: landscape) {
        top: 0;
        right: -45px;
    }

	&:before {
		transform: rotate(45deg);
	}

	&:after {
		transform: rotate(-45deg);
	}

	&:before,
	&:after {
		content: '';
		position: absolute;
		height: 2px;
		width: 100%;
		top: 50%;
		left: 0;
		margin-top: -1px;
		background: #fff;
		border-radius: 5px;
		margin-top: -6px;
	}
}

`

const VideoCard = () => {
	const [isOpen, setOpen] = useState(false);
	const [index, setIndex] = useState(0);
	return (
		<>
			<SkewWraper>
				<VideoWrap className='w-full 2xl:flex 2xl:justify-center px-4 py-2 lg:px-16 relative mb-12'>
					<div className='2xl:w-8/12'>
						<div className='relative h-full w-full'>
							<div className='cont-height w-full'>
								<img
									src={videos[index].img}
									alt='mojec logo as overlay'
									className='h-full object-cover w-full'
								/>
							</div>
							<div
								className='img-cover absolute top-0 left-0 w-full h-full flex flex-col justify-center items-center z-10 cursor-pointer'
								onClick={() => setOpen(true)}
							>
								<PlayIcon className='mb-2' />
								<h1 className='text-2xl text-white font-medium'>
									{videos[index].title}
								</h1>
							</div>
						</div>
						<div className='px-4 h-20 w-full overflow-x-auto scrollbar-hidden bg-black flex justify-center'>
							<ul className='inline-flex h-full items-center space-x-8 justify-center'>
								{videos.map((video, i) => (
									<li
										className={`h-5/6 cursor-pointer w-32 border-2 ${index === i
											? 'border-white'
											: 'border-gray-700'
											} rounded inline-flex`}
										onClick={() => setIndex(i)}
									>
										<img
											src={video.img}
											alt={video.altText}
											className='bg-placement w-full h-full'
										/>
									</li>
								))}
							</ul>
						</div>
					</div>
				</VideoWrap>
			</SkewWraper>
			<ModalWrap>
				<ModalVideo
					channel='youtube'
					autoplay
					isOpen={isOpen}
					videoId={videos[index].videoId}
					onClose={() => setOpen(false)}
				/>
			</ModalWrap>
		</>
	);
};

export default VideoCard;
