import { motion } from 'framer-motion';
import { useState } from 'react';
import styled from 'styled-components';
import { job, job2 } from '../../data/careers';
import careergrowth from '../../assets/images/careergrowth.jpg';
import CareerIcon from '../../assets/svg/CareerIcon';
import RightNavIcon from '../../assets/svg/RightNavIcon';
import Counter from '../Counter';
import FaderWrap from '../FaderWrap';
import { Link } from 'react-router-dom';
import SkewWraper from '../SkewWraper';

const CareerWrapper = styled.section`
	.section-1 {
		width: 100%;
	}

	.section-2 {
		width: 100%;
	}

	.arrow::after {
		border-bottom: 15px solid #fff;
		border-left: 15px solid transparent;
		border-right: 15px solid transparent;
		content: '';
		height: 0;
		position: absolute;
		top: -15px;
		width: 0;
		z-index: 1;
	}

	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: -1.5rem;
		left: -1rem;
	}

	.overly-cont {
		background: rgba(0, 0, 0, 0.29);

		.text-cont {
			background: rgb(15, 57, 85, 0.7);
		}
	}

	.counter-hegt {
		/* height: 18.6rem; */
		min-height: 18.6rem;
	}

	.career-hover {
		transition: all 1s ease-in-out;

		&:hover {
			background: #f8fafa;
		}
	}

	.career-width {
		width: 100%;
	}

	.social-caurosel {
		bottom: 45px;
		left: 210px;
	}

	@media (min-width: 1024px) {
		.section-1 {
			width: 30%;
		}

		.section-2 {
			width: 65%;
		}

		.career-width {
			width: 48%;
		}

		.social-caurosel {
			left: 160px;
		}
	}

	@media (min-width: 1536px) {
		.social-caurosel {
			left: 50%;
		}
	}
`;

const Careers = (props) => {
	const [socialIndex, setSocialIndex] = useState(0);
	const [isScroll, setIsScroll] = useState(true);

	const ContentVaraint = {
		rest: {
			x: isScroll ? 300 : -300,
		},

		hover: {
			x: 0,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 0.8,
			},
		},

		borderRest: {
			y: 50,
			opacity: 0,
		},

		borderAnimate: {
			y: 0,
			opacity: 1,
			transition: {
				delay: 0.5,
				type: 'tween',
				ease: 'easeOut',
				duration: 0.5,
			},
		},
	};

	return (
		<FaderWrap>
			<SkewWraper data_attr='fade-right'>
				<CareerWrapper
					className='h-auto mt-16 relative mb-16 flex justify-center px-4 py-2 lg:px-8'
					{...props}
				>
					<div className='lg:justify-between lg:flex-row relative 2xl:w-8/12'>
						<div className='relative flex flex-col lg:flex-row lg:justify-between'>
							<section className='section-1 '>
								<div className='w-full h-auto'>
									<img
										src={careergrowth}
										alt='career growth picture'
										className='object-cover w-full h-96'
									/>
									<div className='bg-white w-full arrow px-4 py-8 relative'>
										<p className='uppercase text-xs text-blue-500 mb-14'>
											careers
										</p>
										<motion.header className='relative flex flex-col'>
											<h1 className='text-gray-700 text-xl font-bold mb-6'>
												Beware of Job Scams
											</h1>
											<section className='flex justify-center items-center'>
												<div className='flex justify-center items-center w-3/4 h-auto relative'>
													<motion.div
														initial='borderRest'
														animate='borderAnimate'
														className='border-brand-yellow border-l h-24 mr-2'
													></motion.div>
													<div className=''>
														<p className='text-gray-600 text-base font-medium mb-2'>
															We do not
															charge/accept any
															amount or security
															deposit from job
															seekers.
														</p>
														<Link
															to='career'
															className='flex justify-end items-center absolute right-0 '
														>
															<RightNavIcon color='#E5BD5C' />
														</Link>
													</div>
												</div>
											</section>
										</motion.header>
									</div>
								</div>
							</section>
							<section className='section-2 flex flex-col mt-8 lg:mt-0'>
								<div className='w-full relative mb-5'>
									<img
										src={careergrowth}
										alt='career growth picture'
										className='object-cover w-full'
										style={{ height: '22rem' }}
									/>
									<div
										className='absolute w-full top-0 overly-cont z-10 py-10 px-8'
										style={{ height: '22rem' }}
									>
										<h5 className='text-sm uppercase text-white mb-8'>
											current opening
										</h5>
										<section className='p-6 h-40 w-80 text-cont mb-6'>
											<h4 className='text-white text-xl font-bold mb-3'>
												Apply for job at mojec
												international
											</h4>
											<div className='flex ml-6 '>
												<motion.div
													initial='borderRest'
													animate='borderAnimate'
													className='border-white border-l h-auto mr-2'
												></motion.div>
												<div className=''>
													<p className='text-sm font-light italic text-white mb-1'>
														Explore current position
														in Mojec internationals
													</p>
												</div>
											</div>
										</section>
										<Link
											to='career'
											className='flex justify-end items-center'
										>
											<RightNavIcon color='#ffffff' />
										</Link>
									</div>
								</div>
								<section className='flex flex-col lg:flex-row lg:justify-between counter-hegt'>
									<a
										href=''
										className='bg-white mb-8 lg:mb-0 px-4 py-8 relative career-hover career-width'
									>
										<p className='uppercase text-xs text-blue-500 mb-14'>
											in numbers
										</p>
										<motion.header className='relative flex flex-col'>
											<section className='flex flex-col justify-center items-center mb-4'>
												<CareerIcon className='mb-2' />
												<Counter from={0} to={203} />
												<h5 className='font-medium text-lg text-center'>
													Employees at Mojec
													international
												</h5>
											</section>
											<Link
												to='career'
												className='flex justify-end items-center'
											>
												<RightNavIcon color='#E5BD5C' />
											</Link>
										</motion.header>
									</a>
									<section
										className='career-width relative mt-8 lg:mt-0 h-full'
										style={{ background: '#307fe2' }}
									>
										<ol className='flex w-10 justify-between h-2 social-caurosel absolute z-10'>
											{job.map((twt, i) => (
												<li
													key={i}
													className={`cursor-pointer inline-block w-1/5 border border-white ${
														socialIndex === i
															? 'bg-white'
															: ''
													}`}
													onClick={() => {
														setSocialIndex(i);
														if (i < 1) {
															setIsScroll(false);
														} else {
															setIsScroll(true);
														}
													}}
												></li>
											))}
										</ol>
										<Link
											to='career'
											className='section-2 relative mb-8 lg:mb-0'
											className='flex pl-8 pr-4 py-8 flex-col'
										>
											<p className='uppercase text-xs text-white mb-10'>
												careers
											</p>

											<h5 className='text-lg font-medium text-white mb-4'>
												Featured Jobs
											</h5>
											<div className='overflow-hidden'>
												<motion.div
													initial='rest'
													animate='hover'
													variants={ContentVaraint}
													exit={{
														visibility: 'hidden',
													}}
													key={
														job[socialIndex]
															.tweetText
													}
												>
													<section className='flex h-auto relative mb-4 pl-6'>
														<motion.div
															initial='borderRest'
															animate='borderAnimate'
															variants={
																ContentVaraint
															}
															className='border-white border-l h-auto mr-2'
														></motion.div>
														<div className=''>
															<p className='text-sm font-medium text-white mb-1'>
																{
																	job[
																		socialIndex
																	].tweetText
																}
															</p>
															<p className='text-white italic text-xs'>
																{
																	job[
																		socialIndex
																	].postedOn
																}
															</p>
														</div>
													</section>
													<section className='flex h-auto relative pl-6'>
														<motion.div
															initial='borderRest'
															animate='borderAnimate'
															variants={
																ContentVaraint
															}
															className='border-white border-l h-auto mr-2'
														></motion.div>
														<div className=''>
															<p className='text-sm font-medium text-white mb-1'>
																{
																	job2[
																		socialIndex
																	].tweetText
																}
															</p>
															<p className='text-white italic text-xs'>
																{
																	job2[
																		socialIndex
																	].postedOn
																}
															</p>
														</div>
													</section>
												</motion.div>
											</div>
											<div className='flex justify-end items-center absolute right-8 bottom-10'>
												<RightNavIcon color='#ffffff' />
											</div>
										</Link>
									</section>
								</section>
							</section>
						</div>
						<h2 className='font-bold capitalize text-3xl inline-block  text-gray-600 absolute z-10'>
							careers
						</h2>
					</div>
				</CareerWrapper>
			</SkewWraper>
		</FaderWrap>
	);
};

export default Careers;
