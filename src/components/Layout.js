import styled from 'styled-components';
import Nav from './Home/Nav';
import { motion } from 'framer-motion';
import Footer from './Footer';
import CompanyInfo from './CompanyInfo';
import MiniInfo from './MiniInfo';
import ArrowNavigation from './ArrowNavigation';
import FaderWrap from './FaderWrap';
import { verticals } from '../data/verticals';

const LayoutWrap = styled(motion.div)`
	background: #efefef;
	.heading {
		top: 30%;
	}

	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: 0rem;
		left: 1rem;
	}
`;

const HeroSectionWrap = styled.section`
	background: linear-gradient(180deg, #111b28 0%, rgba(0, 0, 0, 0.33) 100%);
	height: 103vh;
	z-index: 15;
	position: relative;
	bottom: 72px;

	@media (min-width: 1024px) {
		height: 100vh;
	}
`;

const ContentVaraint = {
	rest: {
		y: 300,
		opacity: 0,
	},

	hover: {
		y: 0,
		opacity: 1,
		transition: {
			delay: 0.5,
			type: 'tween',
			ease: 'easeOut',
			duration: 1.5,
		},
	},

	borderRest: {
		y: 50,
		opacity: 0,
	},

	borderAnimate: {
		y: 0,
		opacity: 1,
		transition: {
			delay: 1.5,
			type: 'tween',
			ease: 'easeOut',
			duration: 0.5,
		},
	},
};

const Layout = ({ children, image, title, desc, label, link, location }) => {
	return (
		<LayoutWrap className='h-auto'>
			<Nav className='z-50 relative' />
			<HeroSectionWrap></HeroSectionWrap>
			<section className='absolute top-0 h-screen w-full'>
				<ArrowNavigation to='details' />
				<motion.img
					src={image}
					className='z-10 h-screen w-full object-cover relative'
					alt='generation image'
				/>
				<div className='flex justify-center'>
					<div className='w-full 2xl:w-8/12'>
						<motion.header
							initial='rest'
							animate='hover'
							variants={ContentVaraint}
							className='absolute z-30 heading flex lg:w-9/12 2xl:w-2/4 flex-col px-4 py-2 lg:px-16'
						>
							<span className='bg-brand-blue flex justify-center items-center w-24 h-8 italic text-sm capitalize text-white mb-4'>
								{label}
							</span>
							<h1 className='text-white text-3xl font-bold lg:text-5xl mb-4 capitalize'>
								{title}
							</h1>
							<section className='flex justify-center items-center'>
								<div className='flex w-full lg:w-5/6 h-auto'>
									<motion.div
										initial='borderRest'
										animate='borderAnimate'
										variants={ContentVaraint}
										className='border-brand-yellow border-l mr-2'
									></motion.div>
									<div className=''>
										<p className='text-white text-xs lg:text-base font-medium mb-2'>
											{desc}
										</p>
									</div>
								</div>
							</section>
						</motion.header>
					</div>
				</div>
			</section>
			<FaderWrap>
				<div className='flex justify-center' id='details'>
					<section className='py-2 lg:px-32 px-8 pb-16 2xl:w-8/12'>
						{children}
					</section>
				</div>
			</FaderWrap>
			<FaderWrap>
				<div className='bg-gray-200 flex justify-center relative pt-8 '>
					<div className='2xl:w-8/12 relative'>
						<div className='lg:flex lg:justify-center'>
							<div className='grid gap-0 grid-cols-1 lg:grid-cols-2fr w-full'>
								{verticals
									.filter(
										(vertical) => vertical.link !== location
									)
									.map((vertical) =>
										vertical.id % 2 === 0 ? (
											<div
												className='w-full'
												key={vertical.id}
											>
												<CompanyInfo
													image={vertical.image}
													desc={vertical.desc}
													title={vertical.title}
													link={vertical.link}
												/>
											</div>
										) : (
											<div
												className='w-full'
												key={vertical.id}
											>
												<MiniInfo
													img={vertical.image}
													company={vertical.title}
													desc={vertical.desc}
													link={vertical.link}
												/>
											</div>
										)
									)}
							</div>
						</div>
						<h2 className='font-bold capitalize  text-xl lg:text-3xl inline-block text-gray-600 absolute z-10'>
							Businesses
						</h2>
					</div>
				</div>
			</FaderWrap>
			<Footer />
		</LayoutWrap>
	);
};

export default Layout;
