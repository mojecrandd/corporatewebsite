import styled from 'styled-components';
import ModalCloseIcon from '../assets/svg/ModalCloseIcon';
import Portal from './Portal';

const ModalWrap = styled.section`
	z-index: 300;
	background: rgba(0, 0, 0, 0.5);
	backdrop-filter: blur(0.5px);
	transition: all 1s ease-in-out;
`;

const Modal = ({ data, setModal }) => {
	const [{ name, image, position, header, details }] = data;

	const handleCloseModal = (e) => {
		if (e.target.classList.contains('mini-nav-wrap')) {
			setModal(false);
		}
	};

	return (
		<Portal>
			<ModalWrap
				className='flex justify-center items-center h-full w-full fixed lg:p-20  mini-nav-wrap'
				onClick={handleCloseModal}
			>
				<div className='bg-white w-full h-screen p-8 overflow-y-scroll flex justify-center'>
					<div className='2xl:w-8/12'>
						<div className='flex justify-end mb-6'>
							<ModalCloseIcon
								className='cursor-pointer'
								onClick={() => setModal(false)}
							/>
						</div>
						<section className='flex flex-col mb-6 lg:flex-row lg:px-12 lg:justify-between'>
							<div className='mb-6 lg:w-3/5'>
								<h1 className='text-2xl font-extrabold text-brand-blue mb-2 capitalize'>
									{name}
								</h1>
								<h3 className='text-gray-500 text-xl capitalize'>
									{position}
								</h3>
								<div className='flex h-auto ml-8 mt-8'>
									<div className='border-brand-yellow border-l h-auto mr-2'></div>
									<h3 className='hidden lg:block text-2xl font-medium leading-10'>
										{header}
									</h3>
								</div>
							</div>
							<div className='relative h-96 lg:w-1/3 border-overlay cursor-pointer'>
								<img
									src={image}
									className='h-full w-full object-cover'
									alt={name}
								/>
							</div>
						</section>
						<section className='px-0 lg:px-8 flex flex-col justify-center'>
							<p className='font-bold text-base lg:hidden mb-6'>
								{header}
							</p>
							{details.map((data) => (
								<p
									className='text-sm font-medium mb-6 leading-6'
									key={data.id}
								>
									{data.desc}
								</p>
							))}
							<div className='flex justify-center w-full'>
								<div className='border border-brand-yellow w-1/4'></div>
							</div>
						</section>
					</div>
				</div>
			</ModalWrap>
		</Portal>
	);
};

export default Modal;
