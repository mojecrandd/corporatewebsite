import { motion } from 'framer-motion';
import styled from 'styled-components';
import { useEffect, useState } from 'react';
import RefreshIcon from '../assets/svg/RefreshIcon';

const NewsWrapper = styled.section`
	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: -1.5rem;
		left: 1rem;
	}

	.section-1 {
		width: 100%;
		.cont-height {
			height: 25rem;
		}
	}
	.section-2 {
		height: 25rem;
		width: 100%;
	}

	.mini-caurosel {
		top: 350px;
		left: 46%;
	}

	.social-caurosel {
		top: 350px;
		left: 46%;
	}

	.arrow::after {
		border-bottom: 15px solid transparent;
		border-top: 15px solid transparent;
		border-right: 15px solid #fff;
		content: '';
		height: 0;
		position: absolute;
		right: 367px;
		top: 25px;
		width: 0;
		z-index: 1;
		display: none;
	}

	.arrow::before {
		border-bottom: 15px solid #fff;
		border-left: 15px solid transparent;
		border-right: 15px solid transparent;
		content: '';
		height: 0;
		position: absolute;
		right: 389px;
		top: 385px;
		width: 0;
		z-index: 1;
	}

	.wrapper-2 {
		transition: all 1s ease;
		.img-cover1 {
			background: linear-gradient(
				180deg,
				#111b28 0%,
				rgba(0, 0, 0, 0.43) 100%
			);
			height: 100%;

			& :hover {
				opacity: 0.8;
			}
		}
		.img-cover2 {
			background: linear-gradient(
				76.97deg,
				#111b28 34.77%,
				rgba(0, 0, 0, 0.43) 90.6%
			);
			height: 100%;

			& :hover {
				opacity: 0.8;
			}
		}

		.section-hgt {
			height: 25rem;
		}
	}

	@media (min-width: 1024px) {
		.section-1 {
			width: 65%;
		}

		.section-2 {
			width: 30%;
		}

		.arrow::after {
			display: block;
		}

		.arrow::before {
			display: none;
		}

		.social-caurosel {
			left: 160px;
		}

		.mini-caurosel {
			left: 15%;
		}
	}

	@media (min-width: 1536px) {
		.social-caurosel {
			left: 48%;
		}

		.mini-caurosel {
			left: 15%;
		}

		.arrow::after {
			right: 50%;
		}
	}
`;
const GenericIntheNews = ({ title, quotes, data }) => {
	const [newsIndex, setNewsIndex] = useState(0);
	const [socialIndex, setSocialIndex] = useState(0);
	const [isScroll, setIsScroll] = useState(true);

	//socials slider control
	const socialSliderRight = () => {
		setSocialIndex((socialIndex + 1) % quotes.length);
	};

	const slideNewsRight = () => {
		setNewsIndex((newsIndex + 1) % data.length); // increases index by 1
	};

	const ContentVariant = {
		rest: {
			opacity: 0,
		},

		hover: {
			opacity: 1,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 2,
			},
		},

		rest2: {
			x: isScroll ? 300 : -300,
		},

		hover2: {
			x: 0,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 0.8,
			},
		},

		borderRest: {
			y: 50,
			opacity: 0,
		},

		borderAnimate: {
			y: 0,
			opacity: 1,
			transition: {
				delay: 1,
				type: 'tween',
				ease: 'easeOut',
				duration: 0.5,
			},
		},
	};

	useEffect(() => {
		const timer = setTimeout(() => {
			slideNewsRight();
		}, 10000);

		return () => {
			clearTimeout(timer);
		};
	}, [newsIndex]);

	return (
		<NewsWrapper className='px-4 py-2 lg:px-16 relative my-10'>
			<div className='relative flex justify-center'>
				<ol className='flex w-10 justify-between h-2 mini-caurosel absolute z-10'>
					<li
						className={`cursor-pointer inline-block w-1/5 border border-white ${
							newsIndex === 0 ? 'bg-white' : ''
						}`}
						onClick={() => {
							setNewsIndex(0);
							setIsScroll(false);
						}}
					></li>
					<li
						className={`cursor-pointer inline-block w-1/5 border border-white ${
							newsIndex === 1 ? 'bg-white' : ''
						}`}
						onClick={() => {
							setNewsIndex(1);
							if (newsIndex > 1) {
								setIsScroll(false);
							} else {
								setIsScroll(true);
							}
						}}
					></li>
					<li
						className={`cursor-pointer inline-block w-1/5 border border-white ${
							newsIndex === 2 ? 'bg-white' : ''
						}`}
						onClick={() => {
							setNewsIndex(2);
							setIsScroll(true);
						}}
					></li>
				</ol>
				<div className='flex flex-col lg:justify-between lg:flex-row overflow-hidden relative'>
					<div className='relative overflow-hidden section-1 w-full'>
						<motion.section
							initial='rest2'
							animate='hover2'
							variants={ContentVariant}
							key={data[newsIndex].img}
							exit={{ visibility: 'hidden' }}
							className='flex flex-col lg:flex-row relative'
						>
							<div className='bg-gray-300 w-full lg:w-3/6 cont-height'>
								<img
									src={data[newsIndex].img}
									alt={data[newsIndex].altText}
									className='h-full object-cover w-full'
								/>
							</div>
							<div className='bg-white w-full lg:w-3/6 arrow px-4 py-8'>
								<p className='uppercase text-xs text-blue-500 mb-14'>
									press release
								</p>
								<motion.header className='relative flex flex-col'>
									<h1 className='text-gray-700 text-xl font-bold mb-6'>
										{data[newsIndex].headerText}
									</h1>
									<section className='flex justify-center items-center'>
										<div className='flex h-auto w-3/4'>
											<motion.div
												initial='borderRest'
												animate='borderAnimate'
												variants={ContentVariant}
												className='border-brand-yellow border-l mr-2'
											></motion.div>
											<div className=''>
												<p className='text-gray-600 text-base font-medium mb-2'>
													{data[newsIndex].descText}
												</p>
											</div>
										</div>
									</section>
								</motion.header>
							</div>
						</motion.section>
					</div>
					<motion.section
						className={`section-2 ${quotes[socialIndex].bgColor} relative mt-8 lg:mt-0`}
						variants={ContentVariant}
						exit={{ visibility: 'hidden' }}
						key={quotes[socialIndex].quoteWord}
					>
						<ol className='flex w-10 justify-between h-2 social-caurosel absolute z-10'>
							<li>
								<RefreshIcon
									className='cursor-pointer'
									onClick={socialSliderRight}
								/>
							</li>
						</ol>
						<a
							href='//'
							className='section-2 relative mb-8 lg:mb-0'
							className='flex pl-8 pr-4 py-8 flex-col'
						>
							<h6 className='font-medium text-white uppercase text-sm mb-16'>
								quotes box
							</h6>
							<section className='flex h-auto relative ml-8'>
								<motion.div
									initial='borderRest'
									animate='borderAnimate'
									variants={ContentVariant}
									exit={{ visibility: 'hidden' }}
									key={quotes[socialIndex].quoteWord}
									className='border-brand-yellow border-l h-auto mr-2'
								></motion.div>
								<motion.div
									initial='rest'
									animate='hover'
									variants={ContentVariant}
									exit={{ visibility: 'hidden' }}
									key={quotes[socialIndex].quoteWord}
								>
									<p className='text-sm font-medium text-white mb-1'>
										{quotes[socialIndex].quoteWord}
									</p>
									<p className='text-white italic text-xs font-bold mt-2'>
										{quotes[socialIndex].quoteBy}
									</p>
								</motion.div>
							</section>
						</a>
					</motion.section>
				</div>
			</div>
			<h2 className='font-bold capitalize text-xl lg:text-3xl inline-block text-gray-600 absolute z-10'>
				{title}
			</h2>
		</NewsWrapper>
	);
};

export default GenericIntheNews;
