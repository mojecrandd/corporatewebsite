import styled from 'styled-components';
import QouteGenerator from './QouteGenerator';

const JobWrap = styled.div`
	.section-1 {
		width: 100%;
	}

	.section-2 {
		width: 100%;
	}

	@media (min-width: 1024px) {
		.section-1 {
			width: 749px;
		}

		.section-2 {
			width: 390px;
		}
	}
`;

const quotes = [
	{
		quoteWord:
			"People need to feel safe to be who they are—to speak up when they have an idea, or to speak out when they feel something isn't right.",
		quoteBy: '― Eunice Parisi-Carew',
		bgColor: 'bg-black bg-opacity-75',
	},
	{
		quoteWord:
			'Your personal and professional lives will have to go hand in hand and will have influence on each other.',
		quoteBy: '― Abhishek Ratna',
		bgColor: 'bg-brand-blue',
	},
	{
		quoteWord:
			'Man can work anywhere and under any condition, but working in a very beautiful place under very beautiful conditions will surely create superior things!',
		quoteBy: '― Mehmet Murat ildan',
		bgColor: 'bg-blue-900',
	},
];

const JobListing = () => {
	return (
		<JobWrap className='flex flex-col lg:flex-row lg:justify-between'>
			<section className='section-1 px-4 py-8 lg:px-8 h-96 section-1 max-h-96 relative lg:mb-0 bg-brand-blue'>
				<h1 className='text-2xl font-medium text-white'>
					Opening at Mojec International
				</h1>
                <div className="flex justify-center h-4/5 items-center">
                    <p className='text-white italic text-sm'>We currently do not have any opening</p>
                </div>
			</section>
			<div className='section-2 h-96'>
				<QouteGenerator quotes={quotes} />
			</div>
		</JobWrap>
	);
};

export default JobListing;
