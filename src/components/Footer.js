import styled from 'styled-components';
import ContactFooter from '../assets/svg/ContactFooter';
import FacebookFooter from '../assets/svg/FacebookFooter';
import InstagramFooter from '../assets/svg/InstagramFooter';
import LinkedInFooter from '../assets/svg/LinkedIInFooter';
import TwitterFooter from '../assets/svg/TwitterFooter';
import mojecLogo from '../assets/images/mojeclogoW.png';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import ReactTooltip from 'react-tooltip';

const FooterWrap = styled.footer``;

const Footer = () => {
	return (
		<FooterWrap className='px-4 pb-8 pt-16 lg:px-16 bg-brand-blue'>
			<section className='flex justify-center'>
				<div className='w-full flex flex-col lg:flex-row 2xl:w-8/12'>
					<section className='hidden w-8/12 lg:flex justify-between pr-16'>
						<div className=''>
							<Link
								to='#'
								className='cursor-pointer capitalize text-white font-semibold inline-block text-lg mb-6 hover:opacity-70 transition-all ease-in'
							>
								business
							</Link>
							<ul>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='generation'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										generation
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='transmission'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										transmission
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='distribution'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										distribution
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='metering'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										mojec meter company
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='energymanagement'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										energy management
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='agriculture'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										agriculture
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='realestate'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										real estate
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='retail'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										retail
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='energy'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										oil and gas
									</Link>
								</motion.li>
							</ul>
						</div>
						<div className=''>
							<Link
								to='#'
								className='cursor-pointer capitalize text-white font-semibold inline-block text-lg mb-6 hover:opacity-70 transition-all ease-in'
							>
								corporate information
							</Link>
							<ul>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='history'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										history
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='ourpeople'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										our people
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='r&d'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										research & development
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='energy'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										energy
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='corporatevalue'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										corporate value
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='career'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										career
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='corpsocial'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										corporate social responsibility
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='manufacturingfacility'
										className='cursor-pointer capitalize text-sm font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										manufacturing facility
									</Link>
								</motion.li>
							</ul>
						</div>
						<div className=''>
							<ul>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<a
										href='http://reconcile.mojec.com/'
										target='_blank'
										rel='noopener noreferrer'
										className='cursor-pointer capitalize text-lg font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										verify & update payment info
									</a>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<a
										href='http://retrieval.mojec.com/'
										target='_blank'
										rel='noopener noreferrer'
										className='cursor-pointer capitalize text-lg font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										retrieve payment ref
									</a>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<a
										href='http://powergenie.com.ng/'
										target='_blank'
										rel='noopener noreferrer'
										className='cursor-pointer capitalize text-lg font-medium mb-16 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										power genie
									</a>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='privacy'
										className='cursor-pointer capitalize text-lg font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										privacy policy
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='termsofuse'
										className='cursor-pointer capitalize text-lg font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										terms of use
									</Link>
								</motion.li>
								<motion.li
									whileHover={{
										scale: 1.1,
									}}
								>
									<Link
										to='disclaimer'
										className='cursor-pointer capitalize text-lg font-medium mb-2 inline-block text-white hover:opacity-70 transition-all ease-in'
									>
										legal disclaimer
									</Link>
								</motion.li>
							</ul>
						</div>
					</section>
					<section className='w-full lg:w-4/12 lg:pl-6 lg:border-l lg:border-white'>
						<div className='w-full flex justify-between items-center mb-20 lg:mb-40'>
							<a
								href='https://www.facebook.com/MOJECMeter'
								target='_blank'
								rel='noopener noreferrer'
							>
								<FacebookFooter data-tip data-for='facebook' />
								<ReactTooltip
									id='facebook'
									place='top'
									// effect='solid'
									type='light'
								>
									Facebook
								</ReactTooltip>
							</a>
							<a
								href='https://www.linkedin.com/company/mojec-international-limited/'
								target='_blank'
								rel='noopener noreferrer'
							>
								<LinkedInFooter data-tip data-for='linkedIn' />
								<ReactTooltip
									id='linkedIn'
									place='top'
									// effect='solid'
									type='light'
								>
									LinkedIn
								</ReactTooltip>
							</a>
							<a
								href='https://twitter.com/MOJECMeter'
								target='_blank'
								rel='noopener noreferrer'
							>
								<TwitterFooter data-tip data-for='twitter' />
								<ReactTooltip
									id='twitter'
									place='top'
									// effect='solid'
									type='light'
								>
									Twitter
								</ReactTooltip>
							</a>
							<a
								href='https://www.instagram.com/mojec.meter/'
								target='_blank'
								rel='noopener noreferrer'
							>
								<InstagramFooter
									data-tip
									data-for='instagram'
								/>
								<ReactTooltip
									id='instagram'
									place='top'
									// effect='solid'
									type='light'
								>
									Instagram
								</ReactTooltip>
							</a>
							<Link to='contact'>
								<ContactFooter data-tip data-for='contactus' />
								<ReactTooltip
									id='contactus'
									place='top'
									// effect='solid'
									type='light'
								>
									Contact Us
								</ReactTooltip>
							</Link>
						</div>

						<section className='flex justify-around items-center  lg:hidden mb-12'>
							<Link
								to='career'
								className='text-white text-xs capitalize'
							>
								career
							</Link>
							<Link
								to='privacy'
								className='text-white text-xs capitalize'
							>
								privacy policy
							</Link>
							<Link
								to='termsofuse'
								className='text-white text-xs capitalize'
							>
								terms of use
							</Link>
							<Link
								to='disclaimer'
								className='text-white text-xs capitalize'
							>
								legal disclaimer
							</Link>
						</section>

						<Link
							to='#'
							className='flex justify-center items-center'
						>
							<img
								src={mojecLogo}
								className='h-14 lg:h-52'
								alt='Mojec-Logo'
							/>
						</Link>
					</section>
				</div>
			</section>
			<p className='text-white text-xs font-light text-center mt-6'>
				&copy; 2021 Mojec International Limited. All rights reserved.
			</p>
		</FooterWrap>
	);
};

export default Footer;
