import { animate } from 'framer-motion';
import { useEffect, useRef } from 'react';
import { useInView } from 'react-intersection-observer';

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const Counter = ({ from, to }) => {
	const nodeRef = useRef();
	const [ref, inView] = useInView();

	useEffect(() => {
		const node = nodeRef.current;
		if (inView) {
			const controls = animate(from, to, {
				duration: 1,
				onUpdate(value) {
					node.textContent = numberWithCommas(value);
				},
			});
            return () => controls.stop();
		}

	}, [from, to, inView]);

	return (
		<div ref={ref}>
			<h1
				className='text-3xl font-bold text-brand-blue mb-6'
				ref={nodeRef}
			/>
		</div>
	);
};

export default Counter;
