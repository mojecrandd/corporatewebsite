import { motion } from 'framer-motion';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import RightNavIcon from '../assets/svg/RightNavIcon';

const CompanyWrap = styled.section`
	.cont-height {
		height: 25rem;
	}

	.arrow::after {
		border-bottom: 15px solid transparent;
		border-top: 15px solid transparent;
		border-right: 15px solid #fff;
		content: '';
		height: 0;
		position: absolute;
		left: -15px;
		top: 25px;
		width: 0;
		z-index: 1;
		display: none;
	}

	.arrow::before {
		border-bottom: 15px solid #fff;
		border-left: 15px solid transparent;
		border-right: 15px solid transparent;
		content: '';
		height: 0;
		position: absolute;
		right: 389px;
		top: 385px;
		width: 0;
		z-index: 1;
	}

	@media (min-width: 1024px) {
		.arrow::after {
			display: block;
		}

		.arrow::before {
			display: none;
		}
	}

	@media (min-width: 2000px) {
		.arrow::after {
			left: 49.2%;
		}
	}
`;

const CompanyInfo = ({ image, title, desc, link, showIcon = true }) => {
	return (
		<CompanyWrap className='px-4 py-8 lg:px-8 relative lg:mb-0'>
			<div className='relative '>
				<div className='flex flex-col lg:justify-between lg:flex-row overflow-hidden relative'>
					<Link
						to={link}
						className='flex flex-col lg:flex-row section-1 relative'
					>
						<div className='bg-gray-300 w-full lg:w-3/6 cont-height'>
							<img
								src={image}
								alt={title}
								className='h-full object-cover w-full'
							/>
						</div>
						<div className='bg-white w-full lg:w-3/6 arrow px-4 py-8 relative cont-height'>
							<p
								className={`${
									showIcon ? 'block' : 'hidden'
								} uppercase text-xs text-blue-500 mb-2`}
							>
							
							</p>
							<motion.header className='relative flex flex-col h-full justify-center'>
								<h1 className='text-gray-700 text-xl font-bold mb-6 capitalize'>
									{title}
								</h1>
								<section className='flex justify-center items-center'>
									<div className='flex w-3/4'>
										<motion.div
											initial='borderRest'
											animate='borderAnimate'
											className='border-brand-yellow border-l mr-2'
										></motion.div>
										<div className=''>
											<p className='text-gray-600 text-sm font-medium mb-2'>
												{desc}
											</p>
										</div>
									</div>
								</section>
							</motion.header>
							<div
								className={`${
									showIcon ? 'flex' : 'hidden'
								} absolute z-10 justify-end bottom-5 w-full right-6`}
							>
								<RightNavIcon color='#E5BD5C' />
							</div>
						</div>
					</Link>
				</div>
			</div>
		</CompanyWrap>
	);
};

export default CompanyInfo;
