import { motion } from 'framer-motion';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import RightNavIcon from '../assets/svg/RightNavIcon';

const MiniWrap = styled.section`
	.cont-height {
		height: 25rem;
	}

	.overlay {
		background: rgba(0, 0, 0, 0.29);
	}

	@media (min-width: 1024px) {
		/* width: 30%; */
	}
`;

const MiniInfo = ({ img, link, company, desc, showIcon = true }) => {
	return (
		<MiniWrap className='w-full relative px-4 lg:py-8 lg:px-8  mb-16 lg:mb-0'>
			<Link
				to={link}
				className='w-full h-auto block relative cont-height'
			>
				<img
					src={img}
					alt={company}
					className='object-cover w-full h-full'
				/>
				<div className=' w-full px-4 py-8 absolute z-10 top-0 h-full overlay'>
					<p
						className={`${
							showIcon ? 'block' : 'hidden'
						} uppercase text-xs text-white mb-0`}
					>
					
					</p>
					<motion.header className='flex flex-col  h-full justify-center'>
						<h1 className='text-white text-xl font-bold mb-6'>
							{company}
						</h1>
						<section className='flex justify-center items-center'>
							<div className='flex w-3/4 h-auto relative'>
								<motion.div
									initial='borderRest'
									animate='borderAnimate'
									className='border-brand-yellow border-l mr-2'
								></motion.div>
								<div className=''>
									<p className='text-white text-sm font-medium '>
										{desc}
									</p>
								</div>
							</div>
						</section>
						<div
							className={`${
								showIcon ? 'flex' : 'hidden'
							} justify-end  absolute right-5 bottom-5`}
						>
							<RightNavIcon color='#ffffff' />
						</div>
					</motion.header>
				</div>
			</Link>
		</MiniWrap>
	);
};

export default MiniInfo;
