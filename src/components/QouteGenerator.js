import { motion } from 'framer-motion';
import React, { useState } from 'react';
import RefreshIcon from '../assets/svg/RefreshIcon';

const QouteGenerator = ({ quotes }) => {
	const [socialIndex, setSocialIndex] = useState(0);

	//socials slider control
	const socialSliderRight = () => {
		setSocialIndex((socialIndex + 1) % quotes.length);
	};

	const ContentVariant = {
		rest: {
			opacity: 0,
		},

		hover: {
			opacity: 1,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 2,
			},
		},

		borderRest: {
			y: 50,
			opacity: 0,
		},

		borderAnimate: {
			y: 0,
			opacity: 1,
			transition: {
				delay: 1,
				type: 'tween',
				ease: 'easeOut',
				duration: 0.5,
			},
		},
	};

	return (
		<motion.section
			className={`w-full ${quotes[socialIndex].bgColor} relative mt-8 lg:mt-0 h-full`}
			variants={ContentVariant}
			exit={{ visibility: 'hidden' }}
			key={quotes[socialIndex].quoteWord}
		>
			<div
				className={`w-full ${quotes[socialIndex].bgColor} relative mt-8 lg:mt-0 h-full`}
			>
				<ol className='flex w-10 justify-between h-2 social-caurosel absolute z-10 bottom-10 left-1/2'>
					<li>
						<RefreshIcon
							className='cursor-pointer'
							onClick={socialSliderRight}
						/>
					</li>
				</ol>
				<a
					href='//'
					className='section-2 relative mb-8 lg:mb-0'
					className='flex pl-8 pr-4 py-8 flex-col'
				>
					<h6 className='font-medium text-white uppercase text-sm mb-16'>
						quotes box
					</h6>
					<section className='flex h-auto relative ml-8'>
						<motion.div
							initial='borderRest'
							animate='borderAnimate'
							variants={ContentVariant}
							exit={{ visibility: 'hidden' }}
							key={quotes[socialIndex].quoteWord}
							className='border-brand-yellow border-l h-auto mr-2'
						></motion.div>
						<motion.div
							initial='rest'
							animate='hover'
							variants={ContentVariant}
							exit={{ visibility: 'hidden' }}
							key={quotes[socialIndex].quoteWord}
						>
							<p className='text-sm font-medium text-white mb-1'>
								{quotes[socialIndex].quoteWord}
							</p>
							<p className='text-white italic text-xs font-bold mt-2'>
								{quotes[socialIndex].quoteBy}
							</p>
						</motion.div>
					</section>
				</a>
			</div>
		</motion.section>
	);
};

export default QouteGenerator;
