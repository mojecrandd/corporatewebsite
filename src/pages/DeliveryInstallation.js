import Layout from '../components/Layout';
import mojecInstaller from '../assets/images/timeline/mojecInstaller.jpg';
import { Helmet } from 'react-helmet';

const DeliveryInstallation = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Delivery & Installation</title>‍
				<meta name="description" content="Prioritising customer satisfaction" />
			</Helmet>
			<Layout
				title='delivery & installation'
				desc='We prioritise customer satisfaction during and after the offering of services'
				image={mojecInstaller}
				label='business'
			>
				<p className='font-medium mb-8'>
					MOJEC looks after its customers by giving them good and timely
					delivery of their choice and at their own convenient time. MOJEC
					is also a registered installer, certified by NERC in the
					installation of the meters at the customer's wish.
				</p>
				<p className='font-medium mb-8'>
					Mojec has the quickest and easiest way of delivery because we
					have representative in 80% of all the Distribution companies the
					country (Nigeria) and with this; we are able to satisfy our
					customer. We have the capacity to install 600 metering Unit a
					day without reducing the quality of installation. Our
					installation specialists are fully qualified and guaranteed.
				</p>
			</Layout>
		</>
	);
};

export default DeliveryInstallation;
