import styled from 'styled-components';
import Footer from '../components/Footer';
import BrandSlider from '../components/Home/BrandSlider';
import Careers from '../components/Home/Careers';
import Nav from '../components/Home/Nav';
import NewsSlider from '../components/Home/NewsSlider';
import Slider from '../components/Home/Slider';
import TopStories from '../components/Home/TopStories';
import bgoverlay from '../assets/images/bgoverlay.png';
import Awards from '../components/Home/Awards';

const HomeWrapper = styled.div`
	background: #efefef;
`;

const HeroSectionWrap = styled.section`
	height: 103vh;
	z-index: 15;
	position: relative;
	bottom: 72px;

	@media (min-width: 1024px) {
		height: 100vh;
	}
`;
const Home = () => {
	return (
		<HomeWrapper className='h-auto'>
			<Nav className='z-50 relative' />
			<HeroSectionWrap>
				<img
					src={bgoverlay}
					alt='background image'
					className='w-full h-full invisible'
				/>
			</HeroSectionWrap>
			<Slider id='top-story' />
			<NewsSlider id='in-the-news' />
			<TopStories id='social-media' />
			<Careers id='careers' />
			<Awards id='awards'/>
			<BrandSlider id='brand-slider' />
			<Footer />
		</HomeWrapper>
	);
};

export default Home;
