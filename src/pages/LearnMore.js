import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import learnMore from '../assets/images/learnMore.jpg';
import CompanyInfo from '../components/CompanyInfo';
import Footer from '../components/Footer';
import LayoutGeneric from '../components/LayoutGeneric';
import MiniInfo from '../components/MiniInfo';
import { verticals } from '../data/verticals';

const LearnWrap = styled.div`
	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: 1rem;
		left: 1rem;
	}
`;

const LearnMore = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Learn more</title>‍
				<meta name="description" content="Learn more about MOJEC" />
			</Helmet>
			<LearnWrap>
				<LayoutGeneric
					title='mojec international business overview'
					desc='With her headquarters located in Lagos, Nigeria, the MOJEC group of companies is a global conglomerate which began business operations in 1985 and has a stellar track record across the Nigerian Power Sector. '
					image={learnMore}
					needsCompany={false}
					label='business'
					to='details'
				>
					<p className='font-medium mb-8' id='details'>
						Her subsidiary company, MOJEC International or MOJEC Meter
						Company is the largest meter manufacturer in Nigeria. As a
						leading indigenous manufacturer and contractor to power
						utilities in Nigeria and across the West-African hemisphere,
						MOJEC has a current installed production capacity of up to
						1.2 million meters which is set to double in 2021.
					</p>
					<p className='font-medium mb-8'>
						MOJEC partners with major DisCos across the country, with a
						mission ‘To building a World of Possibilities, MOJEC seeks
						to transform the Power Generation Space, Electricity
						Distribution and Metering, by creating a world in Africa
						where Power is available for all twenty-four hours everyday
						24/7’.
					</p>
					<p className='font-medium mb-8'>
						MOJEC International is the principal investment holding
						company and arrowhead of the MOJEC Group.
					</p>
					<p className='font-medium mb-8'>
						In addition to lighting up Nigeria and Africa, MOJEC
						continues to illuminate Nigerian lives by her philanthropic
						efforts which support education, healthcare, livelihood
						generation and provision for the specially gifted children.
					</p>
					<p className='font-medium mb-8 '>
						Each of MOJEC’s subsidiaries operates inter-dependently
						under the guidance and supervision of the Board of
						Directors. There are 7 listed MOJEC subsidiaries whose
						combined market capitalization helps position MOJEC at the
						apex of the Nigerian Metering Space.
					</p>
					<p className='font-medium mb-8'>
						Subsidiaries include MOJEC Meters, MOJEC Power, MOJEC Solar,
						MOJEC Research & Development, MOJEC Technology, MOJEC Real
						Estate and MOJEC Mining.
					</p>
					<p className='font-bold mb-8'>
						Keep scrolling to learn more about our subsidiaries
					</p>
				</LayoutGeneric>
				<div className='bg-gray-200 relative pt-8 flex justify-center '>
					<section className='2xl:w-8/12 relative'>
						<div className='lg:flex justify-between'>
							{verticals
								.filter((vertical) => vertical.id === 1)
								.map((vertical) => (
									<div className='w-full lg:w-9/12'>
										<CompanyInfo
											image={vertical.image}
											desc={vertical.desc}
											title={vertical.title}
											link={vertical.link}
										/>
									</div>
								))}
							{verticals
								.filter((vertical) => vertical.id === 2)
								.map((vertical) => (
									<div className='w-full lg:w-w-30'>
										<MiniInfo
											img={vertical.image}
											company={vertical.title}
											desc={vertical.desc}
											link={vertical.link}
										/>
									</div>
								))}
						</div>
						<div className='lg:flex justify-between'>
							{verticals
								.filter((vertical) => vertical.id === 3)
								.map((vertical) => (
									<div className='w-full lg:w-w-30'>
										<MiniInfo
											img={vertical.image}
											company={vertical.title}
											desc={vertical.desc}
											link={vertical.link}
										/>
									</div>
								))}
							{verticals
								.filter((vertical) => vertical.id === 4)
								.map((vertical) => (
									<div className='w-full lg:w-9/12'>
										<CompanyInfo
											image={vertical.image}
											desc={vertical.desc}
											title={vertical.title}
											link={vertical.link}
										/>
									</div>
								))}
						</div>
						<div className='lg:flex justify-between'>
							{verticals
								.filter((vertical) => vertical.id === 5)
								.map((vertical) => (
									<div className='w-full lg:w-9/12'>
										<CompanyInfo
											image={vertical.image}
											desc={vertical.desc}
											title={vertical.title}
											link={vertical.link}
										/>
									</div>
								))}
							{verticals
								.filter((vertical) => vertical.id === 6)
								.map((vertical) => (
									<div className='w-full lg:w-w-30'>
										<MiniInfo
											img={vertical.image}
											company={vertical.title}
											desc={vertical.desc}
											link={vertical.link}
										/>
									</div>
								))}
						</div>
						<div className='lg:flex justify-between'>
							{verticals
								.filter((vertical) => vertical.id === 7)
								.map((vertical) => (
									<div className='w-full lg:w-w-30'>
										<MiniInfo
											img={vertical.image}
											company={vertical.title}
											desc={vertical.desc}
											link={vertical.link}
										/>
									</div>
								))}
							{verticals
								.filter((vertical) => vertical.id === 8)
								.map((vertical) => (
									<div className='w-full lg:w-9/12'>
										<CompanyInfo
											image={vertical.image}
											desc={vertical.desc}
											title={vertical.title}
											link={vertical.link}
										/>
									</div>
								))}
						</div>
						<div className='lg:flex justify-between'>
							{verticals
								.filter((vertical) => vertical.id === 9)
								.map((vertical) => (
									<div className='w-full lg:w-9/12'>
										<CompanyInfo
											image={vertical.image}
											desc={vertical.desc}
											title={vertical.title}
											link={vertical.link}
										/>
									</div>
								))}
							{verticals
								.filter((vertical) => vertical.id === 10)
								.map((vertical) => (
									<div className='w-full lg:w-w-30'>
										<MiniInfo
											img={vertical.image}
											company={vertical.title}
											desc={vertical.desc}
											link={vertical.link}
										/>
									</div>
								))}
						</div>
						<h2 className='font-bold capitalize text-3xl inline-block text-gray-600 absolute z-10'>
							Businesses
						</h2>
					</section>
				</div>
				<Footer />
			</LearnWrap>
		</>
	);
};

export default LearnMore;
