import Layout from '../components/Layout';
import agriculture from '../assets/images/agriculture.jpg';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

const Agriculture = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Agriculture</title>‍
				<meta name="description" content="MOJECs role in Agribusiness" />
			</Helmet>
			<Layout
				title='agriculture'
				desc='With a close collaboration with key stakeholders MOJEC drives nation building through her role in Agribusiness.'
				image={agriculture}
				label='business'
				location={pathname}
			>
				<p className='font-medium mb-8'>
					Agriculture is a major branch of the economy in Nigeria,
					providing employment for 70% of the population. The sector is
					being transformed by commercialization at the small, medium and
					large-scale enterprise levels. MOJEC is a consultant in
					Agribusiness and together with the Nigerian government, we're
					growing a better public understanding of the role of agriculture
					in culture and economy.
				</p>
				<p className='font-medium mb-8'>
					With a close collaboration with stakeholders, MOJEC will
					develop, expand and offer curriculum-based educational
					programming that will serve to improve safety of agricultural
					proceeds and to build a genuine understanding and appreciation
					for the role agriculture plays in society, the environment and
					the economy.
				</p>
			</Layout>
		</>
	);
};

export default Agriculture;
