import React from 'react';
import { Helmet } from 'react-helmet';
import Footer from '../components/Footer';
import Nav from '../components/Home/Nav';

const Privacy = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Privacy</title>‍
				<meta name="description" content="Our Privacy" />
			</Helmet>
			<div className='bg-gray-200 min-h-screen'>
				<Nav />
				<section className='mt-14 flex justify-center'>
					<div className='py-2 lg:px-32 px-8 pb-16 2xl:w-8/12'>
						<h1 className='text-3xl font-bold text-gray-600 mb-8 text-center'>
							Privacy Policy
						</h1>
						<p className='text-gray-600 mb-8 leading-8'>
							MOJEC International Limited is committed to maintaining
							your confidence and trust as it relates to the privacy
							of your information. Please read below and learn how we
							collect, protect, share and use your information as part
							of our technology platforms, including, without
							limitation, our websites, web pages, interactive
							features, applications, Twitter, Facebook and other
							social media networks.
						</p>
						<p className='text-gray-600 mb-8 leading-8'>
							We take your privacy seriously, no matter how you access
							our Platforms
						</p>
						<h2 className='text-gray-600 mb-8 font-bold text-xl'>
							1. Information we collect on our platform
						</h2>
						<p className='text-gray-600 mb-8 leading-8'>
							We may collect Personal Information (information that
							can be used to identify you as an individual) such as
							your name, email, telephone number, home address,
							demographic information (such as zip code, age). The
							types of Personal Information we collect may vary
							depending on your use of the features of the Platforms.
							MOJEC utilizes third-party social media management
							services to collect and analyze publicly available
							information on various social media sites. Information
							that you post on those sites, as well as publicly
							available information that you post on other pages made
							available through those sites and on other social media
							sites may be used by MOJEC for customer satisfaction,
							customized marketing, marketing analysis, consumer
							research and other legitimate business purposes.
						</p>
						<h2 className='text-gray-600 mb-8 font-bold text-xl'>
							2. How we use the information we collect
						</h2>
						<p className='text-gray-600 mb-8 leading-8'>
							We use the information we collect about and from you for
							a variety of business purposes such as to respond to
							your questions and requests such as requests for product
							information, and brochures; advise you of important
							safety-related information; enable you to participate in
							promotional activities, or other Programs; provide you
							with access to certain areas and features of the
							Platforms such as your interactions with other users.
						</p>
						<h2 className='text-gray-600 mb-8 font-bold text-xl'>
							3. Sharing of information
						</h2>
						<p className='text-gray-600 mb-8 leading-8'>
							Except as described here, we will not provide any of
							your Personal Information to any third parties without
							your specific consent. We may share non-Personal
							Information, such as aggregate data and Usage
							Information with third parties. We may also share your
							information as disclosed at the time you provide your
							information, as set forth in this Privacy Notice and in
							the following circumstances:
						</p>
						<p className='text-gray-600 mb-8 leading-8'>
							Third Parties Providing Services on Our Behalf. We may
							share your Personal Information with third parties that
							perform functions on our behalf (or on behalf of our
							partners) such as service providers that host or operate
							our Platforms, analyze data, process transactions and
							payments, fulfill orders or provide customer service;
							advertisers; sponsors or other third parties that
							participate in or administer our promotions, contests,
							sweepstakes, surveys or provide marketing or promotional
							assistance and "powered by" partners or partners in
							co-branded sites. Your Personal Information may also be
							used by us or shared with our subsidiaries, affiliates,
							sponsors, partners, advertisers or other third parties
							to provide you with product information and promotional
							and other offers.
						</p>
						<p className='text-gray-600 mb-8 leading-8'>
							Legal Disclosure. We may transfer and disclose your
							information to third parties to comply with a legal
							obligation; when we believe in good faith that the law
							or a governmental authority requires it; to verify or
							enforce our Terms of Use or other applicable policies;
							to address fraud, security or technical issues; to
							respond to an emergency; or otherwise to protect our
							rights or property or security of third parties,
							visitors to our Platforms or the public.
						</p>
						<h2 className='text-gray-600 mb-8 font-bold text-xl'>
							4. Children
						</h2>
						<p className='text-gray-600 mb-8 leading-8'>
							The Platforms are not directed to children under 13. We
							do not knowingly collect, use or disclose personally
							identifiable information from anyone under 13 years of
							age. If we determine upon collection that a user is
							under this age, we will not use or maintain his/her
							Personal Information without the parent/guardian's
							consent. If we become aware that we have unknowingly
							collected personally identifiable information from a
							child under the age of 13, we will make reasonable
							efforts to delete such information from our records.
						</p>
						<h2 className='text-gray-600 mb-8 font-bold text-xl'>
							5. Changes
						</h2>
						<p className='text-gray-600 mb-8 leading-8'>
							We may update this Privacy Notice to reflect changes to
							our information practices. If we make any material
							changes we will notify you by email (sent to the e-mail
							address specified in your account) or by means of a
							notice on our Platforms prior to the change becoming
							effective. We encourage you to periodically review this
							page for the latest information on our privacy
							practices.
						</p>
						<h2 className='text-gray-600 mb-8 font-bold text-xl'>
							6. Contact Us
						</h2>
						<p className='text-gray-600 mb-8 leading-8'>
							To contact us with a question call us at +234 17738393
							or mail your inquiry to: enquiries@mojec.com
						</p>
					</div>
				</section>
				<Footer />
			</div>
		</>
	);
};

export default Privacy