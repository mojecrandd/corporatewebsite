import LayoutGeneric from '../components/LayoutGeneric';
import csr1 from '../assets/images/csr1.jpg';
import GenericIntheNews from '../components/GenericIntheNews';
import Footer from '../components/Footer';
import child from '../assets/images/child.jpg';
import classroom from '../assets/images/classroom.jpg';
import stem from '../assets/images/stem.jpg';
import leadership from '../assets/images/leadership.jpg';
import palliatives from '../assets/images/palliatives.jpg';
import { data, quotes } from '../data/corpSocialResp';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

const CorpWrap = styled.div`
	.img-hgt {
		height: 6rem;
	}

	@media (min-width: 1024px) {
		.img-hgt {
			height: 30rem;
		}
	}
`;

const CorpSocialResp = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Social Responsibility</title>‍
				<meta name="description" content="Corporate social responsibility" />
			</Helmet>
			<LayoutGeneric
				title='corporate social responsibility'
				desc='MOJEC INTERNATIONAL has always placed people and their dreams at the center of all her processes and involvement with the Nigerian Power Sector. As a firm that believes in charitable causes beginning at home, MOJEC’s reach has started with her employees and their families and spread across toher clients and the African community as a whole.'
				image={csr1}
				needsCompany={false}
				label='information'
				to='details'
			>
				<p className='font-medium mb-4' id='details'>
					At the MOJEC Group, we are playing our part in building a
					sustainable future and creating a better, brighter world for
					our children to inherit. We believe social responsibility is
					a long-term investment that serves to strengthen our
					operations and competitiveness in the marketplace, enhance
					risk management, attract and engage talented employees, and
					maintain our reputation.
				</p>
				<p className='font-medium mb-8'>
					We are committed to providing comfort, inspiration and
					opportunity to children and families globally.
				</p>
				<section className='h-auto w-full p-4 lg:p-8 flex justify-between flex-col lg:flex-row mb-16 bg-gray-200'>
					<div className='h-24 lg:h-96 w-full lg:w-1/2 mb-4 lg:mb-0'>
						<img
							src={child}
							alt='children'
							className='h-full object-cover w-full'
						/>
					</div>
					<div className='w-full lg:w-5/12'>
						<h5 className='text-lg text-brand-blue font-bold mb-4'>
							SOCIAL IMPACT - MOJEC REFORMATORY HOME
						</h5>
						<section className='flex h-auto relative pl-6 lg:pl-12 w-full'>
							<div className='border-brand-yellow border-l h-auto mr-2'></div>
							<div className=''>
								<p className='text-sm font-medium mb-3'>
									We believe that a person is a person no
									matter how small and there is no keener
									revelation of a society’s soul than the way
									in which it treats its children.
								</p>
								<p className='text-sm font-medium mb-3'>
									Every child should have an opportunity to
									achieve their potential and the reformatory
									home was established with the objective of
									identifying social miscreants, school
									drop-outs and homeless kids in the society.
									The overall mission is to create a new life
									by re-integrating them back into society.
								</p>
								<p className='text-sm font-medium mb-3'>
									We are hoping to reduce the number of
									juvenile delinquents and general crime rate
									so that these kids do not become recidivists
									and hardened criminals at adulthood.
								</p>
							</div>
						</section>
					</div>
				</section>

				<section className='h-auto w-full p-4 lg:p-8 flex justify-between flex-col-reverse lg:flex-row mb-16 bg-white'>
					<div className='w-full lg:w-5/12'>
						<h5 className='text-lg text-brand-blue font-bold mb-4'>
							MOJEC MOJISOLA FOUNDATION
						</h5>
						<section className='flex h-auto relative pl-6 lg:pl-12 w-full'>
							<div className='border-brand-yellow border-l h-auto mr-2'></div>
							<div className=''>
								<p className='text-sm font-medium mb-3'>
									Children must be taught how to think and not
									what to think as they are not things to be
									molded but are people to be unfolded, and
									that is why the MMF invests in the
									educational future of specially gifted
									teenagers. There is ability in disability
									and the MMF gives out scholarships and sets
									up businesses for our special ones.
								</p>
								<p className='text-sm font-medium mb-3'>
									The ultimate goal is to allow them more
									opportunities to become active and
									productive members of the society.
								</p>
								<p className='text-sm font-medium mb-3'>
									The goal is to minimize the obstacles and
									restrictions that individuals with
									disabilities experience on a regular basis.
									The organization relies on advocacy,
									research, and education to achieve its
									goals.
								</p>
							</div>
						</section>
					</div>
					<div className='h-24 lg:h-96 w-full lg:w-1/2 mb-4 lg:mb-0'>
						<img
							src={classroom}
							alt='children in school'
							className='h-full object-cover w-full'
						/>
					</div>
				</section>

				<section className='h-auto w-full p-4 lg:p-8 flex justify-between flex-col lg:flex-row mb-16 bg-gray-200'>
					<div className='h-24 lg:h-96 w-full lg:w-1/2 mb-4 lg:mb-0'>
						<img
							src={stem}
							alt='stem electronics'
							className='h-full object-cover w-full'
						/>
					</div>
					<div className='w-full lg:w-5/12'>
						<h5 className='text-lg text-brand-blue font-bold mb-4'>
							MOJEC’S STEM PROGRAM
						</h5>
						<section className='flex h-auto relative pl-6 lg:pl-12 w-full'>
							<div className='border-brand-yellow border-l h-auto mr-2'></div>
							<div className=''>
								<p className='text-sm font-medium mb-3'>
									The need for qualified candidates in STEM
									(Science, Technology, Engineeringand
									Mathematics) industries continues to grow as
									the world becomes more digitally reliant. We
									are working to encourage interest in a STEM
									education among today’s youth as it is
									guaranteed to have a great impact on the
									futureof business processes around the
									world.
								</p>
								<p className='text-sm font-medium mb-3'>
									STEM is all around us, and affects every
									aspect of modern life. Just about every job
									requires STEM-related skills, whether it be
									using a computer or a soft skill like
									critical thinking or problem solving.
								</p>
								<p className='text-sm font-medium mb-3'>
									MOJEC has recognized the rapid growth in
									high-tech occupations and realizeda decline
									in qualified and diverse candidates for
									those positions. MOJEC also recognizes that
									combating this workforce shortage will only
									be made possible by placing a focus on
									improving education in these academic
									disciplines by helping teachers and their
									students understand how Science, Technology,
									Engineering and Mathematics impact our world
									and prepare for the workforce of tomorrow.
								</p>
								<p className='text-sm font-medium mb-3'>
									As a leading innovative organization, we
									believe that building the best mindscan
									start as far back as high schools, and
									exposing kids who have shown a large
									propensity for learning and education to
									subjects such as programmingand engineering
									, science and deep learning, which were once
									considered higher level languages will help
									pick the next generation of world leaders.
								</p>
								<p className='text-sm font-medium mb-3'>
									MOJEC also engages the teeming African youth
									who are full of brilliant ideas and concepts
									with the organization of immersive bootcamps
									which provide hands-on approaches towards
									specific fields in engineering and
									technology. We have also hosted as well as
									sponsored hackathons, leadership seminars
									and internships where young minds come
									together to conceptualize, visualize,
									design, implement and maintain processes
									that will change the world as we know it.
								</p>
							</div>
						</section>
					</div>
				</section>

				<section className='h-auto w-full p-4 lg:p-8 flex justify-between flex-col-reverse lg:flex-row mb-16 bg-white'>
					<div className='w-full lg:w-5/12'>
						<h5 className='text-lg text-brand-blue font-bold mb-4'>
							MOJEC FUTURE LEADERS INTERNSHIP PROGRAM (FLIP)
						</h5>
						<section className='flex h-auto relative pl-6 lg:pl-12 w-full'>
							<div className='border-brand-yellow border-l h-auto mr-2'></div>
							<div className=''>
								<p className='text-sm font-medium mb-3'>
									MOJEC’s FLIP program is aimed at giving
									students at college and university levels
									the ingenuity to find their ardor, curiosity
									and fervor in careers and lifeafter school.
								</p>
								<p className='text-sm font-medium mb-3'>
									Our programs span the critical years of
									adolescence up until early adulthood,helping
									young adults explore college and career
									possibilities, develop education plans and
									understand their own role in achieving
									success.
								</p>
								<p className='text-sm font-medium mb-3'>
									Chosen candidates are placed within the
									MOJEC INT’L group headquarters as they serve
									in their different capacities associated
									with their field of study andwill spend time
									learning operational processes, engineering
									techniques, administrative procedures and
									the amazing opportunity to imbibe from
									proven experts and top management, while
									developing critical professional skills
									through networking, research and
									communication.
								</p>
								<p className='text-sm font-medium mb-3'>
									MOJEC believes that there has never been a
									more important time for college students to
									gain necessary work experience before
									competing in what is becoming a
									super-competitive, global job market.
									Students who intern or co-op provide
									themselves opportunities to apply what they
									learn in the lecture halls and put it into
									practice at work.
								</p>
							</div>
						</section>
					</div>
					<div className='h-24 lg:h-96 w-full lg:w-1/2 mb-4 lg:mb-0'>
						<img
							src={leadership}
							alt='leading'
							className='h-full object-cover w-full'
						/>
					</div>
				</section>

				<section className='h-auto w-full p-4 lg:p-8 flex justify-between flex-col lg:flex-row bg-gray-200'>
					<div className='h-24 lg:h-96 w-full lg:w-1/2 mb-4 lg:mb-0'>
						<img
							src={palliatives}
							alt='sharing of palliatives'
							className='h-full object-cover w-full'
						/>
					</div>
					<div className='w-full lg:w-5/12'>
						<h5 className='text-lg text-brand-blue font-bold mb-4'>
							POVERTY ALLEVIATION COMMUNITY SERVICE (PACS) <br />
							MOJEC’S LEAP (Livelihood Empowerment Against
							Poverty) PROGRAM
						</h5>
						<section className='flex h-auto relative pl-6 lg:pl-12 w-full'>
							<div className='border-brand-yellow border-l h-auto mr-2'></div>
							<div className=''>
								<p className='text-sm font-medium mb-3'>
									We are well aware of the National Economic
									Decline across the world and especially how
									it has affected African and Nigerian
									families in particular, and while we cannot
									single-handedly change the current economic
									crisis, MOJEC will always try to lift some
									of the weight off the shoulders of families
									who need financial aid.
								</p>
								<p className='text-sm font-medium mb-3'>
									MOJEC provides monetary aid to the members
									of the Nigerian community, who still
									struggle to stay above the survival level of
									the food chain, particularly in households
									with orphans and/or vulnerable children who
									have been victims of rape/abuse, the elderly
									and people with extreme disabilities.
								</p>
								<p className='text-sm font-medium mb-3'>
									MOJEC also runs a non-profit that collects
									and distributes food to hunger-relief
									charities. Our staff volunteer and work
									together with partner agencies in the fight
									to end hunger. Our food drives help food
									banks, food pantries and soup kitchens
									collect food donations to keep their shelves
									stocked. We also use these means to raise
									awareness about hunger in every community we
									visit and encourage the well-to-do in these
									areas to also help out.
								</p>
								<p className='text-sm font-medium mb-3'>
									This drive helped in putting smiles to faces
									and families living below the minimum wage
									during the COVID-19 pandemic and the
									subsequent lockdown imposed by the Nigerian
									Government to help stop the spread of the
									virus.
								</p>
								<p className='text-sm font-medium mb-3'>
									MOJEC has touched the hearts of thousands of
									Nigerian with this initiative.
								</p>
							</div>
						</section>
					</div>
				</section>
			</LayoutGeneric>
			<div className='bg-gray-200 flex justify-center'>
				<div className='2xl:w-8/12'>
					<GenericIntheNews
						title='Socials'
						quotes={quotes}
						data={data}
					/>
				</div>
			</div>
			<Footer />
		</>
	);
};

export default CorpSocialResp;
