import React from 'react';
import { Helmet } from 'react-helmet';
import vendorFinance from '../assets/images/vendorFinance.jpg';
import Layout from '../components/Layout';

const VendorFinancing = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Vendor-Financing</title>‍
				<meta name="description" content="Vendor Financing" />
			</Helmet>
			<Layout
				title='vendor financing'
				desc='MOJEC act as a bridge
				between investors and the utility to achieve a common goal of
				bringing development to our environment'
				image={vendorFinance}
				label='information'
			>
				<p className='font-medium mb-8'>
					In ways that always yield dividends MOJEC act as a bridge
					between investors and the utility to achieve a common goal of
					bringing development to our environment. Time after time we have
					been referred to as the Big Link in making dreams come true and
					MOJEC has thrived with the conditions of certainty and
					uncertainty in allocation of assets and liabilities and bringing
					in investors either locally or outside the shores of Nigeria to
					meet immediate need for financing over time under.
				</p>
			</Layout>
		</>
	);
};

export default VendorFinancing;
