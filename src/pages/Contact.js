import { motion } from 'framer-motion';
import Nav from '../components/Home/Nav';
import mapBG from '../assets/images/mapBG.png';
import customerCare from '../assets/images/customerCare.jpg';
import Footer from '../components/Footer';
import MapIcon from '../assets/svg/MapIcon';
import styled from 'styled-components';
import { useState } from 'react';
import LocationIcon from '../assets/svg/LocationIcon';
import RightNavIcon from '../assets/svg/RightNavIcon';
import AddressIcon from '../assets/svg/AddressIcon';
import PhoneIcon from '../assets/svg/PhoneIcon';
import { Helmet } from 'react-helmet';

const ContactWrap = styled.div`
	.overlay {
		background: rgba(0, 0, 0, 0.329);
	}

	.lagos-pos {
		left: 50%;
		bottom: 110px;
	}

	.abuja-pos {
		left: 49%;
		bottom: 80px;
	}

	.lagos-popover {
		right: 30px;
	}

	.img-height {
		height: 80%;
	}

	@media (min-width: 1024px) {
		.lagos-pos {
			left: 50%;
			bottom: 155px;
		}

		.abuja-pos {
			left: 48.5%;
			bottom: 120px;
		}

		.lagos-popover {
			left: 50px;
		}
	}

	@media (min-width: 1430px) {
		.lagos-pos {
			left: 50%;
			bottom: 205px;
		}

		.abuja-pos {
			left: 49.5%;
			bottom: 155px;
		}

		.lagos-popover {
			left: 50px;
		}
	}

	@media (min-width: 1536px) {
		.lagos-pos {
			left: 50%;
			bottom: 48%;
		}

		.abuja-pos {
			left: 48%;
			bottom: 40%;
		}
		.lagos-popover {
			bottom: 10px;
		}
	}
`;

const ContentVaraint = {
	rest: {
		scale: 2,
	},

	active: {
		scale: 1,
		transition: {
			// delay: 0.5,
			type: 'tween',
			ease: 'easeOut',
			duration: 1.5,
		},
	},

	borderRest: {
		y: '-40vh',
		opacity: 0,
	},

	borderAnimate: {
		y: 0,
		opacity: 1,
		transition: {
			delay: 1.5,
			type: 'spring',
			stiffness: 200,
			duration: 1.5,
		},
	},
	borderRest2: {
		y: '-40vh',
		opacity: 0,
	},

	borderAnimate2: {
		y: 0,
		opacity: 1,
		transition: {
			delay: 2,
			type: 'spring',
			stiffness: 200,
			duration: 1.5,
		},
	},

	imgRest: {
		opacity: 0,
	},

	imgAnimate: {
		opacity: 1,
		transition: {
			duration: 1.5,
		},
	},
};

const Contact = () => {
	const [state, setState] = useState({
		lagos: false,
		abuja: false,
	});
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Contact</title>‍
				<meta name="description" content="Contact Us" />
			</Helmet>
			<ContactWrap className='bg-gray-200 min-h-screen relative'>
				<Nav />
				<section className='mt-5 relative'>
					<h1 className='capitalize text-2xl font-normal text-center mb-10'>
						contact us
					</h1>
					<div className='flex justify-center'>
						<section className='flex justify-between overflow-hidden img-height 2xl:w-4/5'>
							<motion.div
								initial='imgRest'
								animate='imgAnimate'
								variants={ContentVaraint}
								className='w-full h-auto relative lg:w-2/6 mb-10 lg:ml-8 lg:mb-0 bg-black'
							>
								<img
									src={customerCare}
									className='h-full w-full object-contain relative'
									alt='customer care'
								/>
								<div className='w-full px-4 py-8 absolute z-10 top-0 h-full overlay flex flex-col justify-center'>
									<section className='mb-8'>
										<h3 className='text-white text-lg lg:text-2xl font-bold capitalize mb-4'>
											Africa head office (lagos)
										</h3>
										<div className='flex items-center mb-2'>
											<AddressIcon className='mr-2' />
											<p className='text-white text-sm'>
												244/246 Apapa oshodi expressway,
												Lagos, Nigeria
											</p>
										</div>
										<div className='flex items-center'>
											<PhoneIcon className='mr-2' />
											<p className='text-white text-sm'>
												014537736, 014538015,08174589132
											</p>
										</div>
									</section>
									<section className='mb-8'>
										<h3 className='text-white text-lg lg:text-2xl font-bold capitalize mb-4'>
											Abuja office
										</h3>
										<div className='flex items-center mb-2'>
											<AddressIcon className='mr-2 w-10' />
											<p className='text-white text-sm'>
												Plot 1335 (7478) Close alexandra
												cresent off Mallam Aminu Kano
												cresent, zone A7 wuse11, Abuja, FCT.
											</p>
										</div>
										<div className='flex items-center'>
											<PhoneIcon className='mr-2' />
											<p className='text-white text-sm'>
												014537736, 014538015,08174589132
											</p>
										</div>
									</section>
								</div>
							</motion.div>
							<div className='hidden lg:block w-full h-full lg:w-2/3 relative scrollbar-hidden mb-10 max-w-full px-8'>
								<motion.img
									src={mapBG}
									initial='rest'
									animate='active'
									variants={ContentVaraint}
									className='h-full w-full object-cover relative'
									alt='generation image'
								/>
								<motion.div
									initial='borderRest'
									animate='borderAnimate'
									variants={ContentVaraint}
									className='absolute z-10 lagos-pos'
								>
									<div
										className={`lagos-popover bg-white border border-gray-400 px-4 py-2 absolute z-20 w-60 ${state.abuja ? 'block' : 'hidden'
											}`}
										onMouseEnter={() =>
											setState({
												...state,
												abuja: true,
												lagos: false,
											})
										}
										onMouseLeave={() =>
											setState({
												...state,
												abuja: false,
												lagos: false,
											})
										}
									>
										<div className='flex justify-between items-center mb-4'>
											<h2 className='capitalize text-2xl font-normal text-brand-blue'>
												Abuja
											</h2>
											<LocationIcon />
										</div>
										<p className='font-bold text-sm mb-1'>
											ABUJA OFFICE
										</p>
										<p className='text-xs capitalize mb-1'>
											PLOT 1335 (7478) CLOSE ALEXANDRA
											CRESCENT OFF MALLAM AMINU KANO CRESCENT,
											ZONE A7 WUSE11, ABUJA, FCT.
										</p>
										<p className='text-xs capitalize mb-1'>
											<span className='font-bold'>
												Phone:{' '}
											</span>{' '}
											014537736, 014538015,08174589132
										</p>
									</div>
									<MapIcon
										onClick={() =>
											setState({
												...state,
												abuja: !state.abuja,
												lagos: false,
											})
										}
										onMouseEnter={() =>
											setState({
												...state,
												abuja: true,
												lagos: false,
											})
										}
										className='cursor-pointer'
									/>
								</motion.div>
								<motion.div
									initial='borderRest2'
									animate='borderAnimate2'
									variants={ContentVaraint}
									className='absolute z-10 abuja-pos'
								>
									<div
										className={`lagos-popover bg-white border border-gray-400 px-4 py-2 absolute z-20 w-60 ${state.lagos ? 'block' : 'hidden'
											}`}
										onMouseEnter={() =>
											setState({
												...state,
												abuja: false,
												lagos: true,
											})
										}
										onMouseLeave={() =>
											setState({
												...state,
												abuja: false,
												lagos: false,
											})
										}
									>
										<div className='flex justify-between items-center mb-4'>
											<h2 className='capitalize text-2xl font-normal text-brand-blue'>
												Lagos
											</h2>
											<LocationIcon />
										</div>
										<p className='font-bold text-sm mb-1'>
											AFRICA HEAD OFFICE (LAGOS)
										</p>
										<p className='text-xs capitalize mb-1'>
											244/246 APAPA OSHODI EXPRESSWAY, LAGOS,
											NIGERIA
										</p>
										<p className='text-xs capitalize mb-1'>
											<span className='font-bold'>
												Phone:{' '}
											</span>{' '}
											014537736, 014538015,08174589132
										</p>
									</div>
									<MapIcon
										onClick={() =>
											setState({
												...state,
												abuja: false,
												lagos: !state.lagos,
											})
										}
										onMouseEnter={() =>
											setState({
												...state,
												abuja: false,
												lagos: true,
											})
										}
										className='cursor-pointer'
									/>
								</motion.div>
							</div>
						</section>
					</div>
					<div className='flex justify-center items-center mb-16'>
						<motion.a
							initial={{
								background: '#2659A2',
								borderWidth: -180,
							}}
							whileHover={{
								backgroundColor: '#13376A',
								borderWidth: 0,
							}}
							transition={{ duration: 0.5 }}
							href='mailto:support@mojec.com'
							className='flex justify-center items-center bg-brand-blue h-11 w-44 text-white'
						>
							<span className='text-sm font-bold inline-block mr-4'>
								Send us a mail
							</span>
							<motion.span
								initial={{ x: -10 }}
								animate={{ x: 0 }}
								transition={{
									yoyo: Infinity,
									duration: 0.5,
								}}
							>
								<RightNavIcon color='#ffffff' />
							</motion.span>
						</motion.a>
					</div>
				</section>
				<Footer />
			</ContactWrap>
		</>
	);
};

export default Contact;
