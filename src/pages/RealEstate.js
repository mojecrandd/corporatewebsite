import { Helmet } from 'react-helmet';
import { useLocation } from 'react-router-dom';
import real_estate from '../assets/images/real_estate.png';
import Layout from '../components/Layout';

const RealEstate = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Real Estate</title>‍
				<meta name="description" content="Real Estate" />
			</Helmet>
			<Layout
				title='real estate'
				desc='Provision of office and corporate residential leases to foreign and local companies wishing to house their staff'
				image={real_estate}
				label='business'
				location={pathname}
			>
				<h5 className='text-lg text-brand-blue font-bold mb-6'>
					MOJEC Real Estate
				</h5>
				<p className='font-medium mb-8'>
					Our real estate holdings consist of land and the building's
					ownership, along with its natural resources such as crops,
					minerals, or water; MOJEC is in the profession of buying,
					selling, or renting land, buildings or housing. MOJEC
					distinguishes itself in the commercial and hospitable real
					estate sector across Africa.
				</p>
				<p className='font-medium mb-8'>
					Presently in Nigeria, MOJEC is a major owner of several hectares
					of land space, office & residential buildings, it provides
					office and corporate residential leases to foreign and local
					companies wishing to house their foreign staff.
				</p>
				<h5 className='text-lg text-brand-blue font-bold mb-6'>
					MOJEC Hotels
				</h5>
				<p className='font-medium mb-8'>
					In 2012, MOJEC seeks to expand its operations by building its
					first chain of business hotels across the country to cater to a
					large number of business travelers, and ever since it has been a
					success story.
				</p>
			</Layout>
		</>
	);
};

export default RealEstate;
