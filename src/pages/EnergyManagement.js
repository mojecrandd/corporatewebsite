import Layout from '../components/Layout';
import meter3 from '../assets/images/meter3.jpg';
import NumberingIcon from '../assets/svg/NumberingIcon';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

const EnergyManagement = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Energy Management</title>‍
				<meta name="description" content="Energy Management" />
			</Helmet>
			<Layout
				title='energy management'
				desc='Leverage on MOJEC’s pool of technician, custom made electric items, and cost saving tips'
				image={meter3}
				label='business'
				location={pathname}
			>
				<p className='font-medium mb-8'>
					The ability to monitor energy consumption, identify waste and
					highlight areas for improvement is an essential part of energy
					management. MOJEC "power Genie" services can provides the
					solution.
				</p>
				<h5 className='text-lg text-brand-blue font-bold mb-8'>
					Our energy management solutions allow you to:
				</h5>
				<ul className='grid gap-8 grid-cols-1 lg:grid-cols-2'>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							monitor consumption
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							identify inefficiency
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							set consumption parameters and alert
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							analyze consumption patterns
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							improve forecasting and budget allocation accuracy
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							improve control methods
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							optimize load management
						</span>
					</li>
				</ul>
			</Layout>
		</>
	);
};

export default EnergyManagement;
