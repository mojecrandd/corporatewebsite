import { Helmet } from 'react-helmet';
import { useLocation } from 'react-router-dom';
import retail from '../assets/images/retail.jpg';
import Layout from '../components/Layout';

const Retail = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Retail</title>‍
				<meta name="description" content="MOJEC Retail outlet" />
			</Helmet>
			<Layout
				title='retail'
				desc='MOJEC  is part of an integrated system that makes the supply chain effective'
				image={retail}
				label='business'
				location={pathname}
			>
				<p className='font-medium mb-8'>
					With the lack of sufficient malls to cater to the growing
					appetite of Africa's most populous country Nigeria, MOJEC is
					gradually spread its reach in two major cities namely Lagos &
					Abuja. With the Abuja mall already running MOJEC is seeking to
					build another mall in Lagos in 2014.
				</p>
				<p className='font-medium mb-8'>
					Customer servicing and effective delivery of goods to end users
					is our bench mark. MOJEC is part of an integrated system that
					makes the supply chain effective.
				</p>
			</Layout>
		</>
	);
};

export default Retail;
