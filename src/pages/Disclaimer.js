import { Helmet } from 'react-helmet';
import Footer from '../components/Footer';
import Nav from '../components/Home/Nav';

const Disclaimer = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Disclaimer</title>‍
				<meta name="description" content="Disclaimer" />
			</Helmet>
			<div className='bg-gray-200 min-h-screen'>
				<Nav />
				<section className='mt-14 flex justify-center'>
					<div className='py-2 lg:px-32 px-8 pb-16 2xl:w-8/12'>
						<h1 className='text-3xl font-bold text-gray-600 mb-4 text-center'>
							Legal Disclaimer
						</h1>
						<h2 className='text-gray-600 mb-8 font-bold text-lg capitalize text-center'>
							Acquisition of electricity prepaid meters
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							This is to inform the general public that you may now
							apply and acquire{' '}
							<span className='font-bold'>Prepaid Meter(s)</span> only
							through your local Electricity Distribution Company
							(DISCO) in line with the Federal Government National
							Mass Metering Programme (NMMP)
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Members of the public are hereby adviced not to
							patronize unauthorised persons or pay cash directly to
							individual or into private bank company
						</p>
						<p className='text-red-500 mb-4 leading-8 text-center font-bold text-lg'>
							Please beware of fraudsters!
						</p>
					</div>
				</section>
				<Footer />
			</div>
		</>
	);
};

export default Disclaimer;
