import { Helmet } from 'react-helmet';
import Footer from '../components/Footer';
import Nav from '../components/Home/Nav';

const TermsOFUse = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Terms of Use</title>‍
				<meta name="description" content="Terms of Use" />
			</Helmet>
			<div className='bg-gray-200 min-h-screen'>
				<Nav />
				<section className='mt-14 flex justify-center'>
					<div className='py-2 lg:px-32 px-8 pb-16 2xl:w-8/12'>
						<h1 className='text-3xl font-bold text-gray-600 mb-8 text-center'>
							Terms of use
						</h1>
						<p className='text-gray-600 mb-4 leading-8'>
							Welcome to Mojec International!
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Mojec International has developed these Terms of Service
							to govern your use of this website.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							Your use of our website is an acknowledgement to us that
							you have read and agreed to these Terms of Service.
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							Please read them carefully and if you have questions,
							please email. This Terms of Service document is a
							binding contract regarding your use of Mojec
							International's website. If you do not agree with any of
							these terms, please exit the website.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							All information, content, services and software
							displayed on, transmitted through, or used in connection
							with Mojec International, including for example pricing
							information, community updates, online forum comments,
							reviews, directories, guides, text, photographs, images,
							illustrations, audio clips, video, HTML code, source and
							object code, trademarks, logos, and other forms of
							original content, as well as its selection and
							arrangement, is owned by Mojec International and any of
							its accompanying companies. News articles, pricing
							information, community updates, online forum comments,
							reviews, directories, guides, text, photographs, images,
							illustrations, audio clips, video, HTML code, source and
							object code, trademarks, logos and other forms of
							original content will be referred to as Content through
							the rest of this document.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							You may use the Content online only, and solely for your
							personal and / or non-commercial use. You may not remove
							any trademark, copyright or other notice from such
							Content. If you operate a Web site and wish to link to
							Mojec International, you may do so provided you agree to
							cease using such links upon request from Mojec
							International. No other use is permitted without prior
							written permission of Mojec International. The permitted
							use described in this Paragraph is contingent on your
							compliance at all times with these Terms of Service.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							You may not, for example, republish any portion of the
							Content on any Internet, Intranet or extranet site or
							incorporate the Content in any database, compilation,
							archive or cache. You may not distribute any Content to
							others, whether or not for payment or other
							consideration, and you may not modify copy, frame,
							cache, reproduce, sell, publish, transmit, display or
							otherwise use any portion of the Content. You agree not
							to disassemble any software or other products or
							processes accessible through Mojec International, not to
							insert any code or product or manipulate the content of
							Mojec International in any way that affects the user's
							experience, and not to use any data mining, data
							gathering or extraction method.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Requests to use Content for any purpose other than as
							permitted in these Terms of Service should be directed
							to.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							Intellectual Property
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							Mojec International respects the intellectual property
							of others. If you believe your work has been copied in a
							way that constitutes copyright infringement or are aware
							of any infringing material on Mojec International,
							please contact us at the address listed below under the
							heading "Contact”
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							User-Provided Information and Content
						</h2>
						<p className='text-gray-600 mb-2 leading-8'>
							In today’s world, user generated content has become a
							large part of what occurs in the online space. By
							providing information to, communicating with, and/or
							placing material on, Mojec International’s website,
							including for example communication during registration,
							communication in any forum, message or chat area,
							posting any resume on the site, you acknowledge that:
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							You own or otherwise have all necessary rights to the
							content you provide and the rights to use it as provided
							in this Terms of Service;
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							1. All information you provide is true, accurate,
							current and complete, and does not violate these Terms
							of Service; and,
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							2. The content will not cause injury to any person or
							entity. Using a name other than your own legal name is
							prohibited
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							For all such information and material, you grant Mojec
							International a royalty-free, perpetual, irrevocable,
							non-exclusive right and license to use, copy, modify,
							display, archive, store, distribute, reproduce and
							create derivative works from such information, in any
							form, media, software or technology of any kind now
							existing or developed in the future. Without limiting
							the generality of the previous sentence, you authorize
							Mojec International to share the information across all
							Mojec International affiliated websites, to include the
							information in a searchable format accessible by users
							of Mojec International and other Mojec International
							websites, and to use your name and any other information
							in connection with its use of the material you provide.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							You also grant Mojec International the right to use any
							material, information, ideas, concepts, know-how or
							techniques contained in any communication you send to us
							for any purpose whatsoever, including but not limited to
							developing, manufacturing and marketing products using
							such information. All rights in this paragraph are
							granted without the need for additional compensation of
							any sort to you.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Please note that Mojec International does not accept
							unsolicited materials or ideas for use or publication,
							and is not responsible for the similarity of any of its
							content or programming in any media to materials or
							ideas transmitted to Mojec International. Should you
							send any unsolicited materials or ideas, you do so with
							the understanding that no additional consideration of
							any sort will be provided to you, and that you are
							waiving any claim against Mojec International and its
							affiliates regarding the use of such materials and
							ideas, even if material or an idea is used that is
							substantially similar to the idea you sent.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							Guiding Principles for Posting on Mojec International
						</h2>
						<p className='text-gray-600 mb-1 leading-8'>
							Interactive areas, discussion boards, chat rooms, etc.,
							are intended to encourage public discourse. By using
							these areas of Mojec International, you are
							participating in a community that is intended for all
							our users. Therefore, we reserve the right to remove any
							content posted on our site at any time for any reason.
							Decisions as to whether content violates any of our
							guiding principles will be made by Mojec International
							in its discretion after we have actual notice of such
							posting. Without limiting our right to remove content,
							we have attempted to provide guidelines to those posting
							content on our site. When using Mojec International,
							please do not post material that:
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							1. Contains vulgar, profane, abusive, racist or hateful
							language or expressions, epithets or slurs, text,
							photographs or illustrations in poor taste, inflammatory
							attacks of a personal, racial or religious nature.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							2. Is defamatory, threatening, disparaging, grossly
							inflammatory, false, misleading, fraudulent, inaccurate,
							and unfair, contains gross exaggeration or
							unsubstantiated claims, violates the privacy rights of
							any third party, and is unreasonably harmful or
							offensive to any individual or community.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							3. Violates any right of Mojec International or any
							third party.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							4. Discriminates on the grounds of race, religion,
							national origin, gender, age, marital status, sexual
							orientation or disability, or refers to such matters in
							any manner prohibited by law.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							5. Violates or inappropriately encourages the violation
							of any municipal, state, federal or international law,
							rule, regulation or ordinance.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							6. Unfairly interferes with any third party's
							uninterrupted use of Mojec International.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							7. Advertises, promotes or offers to trade any goods or
							services, except in areas specifically designated for
							such purpose.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							8. Uploads copyrighted or other proprietary material of
							any kind on Mojec International without the express
							permission of the owner of that material.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							9. Uses or attempts to use another's account, password,
							service or system except as expressly permitted by the
							Terms of Service.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							10. Uploads or transmits viruses or other harmful,
							disruptive or destructive files.
						</p>
						<p className='text-gray-600 mb-1 leading-8'>
							11. Disrupts, interferes with, or otherwise harms or
							violates the security of Mojec International, or any
							services, system resources, accounts, passwords, servers
							or networks connected to or accessible through Mojec
							International or affiliated or linked sites.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							12. Repeats prior posting of the same message under
							multiple threads or subjects.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Any violation of these guidelines will be referred to
							Law Enforcement Authorities.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							Third Party Relationships through Mojec International
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							Your dealings or communications through Mojec
							International with any party other than Mojec
							International are solely between you and that third
							party. For example, certain areas of Mojec International
							may, in the future, allow you to conduct transactions or
							purchase goods or services. In most cases, these
							transactions will be conducted by our third-party
							partners and vendors. Under no circumstances will Mojec
							International be liable for any goods, services,
							resources or content available through such third party
							dealings or communications, or for any harm related
							thereto. Please review carefully the third party's
							policies and practices and make sure you are comfortable
							with them before you engage in any transaction.
							Complaints, concerns or questions relating to materials
							provided by third parties should be forwarded directly
							to the third party.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							General Disclaimer and Limitation of Liability
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							While Mojec International uses reasonable efforts to
							include accurate and up-to-date information, we make no
							representations as to the accuracy of the Content and
							assume no liability or responsibility for any error or
							omission in the Content. Mojec International does not
							represent that use of any Content will not infringe
							rights of third parties. Mojec International has no
							responsibility for actions of third parties or for
							content provided or posted by others.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Use of Mojec International is at your own risk. Mojec
							International or any of its affiliated companies or
							employees makes no representation of any kind regarding
							the content on our websites, advertising, information on
							products and services or other content generated from
							the use of the website.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Mojec International contains facts, views and statements
							of third party companies and individuals. We do not
							represent or endorse the accuracy or reliability of any
							advice, opinion or statement that is displayed, uploaded
							or distributed through the Mojec International website.
							Your reliance on any such information is at your sole
							risk.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							Indemnity
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							As a user on the website, you agree to indemnify, defend
							and hold harmless Mojec International and their
							strategic partners, executive management team, board of
							directors and other stakeholders from any and all claims
							(including claims for defamation, trade disputes,
							privacy and intellectual property infringement) and
							damages (including lawyer fees and court costs) arising
							from allegations regarding your accepted use of Mojec
							International or our use of any information that you
							provide.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							International Users
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							Mojec International is controlled, operated and
							administered in Nigeria. Mojec International cannot be
							held responsible if content accessed through our website
							is considered illegal in certain territories, or is in
							violation of regulations in that country. By accessing
							the site, it means that you are aware of local laws and
							that you are in compliance with those laws.
						</p>
						<h2 className='text-gray-600 mb-2 font-bold text-lg'>
							Other Topics
						</h2>
						<p className='text-gray-600 mb-4 leading-8'>
							Mojec International reserves the right to change these
							Terms of Service at any time in its discretion and to
							notify users of any such changes solely by changing this
							Terms of Service. Your continued use of Mojec
							International after the posting of any amended Terms of
							Service shall constitute your agreement to be bound by
							any such changes. Your use of this site prior to the
							time this Terms of Service was posted will be governed
							according to the Terms of Service that applied at the
							time of your use.
						</p>
						<p className='text-gray-600 mb-4 leading-8'>
							Mojec International may modify, suspend, discontinue or
							restrict the use of any portion of the website,
							including the availability of any portion of the Content
							at any time, without notice or liability. The website
							may deny access to any person or user at any time for
							any reason. In addition, the website may at any time
							transfer its rights and obligations under this Agreement
							to any Mojec International affiliate, subsidiary or
							business unit, or any of their affiliated companies or
							divisions, or any entity that may acquire or merge with
							Mojec International.
						</p>
						<h5 className='text-gray-600 text-sm font-bold mb-4'>
							Contact Information
						</h5>
						<h2 className='text-gray-600 mb-0 font-bold text-lg'>
							AFRICA HEAD OFFICE (LAGOS)
						</h2>
						<p className='text-gray-600'>
							244/246 APAPA OSHODI EXPRESSWAY, LAGOS, NIGERIA
						</p>
						<p className='text-gray-600 mb-4'>
							TEL: +234 17738393, +234 8027931947
						</p>
						<h2 className='text-gray-600 mb-0 font-bold text-lg'>
							ABUJA OFFICE
						</h2>
						<p className='text-gray-600'>
							PLOT 1335 (7478) CLOSE ALEXANDRA CRESCENT OFF MALLAM
							AMINU KANO CRESCENT, ZONE A7 WUSE11, ABUJA, FCT.
						</p>
						<p className='text-gray-600 mb-4'>
							TEL: +234 8093956766, +234 8095558573
						</p>
						<p className='text-gray-600 mb-4'>
							We value your feedback and look forward to hearing from
							you
						</p>
						<p className='text-gray-600 mb-4'>
							EMAIL: admin@mojec.com, power@mojec.com,
							enquires@mojec.com
						</p>
					</div>
				</section>
				<Footer />
			</div>
		</>
	);
};

export default TermsOFUse;
