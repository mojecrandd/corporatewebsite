import Layout from '../components/Layout';
import transmission from '../assets/images/transmission.jpg';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

const Transmission = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Transmission</title>‍
				<meta name="description" content="Power Transmission" />
			</Helmet>
			<Layout
				title='transmission'
				desc='Our Engineers have designed transmission networks to transport the energy as efficiently as feasible, while at the same time taking into account economic factors, network safety and redundancy'
				image={transmission}
				label='business'
				location={pathname}
			>
				<h5 className='text-lg text-brand-blue font-bold mb-6'>
					Electric Power Transmission
				</h5>
				<p className='font-medium mb-8'>
					The bulk transfer of electrical energy, from generating power
					plants to electrical substations located near demand centres is
					of great essence if power is to be delivered. Our Engineers have
					designed transmission networks to transport the energy as
					efficiently as feasible, while at the same time taking into
					account economic factors, network safety and redundancy.
				</p>
				<p className='font-medium mb-8'>
					The power generation capacity in Nigeria that is obtainable is
					less than 5000Mw, but there are more demand for power
					consumption, hence the need for more transmission projects to
					bring about system reliability.
				</p>
				<h5 className='text-lg text-brand-blue font-bold mb-6'>
					ALSCON - Ibom Power Project at Ikot-Abasi (Akwa-Ibom State)
				</h5>
				<p className='font-medium mb-8'>
					To further boost the power sector in both generation and
					transmission sectors, we have been commissioned to undertake a
					project on the evacuation of 200MW power from Alscon plant into
					the national transmission network grid of Nigeria.
				</p>
				<p className='font-medium mb-8'>
					The Alscon gas-turbine power plant is equipped with six (6) gas
					turbines with a total capacity of 540MW/h. The power plant can
					fully cover the requirements of the smelter in electricity and
					evacuate the excess of generated power into the national grid.
					This will in turn improve the reliability of the system as a
					whole.
				</p>
			</Layout>
		</>
	);
};

export default Transmission;
