import mojecHistory from '../assets/images/mojecHistory.jpg';
import Footer from '../components/Footer';
import LayoutGeneric from '../components/LayoutGeneric';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import LineDivider from '../assets/svg/LineDivider';
import DividerArrowUp from '../assets/svg/DividerArrowUp';
import DividerArrowDown from '../assets/svg/DividerArrowDown';
import { timeline } from '../data/timeline';
import { useEffect, useRef, useState } from 'react';
import { Helmet } from 'react-helmet';

const HistoryWrap = styled.section`
	@import url('https://fonts.googleapis.com/css2?family=Delius&display=swap');
	font-family: 'Delius', cursive;
	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: 0rem;
		left: 1rem;
	}

	/* .overlay {
		background: rgba(0, 0, 0, 0.29);
	} */

	.text-cont {
		transition: background-color 1s ease-in 0s;
		pointer-events: auto;
	}

	.scroll-ani {
		scroll-behavior: smooth;
	}

	.text-cont:hover {
		background-color: rgba(140, 140, 140, 0.3);
	}

	.img-overlay {
		background: rgba(0, 0, 0, 0.29);
	}

	progress {
		border-radius: 1px;
	}
	progress::-webkit-progress-bar {
		background-color: white;
		border-radius: 7px;
	}
	progress::-webkit-progress-value {
		background-color: #e5bd5c;
		border-radius: 1px;
	}
`;

const History = () => {
	const [index, setIndex] = useState(0);
	const slideRight = () => {
		setIndex((index + 1) % timeline.length); // increases index by 1
	};

	const ref = useRef(null);
	const handleScroll = (scrollOffset) =>
		(ref.current.scrollTop += scrollOffset);

	useEffect(() => {
		const timer = setTimeout(() => {
			slideRight();
		}, 8000);
		return () => {
			clearTimeout(timer);
		};
	}, [index]);

	const Variant = {
		rest: {
			scale: 1.5,
		},

		hover: {
			scale: 1,
			transition: {
				// delay: 0.5,
				type: 'tween',
				ease: 'easeOut',
				duration: 1.5,
			},
		},

		rest2: {
			x: 100,
			opacity: 0,
		},

		hover2: {
			x: 0,
			opacity: 1,
			transition: {
				delay: 0.5,
				type: 'tween',
				ease: 'easeOut',
				duration: 2,
			},
		},
	};

	return (
		<>
			<Helmet>‍
				<title>MOJEC | History</title>‍
				<meta name="description" content="MOJEC over the years" />
			</Helmet>
			<div className=''>
				<LayoutGeneric
					title='History - Mojec As A Company'
					desc='Mojec international is an international holding company with subsidiaries in the power, energy, real estate and retail sector with operations across Africa and Asia.'
					image={mojecHistory}
					needsCompany={false}
					label='information'
					to='details'
				>
					<p className='font-medium mb-8' id='details'>
						Registered in 1985, MOJEC International is an international
						holding company with subsidiaries in the power, energy, real
						estate, and retail sector with operations across Africa and
						Asia. MOJEC is ranked as one of the foremost indigenous
						metering companies in corporate Nigeria.
					</p>
					<p className='font-medium mb-8'>
						A leading Electricity EPC Contractor to the electricity
						distribution companies and other African utilities on
						transmission and distribution of projects. MOJEC is chiefly
						known on the continent as a market leader and innovator in
						metering. Always on the upfront of the latest in Metering
						Technology, MOJEC manufactures and supplies a variety of
						meters ranging from: Maximum Demand, Whole Current, Pre-Paid
						Meters, Automatic Meter Reading System (AMR) as well as
						latest in Metering technology, Automatic Meter
						Infrastructure (AMI) via its sister company MOJEC Meter
						Company, one of Nigeria's first-meter factories and Africa's
						largest Meter Manufacturer.
					</p>
					<p className='font-medium mb-8'>
						MOJEC Oil and Gas Company, a subsidiary of MOJEC, whose
						roots go far back as three years, giving excellent delivery
						and unparalleled customer solutions, is a leading provider
						of innovative products and services to the global oil and
						gas industry.
					</p>
					<p className='font-medium mb-8'>
						Today, MOJEC is one of the largest meter suppliers in
						Nigeria, with international joint ventures and alliances
						with some of the world's leading manufacturers in Asia and
						Europe, with a staff strength of almost a hundred people.
					</p>
					<p className='font-medium mb-8'>
						Whilst our meters may be household names in Nigeria markets,
						MOJEC also has a real estate and retail division that
						develops, operates, and manages mixed-use commercial and
						residential properties in Nigeria's major cities namely;
						Lagos, Ibadan, and Abuja.
					</p>
					<p className='font-medium mb-8'>
						From 1985 till today, MOJEC has transformed itself into a
						global specialist in energy management, Transmission, and
						power generation and has today become a solution provider
						that helps make most of your energy.
					</p>
				</LayoutGeneric>
				<HistoryWrap className='bg-black flex  overflow-hidden justify-center h-screen w-full mb-16 relative'>
					<div className='relative h-full w-full'>
						<motion.img
							src={timeline[index].image}
							className='relative top-0 h-screen w-full object-cover'
							alt={timeline[index].year}
							variants={Variant}
							initial='rest'
							key={timeline[index].image}
							animate='hover'
							exit={{ visibility: 'hidden' }}
						/>
						<div className='img-overlay absolute top-0 z-10 w-full h-full'></div>
					</div>
					<section className='absolute w-full h-full z-20 overlay flex justify-center'>
						<div className='flex justify-center w-full 2xl:w-8/12 h-full'>
							<div className='w-full px-4 mx-4 py-2 lg:w-10/12 flex'>
								<section className='w-16 h-5/6 self-center flex flex-col items-center justify-center'>
									<span
										className='w-12 h-12 cursor-pointer flex justify-center items-center'
										onClick={() => handleScroll(-100)}
									>
										<DividerArrowUp />
									</span>
									<div
										className='overflow-y-auto scrollbar-hidden h-full'
										ref={ref}
									>
										<ul className='flex flex-col h-5/6 items-center space-y-2 justify-between scroll-ani'>
											{timeline.map((timel, i) => (
												<li
													key={i}
													className='items-center whitespace-nowrap flex flex-col justify-center'
												>
													<span
														className={`block text-base font-bold ${i === index
																? 'text-brand-yellow'
																: 'text-white'
															}  my-2  cursor-pointer`}
														onClick={() => setIndex(i)}
													>
														{timel.year}
													</span>
													<LineDivider />
												</li>
											))}
										</ul>
									</div>
									<span
										className='w-12 h-12 cursor-pointer flex justify-center items-center'
										onClick={() => handleScroll(100)}
									>
										<DividerArrowDown />
									</span>
								</section>
								<section className='h-5/6 self-end lg:ml-12 w-full lg:w-6/12 flex flex-col justify-around'>
									<motion.div
										className='p-8 text-cont self-start'
										variants={Variant}
										initial='rest2'
										key={timeline[index].image}
										animate='hover2'
										exit={{ visibility: 'hidden' }}
									>
										<div className='flex items-center mb-8'>
											<h1 className='text-2xl lg:text-6xl font-bold text-brand-yellow mr-4'>
												{timeline[index].year}
											</h1>
											<span className='border-brand-yellow border-l-2 h-6 mr-4 self-end'></span>
											<h5 className='capitalize self-end text-xl font-medium text-gray-300'>
												the early years
											</h5>
										</div>
										<p className='text-white'>
											{timeline[index].desc}
										</p>
									</motion.div>
									<div className='w-full p-8'>
										<div className='flex items-end mb-0'>
											<span className='text-white mr-1 '>
												{index + 1}
											</span>
											<span className='text-white mr-1'>
												/
											</span>
											<span className='text-white text-sm'>
												{timeline.length}
											</span>
										</div>
										<progress
											id='file'
											value={(index + 1) * 10}
											max={timeline.length * 10}
											className='w-full lg:w-8/12 h-1'
										></progress>
									</div>
								</section>
							</div>
						</div>
					</section>
				</HistoryWrap>
				<Footer />
			</div>
		</>
	);
};

export default History;
