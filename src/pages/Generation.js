import Layout from '../components/Layout';
import generation from '../assets/images/generation.jpg';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

const Generation = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Generation</title>‍
				<meta name="description" content="Power Generation" />
			</Helmet>
			<Layout
				title='power generation'
				desc='MOJEC is currently one of the leading company in Nigeria in terms of electricity distribution and power generation'
				image={generation}
				label='business'
				location={pathname}
			>
				<p className='font-medium mb-8'>
					MOJEC is currently one of the leading company in Nigeria and in
					partnerships with various companies all over the world in
					electricity distribution and power generation. Presently Mojec
					is working on being able to generate solar energy as an
					additional means of electricity supplied in some states in
					Nigeria and with this the rate/level of electricity outage is
					being reduce drastically.
				</p>
				<p className='font-medium'>
					Our model achieved through the method of Grid-connected systems
					which are being used for homes, public facilities such as
					schools and hospitals, and commercial facilities such as offices
					and shopping centers. Electricity generated during the daytime
					can be used right away, and in some cases surplus electricity
					can be sold to the utility power company. If the system doesn't
					generate enough electricity, or generates none at all (for
					example, on a cloudy or rainy day, or at night) electricity is
					purchased from the utility power company. Power production
					levels and surplus selling can be checked in real time on a
					monitor, an effective way to gauge daily energy consumption.
				</p>
			</Layout>
		</>
	);
};

export default Generation;
