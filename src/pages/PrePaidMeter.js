import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import randd from '../assets/images/r&d.jpg';
import BulletListIcon from '../assets/svg/BulletListIcon';
import NumberingIcon from '../assets/svg/NumberingIcon';
import Footer from '../components/Footer';
import LayoutGeneric from '../components/LayoutGeneric';
import { prePaid } from '../data/products';

const Wrapper = styled.div`
	background: #efefef;
	.overlay {
		background: rgba(0, 0, 0, 0.2);
	}
`;

const PrePaidMeter = () => {
	const [socialIndex, setSocialIndex] = useState(0);
	const [isTweetScroll, setIsTweetScroll] = useState(true);

	const ContentVaraint = {
		rest2: {
			x: isTweetScroll ? 300 : -300,
			opacity: 0,
		},

		hover2: {
			x: 0,
			opacity: 1,
			transition: {
				type: 'tween',
				ease: 'easeOut',
				duration: 0.8,
			},
		},
	};

	const slideSocials = () => {
		setSocialIndex((socialIndex + 1) % prePaid.images.length);
	};

	useEffect(() => {
		const indexTimer = setTimeout(() => {
			slideSocials();
		}, 6000);

		return () => {
			clearTimeout(indexTimer);
		};
	}, [socialIndex]);

	return (
		<>
			<Helmet>‍
				<title>MOJEC | Meter</title>‍
				<meta name="description" content="Pre Paid Meter" />
			</Helmet>
			<Wrapper>
				<LayoutGeneric
					title={prePaid.images[0].headerContent}
					desc={prePaid.images[0].descContent}
					image={randd}
					needsCompany={false}
					label={prePaid.images[0].tag}
					to='details'
				>
					<section
						className='flex flex-col w-full justify-between'
						id='details'
					>
						<div className='lg:h-screen mb-16 overflow-hidden'>
							<div className='w-full h-full bg-white relative overflow-hidden'>
								<motion.img
									src={prePaid.images[socialIndex].img}
									alt={prePaid.images[socialIndex].headerContent}
									initial='rest2'
									animate='hover2'
									variants={ContentVaraint}
									key={prePaid.images[socialIndex].img}
									className='bg-placement w-full h-full'
								/>
								<div className='w-full absolute z-10 top-0 h-full overlay flex items-center justify-center'>
									<ol className='flex w-1/4 justify-center h-2 bottom-8 absolute z-10'>
										{prePaid.images.map((img, i) => (
											<li
												key={i}
												className={`cursor-pointer inline-block w-2 border border-white mr-2 ${socialIndex === i
													? 'bg-white'
													: ''
													}`}
												onClick={() => {
													setSocialIndex(i);
													if (i < 1) {
														setIsTweetScroll(false);
													} else {
														setIsTweetScroll(true);
													}
												}}
											></li>
										))}
									</ol>
								</div>
							</div>
						</div>
						<div className='w-full mb-16 bg-white flex justify-center items-center px-4 lg:px-8 py-8 product-bg'>
							<div className=''>
								<h3 className='text-2xl font-bold mb-8'>
									Pre-Paid Product Offering
								</h3>
								<ul className='grid gap-8 grid-cols-1 lg:grid-cols-2'>
									{prePaid.content.map((content) => (
										<li
											className='flex items-center'
											key={content.id}
										>
											<NumberingIcon className='mr-4 block' />
											<span className='block font-medium text-sm'>
												{content.text}
											</span>
										</li>
									))}
								</ul>
								<div className='mt-8 lg:mt-16'>
									<h3 class='text-2xl font-semibold capitalize mb-6'>
										product overview
									</h3>
									{prePaid.overview.map((prePaid) => (
										<div className='mb-6'>
											<h5 className='text-base mb-3 font-semibold'>
												{prePaid.prodName}
											</h5>
											<p className='text-sm text-gray-800 mb-4'>
												{prePaid.desc}
											</p>
											<ul className='grid gap-8 grid-cols-1 lg:grid-cols-2'>
												{prePaid.list && prePaid.list.map((content) => (
													<li
														className='flex items-center'
														key={content.id}
													>
														<BulletListIcon className='mr-4 block' />
														<span className='block font-medium text-sm text-gray-800'>
															{content.desc}
														</span>
													</li>
												))}
											</ul>
										</div>
									))}
								</div>
							</div>
						</div>
					</section>
				</LayoutGeneric>
				<Footer />
			</Wrapper>
		</>
	);
};

export default PrePaidMeter;
