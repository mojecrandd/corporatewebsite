import styled from 'styled-components';
import Footer from '../components/Footer';
import LayoutGeneric from '../components/LayoutGeneric';
import ourPeople from '../assets/images/ourPeople.jpg';
import { motion } from 'framer-motion';
import FaderWrap from '../components/FaderWrap';
import RightNavIcon from '../assets/svg/RightNavIcon';
import { data } from '../data/ourPeople';
import Modal from '../components/Modal';
import { useState } from 'react';

const OurWrap = styled.div`
	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: 1rem;
		left: 1rem;
	}

	.border-overlay {
		border: 0.2px solid #f1f1f1;
	}

	.overlay {
		background: transparent;
		transition: all 1s ease-in-out;
		color: rgb(69, 69, 69);

		&:hover {
			background: transparent;
			color: #706e6e;
		}

		.imgOverlayCover {
			background: white;
			transition: all 1s ease-in-out;

			&:hover {
				background: transparent;
			}

			.imgHover {
				background: rgba(0, 0, 0, 0.42);
			}
		}

		.imgOverlay {
			transition: all 1s ease-in-out;
			border: 1px solid #ffffff;
		}
	}

	.overlay:hover .imgOverlayCover {
		visibility: hidden;
		opacity: 0;
		transition: all 1s ease-in-out;
	}
`;

const OurPeople = () => {
	const [modal, setModal] = useState({
		show: false,
		data: null,
	});

	const handleClick = (id) => {
		let selected = data.filter((data) => data.id === id);
		setModal({ ...modal, show: true, data: selected });
	};

	return (
		<OurWrap>
			<LayoutGeneric
				title='our people'
				desc='We are serious about surrounding ourselves with smart, experienced people who can help us grow our business and improve our processes'
				image={ourPeople}
				needsCompany={false}
				label='information'
				to='details'
			>
				<p className='font-medium mb-8' id='details'>
					Here at Mojec International we are serious about surrounding
					ourselves with smart, experienced people who can help us
					grow our business and improve our processes.
				</p>
				<p className='font-medium mb-8'>
					At Mojec international we believe in creative ideas that can
					change the world, we never rest and are constantly evolving
					to remain connected by one transforming belief. We are a
					unique, passionate, ambitious and dedicated people who
					consistently strive to improve employee engagement whether
					it's our engineers, our project managers or our leadership
					teams. We have diverse workforce offering a wide range of
					skills. Our employees help us gain and build competitive
					advantage through energy, imagination and local insight,
					their talent and dedication drives our success and when they
					put their minds to it, there is really nothing they cannot
					achieve.
				</p>
				<p className='font-medium mb-8'>
					We welcome the unique perspectives and different strengths
					our team membersbring to the job and we know that diversity
					makes us a greater team. Our people are the vision behind
					our success and the ideas behind our innovation and global
					expertise.
				</p>
			</LayoutGeneric>
			<FaderWrap>
				<div className='bg-gray-200 flex justify-center'>
					<div className='py-2 lg:px-10 px-8 2xl:w-8/12 relative'>
						<section className='grid gap-0 grid-cols-1 lg:grid-cols-3 py-8'>
							{data.map((data) => (
								<div
									key={data.id}
									className='relative h-96 w-full border-overlay cursor-pointer'
									onClick={() => handleClick(data.id)}
								>
									<motion.img
										src={data.image}
										className='h-full w-full object-cover'
										alt={data.name}
									/>
									<div className='absolute top-0 z-10 overlay flex justify-end items-center w-full h-full flex-col'>
										<div className='absolute top-0 z-10 imgOverlayCover flex justify-center items-center w-full h-full flex-col'>
											<div className='w-60 h-60 mb-8 relative'>
												<motion.img
													src={data.image}
													className='h-full w-full object-fill'
													alt={data.name}
												/>
												<div className='imgHover absolute top-0 w-full h-full'></div>
											</div>
										</div>
										<div className='imgOverlay w-60 h-60 mb-8'></div>
										<div className='mb-8 z-30 flex justify-between w-3/5 items-center'>
											<section>
												<h3 className='text-xl font-bold capitalize'>
													{data.name}
												</h3>
												<p className='capitalize text-sm'>
													{data.position}
												</p>
											</section>
											<RightNavIcon color='#E5BD5C' />
										</div>
									</div>
								</div>
							))}
						</section>
						<h2 className='font-bold capitalize text-xl lg:text-3xl inline-block text-gray-600 absolute z-10'>
							our people
						</h2>
					</div>
				</div>
			</FaderWrap>
			{modal.show && <Modal setModal={setModal} data={modal.data} />}
			<Footer />
		</OurWrap>
	);
};

export default OurPeople;
