import Layout from '../components/Layout';
import corpValue from '../assets/images/corpValue.jpg';
import NumberingIcon from '../assets/svg/NumberingIcon';
import { Helmet } from 'react-helmet';

const CorpValues = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Corporate Values</title>‍
				<meta name="description" content="Our values" />
			</Helmet>
			<Layout
				title='corporate values'
				desc='MOJEC’s culture is driven by transparency, respect,
				collaboration andfeedback. We are obsessed with client
				experience and communicating with our customers as well as
				within the team. We love lean, iterative improvements and our
				success is measured by the value we create for our users,
				enabling them to build a world of endless possibilities for
				themselves.'
				image={corpValue}
				label='information'
			>
				<h5 className='text-lg text-black font-bold mb-8' id='details'>
					Here are the core values we live by:
				</h5>
				<ul className='grid gap-8 grid-cols-1 lg:grid-cols-2'>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							Obsess over our customers and their satisfaction.
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							Invent, Simplify and Innovate.
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							Question Assumptions, Think Deeply.
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							Build Trust with Transparency.
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							Always Learn, It’s fine to make mistakes as long as we
							are learning from them.
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							Insist on the highest standards, deliver results.
						</span>
					</li>
					<li className='flex items-center'>
						<NumberingIcon className='mr-4' />
						<span className='block font-medium uppercase text-base'>
							A team that’s empowered, diverse and inclusive.
						</span>
					</li>
				</ul>
			</Layout>
		</>
	);
};

export default CorpValues;
