import { motion } from 'framer-motion';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Nav from '../components/Home/Nav';
import { data } from '../data/newsSlider';
import Footer from '../components/Footer';
import ArrowNavigation from '../components/ArrowNavigation';
import { Helmet } from 'react-helmet';

const ArticlesWrap = styled.div`
	background: #efefef;
	.heading {
		top: 30%;
	}

	h2 {
		box-shadow: 0 6px 9px 0 rgba(0, 0, 0, 0.2);
		background-color: rgba(255, 255, 255, 0.8);
		padding: 8px 20px;
		top: 1rem;
		left: 1rem;
	}

	//animation
	.slider {
		transform: translateX(-100%);
		-webkit-transform: translateX(-100%);
	}

	.slide-in {
		animation: slide-in 0.5s forwards;
		-webkit-animation: slide-in 0.5s forwards;
	}

	.slide-out {
		animation: slide-out 0.5s forwards;
		-webkit-animation: slide-out 0.5s forwards;
	}

	@keyframes slide-in {
		100% {
			transform: translateX(0%);
		}
	}

	@-webkit-keyframes slide-in {
		100% {
			-webkit-transform: translateX(0%);
		}
	}

	@keyframes slide-out {
		0% {
			transform: translateX(100%);
		}
		100% {
			transform: translateX(0%);
		}
	}

	@-webkit-keyframes slide-out {
		0% {
			-webkit-transform: translateX(100%);
		}
		100% {
			-webkit-transform: translateX(0%);
		}
	}
`;

const HeroSectionWrap = styled.section`
	background: linear-gradient(
		76.97deg,
		rgba(17, 27, 40, 0.24) 34.77%,
		rgba(0, 0, 0, 0.08) 90.6%
	);
	height: 103vh;
	z-index: 15;
	position: relative;
	bottom: 72px;

	@media (min-width: 1024px) {
		height: 100vh;
	}
`;

const ArticlesPage = ({ match }) => {
	const [index, setIndex] = useState(0);
	const [isActive, setIsActive] = useState(true);
	const id = match.params.ID;
	const selected = data[id];

	const slideRight = () => {
		setIndex((index + 1) % selected.images.length); // increases index by 1
		setIsActive(!isActive);
	};

	const ContentVaraint = {
		rest: {
			y: 300,
			opacity: 0,
		},

		hover: {
			y: 0,
			opacity: 1,
			transition: {
				delay: 0.5,
				type: 'tween',
				ease: 'easeOut',
				duration: 1.5,
			},
		},

		borderRest: {
			y: 50,
			opacity: 0,
		},

		borderAnimate: {
			y: 0,
			opacity: 1,
			transition: {
				delay: 1.5,
				type: 'tween',
				ease: 'easeOut',
				duration: 0.5,
			},
		},
	};

	useEffect(() => {
		const timer = setTimeout(() => {
			slideRight();
		}, 15000);
		return () => {
			clearTimeout(timer);
		};
	}, [index]);

	return (
		<>
			<Helmet>‍
				<title>MOJEC | News</title>‍
				<meta name="description" content="MOJECs in the news" />
			</Helmet>
			<ArticlesWrap className='h-auto'>
				<Nav className='z-50 relative' />
				<HeroSectionWrap></HeroSectionWrap>
				<ArrowNavigation to='in-the-news' />
				<section className='absolute top-0 h-screen w-full bg-black'>
					<ol className='flex justify-between h-3 mini-caurosel absolute z-20 bottom-8 left-20 lg:left-32 2xl:left-1/3'>
						{selected.images.map((img, i) => (
							<li
								className={`cursor-pointer inline-block w-3 mr-2 border border-white ${index === i ? 'bg-white' : ''
									}`}
								onClick={() => {
									setIndex(i);
									index !== i && setIsActive(!isActive);
								}}
							></li>
						))}
					</ol>
					<section className='overflow-hidden h-screen'>
						<motion.img
							src={selected.images[index]}
							className={`z-10 h-full w-full object-cover relative ${isActive ? 'slide-in' : 'slide-out'
								} slider`}
							alt='generation image'
						/>
					</section>
					<div className='flex justify-center'>
						<div className='w-full 2xl:w-8/12'>
							<motion.header
								initial='rest'
								animate='hover'
								variants={ContentVaraint}
								key={selected.headerText}
								exit={{ opacity: 0 }}
								className='absolute z-30 heading flex lg:w-w-3/12 2xl:w-1/3 flex-col px-4 py-2 lg:px-16'
							>
								<span className='bg-brand-blue flex justify-center items-center w-24 h-8 italic text-sm capitalize text-white mb-4'>
									News
								</span>
								<h1 className='text-white text-3xl font-bold lg:text-5xl mb-4 capitalize'>
									{selected.headerText}
								</h1>
								<section className='flex justify-center items-center'>
									<div className='flex w-3/4 h-auto'>
										<motion.div
											initial='borderRest'
											animate='borderAnimate'
											variants={ContentVaraint}
											className='border-brand-yellow border-l mr-2'
										></motion.div>
										<div className=''>
											<p className='text-white text-base font-medium mb-2'>
												{selected.descText}
											</p>
										</div>
									</div>
								</section>
							</motion.header>
						</div>
					</div>
				</section>
				<section className='flex justify-center' id='in-the-news'>
					<div className='py-2 lg:px-32 px-8 pb-16 2xl:w-8/12'>
						{selected.details.map((detail) => (
							<p
								className='text-gray-600 mb-4 leading-8'
								key={detail.id}
							>
								{detail.desc}
							</p>
						))}
					</div>
				</section>
				<Footer />
			</ArticlesWrap>
		</>
	);
};

export default ArticlesPage;
