import Layout from '../components/Layout';
import mojecInstaller from '../assets/images/timeline/mojecInstaller.jpg';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

const Distribution = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Distribution</title>‍
				<meta name="description" content="MOJEC's distribution system" />
			</Helmet>
			<Layout
				title='power distribution'
				desc='MOJEC has been able to successfully measure energy from the high tension side to the low tension side of distribution networks across the country'
				image={mojecInstaller}
				label='business'
				location={pathname}
			>
				<p className='font-medium mb-8'>
					An electric power distribution system is the final stage in the
					delivery of electric power. MOJEC has been able to successfully
					measure energy from the high tension side to the low tension
					side of distribution networks across the country. Firmly we have
					achieved mile stones in helping the utility companies account
					for energy been distributed.
				</p>
				<p className='font-medium'>
					Smart Grid is the direction for modern electric power delivery,
					and MOJEC is a market leader in innovations.
				</p>
			</Layout>
		</>
	);
};

export default Distribution;
