import Layout from '../components/Layout';
import randd from '../assets/images/r&d.jpg';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';

const RandD = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | R&D</title>‍
				<meta name="description" content="Research and Development" />
			</Helmet>
			<Layout
				title='research & development'
				desc="The MOJEC Group harnesses the skills of the World's finest Engineering Researchers, Developers and Consultants who play a combined role of conceptualizing, designing and implementing industry standard products"
				image={randd}
				label='information'
				location={pathname}
			>
				<p className='font-medium mb-8'>
					The Research and Development arm was set up towards enacting the
					motto of the Group, opening up the Nigerian and West African
					Power sector to the endless possiblities of smart metering, AMI
					monitoring and token vending.
				</p>
				<p className='font-medium mb-8'>
					Today, the finished products initiated from the thought process
					of the Research and Development arm of MOJEC ranks among the
					best in Nigeria and across the African Continent.
				</p>
				<p className='font-medium mb-8'>
					The combined efforts of the team have made sure that MOJEC's
					smart meters remain the pedestal across Nigeria.
				</p>
				<p className='font-medium mb-8'>
					MOJEC's state-of-the-art Advanced Metering Infrastructure (AMI)
					monitoring system emerged as a key factor in meter tracking
					across the DisCos and also aided the easy monitoring and
					tracking of field meters.
				</p>
				<p className='font-medium mb-8'>
					MOJEC's Research and Development also played a key role in
					improving customer relations by creating systems such as Payment
					Reconciliation and Retrieval platforms for easy and efficient
					customer dispute resolution.
				</p>
			</Layout>
		</>
	);
};

export default RandD;
