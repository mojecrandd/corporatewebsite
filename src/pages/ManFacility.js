import { Helmet } from 'react-helmet';
import randd from '../assets/images/r&d.jpg';
import Layout from '../components/Layout';


const ManFacility = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Facility</title>‍
				<meta name="description" content="manufacturing facilities" />
			</Helmet>
			<Layout
				title='manufacturing facilities'
				desc='Our Manufacturing Facility is second to none in Nigeria'
				image={randd}
				label='information'
			>
				<p className='font-medium mb-8'>
					Our state of the art working environment, automatic test
					benches, dedicated staff and with a huge working experience to
					draw from. We have align ourselves over the years with
					international experts in the birth of this vision in giving it a
					standard that can compete on the world stage. Quality of service
					has been the bedrock of MOJEC INTERNATIONAL and that is why a
					lot has been put into the setup of the multibillionaire project,
					i.e. making meters available to every consumer of electricity in
					Nigeria and beyond.
				</p>
				<p className='font-medium mb-8'>
					The installed capacity is enough to roll out over a million
					meters yearly, with fully automatic test benches, and the
					highest number of such in Nigeria. Most of our Manufacturing
					processes are computerized which has helped us in delivering our
					goods on time every time.
				</p>
				<p className='font-medium mb-8'>
					Our Quality Management System as over the years make sure that
					the roll out of large amount of meters is not the primary aim,
					but making sure that every meter coming out of the factory is
					verified ok. Quality is part of our production process to ensure
					that all product are fit for purpose.
				</p>
			</Layout>
		</>
	);
}

export default ManFacility
