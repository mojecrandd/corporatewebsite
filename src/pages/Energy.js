import Layout from "../components/Layout";
import energy from '../assets/images/energy.jpg'
import { useLocation } from "react-router-dom";
import { Helmet } from "react-helmet";

const Energy = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Oil and Gas</title>‍
				<meta name="description" content="MOJEC Oil & Gas" />
			</Helmet>
			<Layout
				title='Oil and Gas'
				desc='As an indigenous Nigerian oil and gas distribution company with a vision to be the leading distributor of gas to the Nigerian domestic market'
				image={energy}
				label='business'
				location={pathname}
			>
				<h5 className='text-lg text-brand-blue font-bold mb-6'>
					MOJEC is an indigenous Nigerian oil and gas distribution company
				</h5>
				<p className='font-medium mb-8'>
					MOJEC is an indigenous Nigerian oil and gas distribution company
					with a vision to be the leading distributor of gas to the
					Nigerian domestic market. We aim to achieve this by developing
					our business portfolios to create long-term sustainable growth.
				</p>
				<p className='font-medium mb-8'>
					Our business model is designed to capture the tail end of the
					value chain i.e. marketing to end users. This model is supported
					by an experienced Board and senior management team, high
					standards of corporate governance and corporate social
					responsibility.
				</p>
			</Layout>
		</>
	);
}

export default Energy
