import Layout from '../components/Layout';
import afterss from '../assets/images/afterss.jpg';
import { Helmet } from 'react-helmet';

const AfterSales = () => {
	return (
		<>
			<Helmet>‍
				<title>MOJEC | After Sales</title>‍
				<meta name="description" content="Amazing customer service" />
			</Helmet>
		<Layout
			title='after sales services'
			desc='MOJEC consistently strive to create an environment of amazing customer service'
            image={afterss}
            label='business'
		>
			<p className='font-medium mb-8'>
				Mojec, as a company, is fully concerned about her customer's
				welfare, that is; their feelings about the product, the
				challenges they face while using the product, and their opinions
				about the product. Mojec carries this out by checking on the
				customers often enough to know what exactly the challenges they
				face and proffering a lasting solution to such challenges. The
				company is also blessed with adequate personnel and a guaranteed
				Quality control team that carry out regular calls on the
				customers in order to pass on knowledge.
			</p>
			<p className='font-medium mb-8'>
				Mojec also has a customer service unit, where customers can
				lodge their grievances and complaints. Through this we are able
				to satisfy and make our entire customer comfortable.
			</p>
		</Layout>
		</>
	);
};

export default AfterSales;
