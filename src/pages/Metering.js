import Layout from "../components/Layout";
import meter2 from '../assets/images/meter2.jpg'
import { useLocation } from "react-router-dom";
import { Helmet } from "react-helmet";

const Metering = () => {
	const { pathname } = useLocation();
	return (
		<>
			<Helmet>‍
				<title>MOJEC | Meter Company</title>‍
				<meta name="description" content="MOJEC MEtering Company" />
			</Helmet>
			<Layout
				title='mojec meter company'
				desc='MOJEC Meter Company pioneered the concept of smart metering technology in Nigeria by  setting up a state-of-the-art electricity meter plant in the country with a production capacity of  1,200,000 meters annually designed to serve the local African markets, the EMEA and  neighboring markets; fully equipped to handle customer demand.'
				image={meter2}
				label='business'
				location={pathname}
			>
				<p className='font-medium mb-8'>
					MOJEC manufactures and supplies diﬀerent meter types ranging
					from Single Phase Prepayment Meters, Three Phase Prepayment
					Meters, Whole Current Meters, LV MD Credit Meter/Prepayment
					Meters to HT Metering Panel.
				</p>
				<p className='font-medium mb-8'>
					MOJEC also deploys alongside its meters, data management
					systems, Advanced Metering Infrastructure (AMI) and PowerGenie,
					an automated revenue collection service
				</p>
				<p className='font-medium mb-8'>
					Smart metering allows electricity consumers to manage their
					consumption and eradicate outrageous estimated billing. It also
					beneﬁts electricity distribution companies by providing revenue
					protection service, detecting energy and bypass incidents, and
					issuing reports for events of tampering.
				</p>
				<p className='font-medium'>
					Partnering with EKO Electricity distribution company, the ﬁrst
					Distribution Company to deploy CAPMI scheme in the country,
					propelled us to launch full scale into the electricity market
					post- privatization of the power sector in Nigeria. Since its
					inception, we have achieved a 80% footprint in 9 out of 11 of
					the electricity distribution companies.
				</p>
			</Layout>
		</>
	);
}

export default Metering
