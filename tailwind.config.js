module.exports = {
	purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			colors: {
				'brand-blue': '#2659A2',
				'brand-yellow': '#E5BD5C',
			},
			gridTemplateColumns: {
				'2fr': 'auto auto',
			},
			width: {
				'w-30': '30%',
			},
			screens: {
				'3xl': '2000px',
			},
		},
		fontFamily: {
			display: ['Lato', 'sans-serif'],
			body: ['Lato', 'sans-serif'],
		},
	},
	variants: {
		extend: {},
	},
	plugins: [],
};
