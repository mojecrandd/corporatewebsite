var Scrollbar = window.Scrollbar;

Scrollbar.init(document.querySelector('#my-scrollbar'), {
	damping: 0.01,
	alwaysShowTracks: true,
});
